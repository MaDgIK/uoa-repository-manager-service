#FROM tomcat:7.0.90-jre8
FROM tomcat:8.5.71-jre8-openjdk-bullseye
MAINTAINER "pkanakakis@di.uoa.gr"
RUN ["rm", "-fr", "/usr/local/tomcat/webapps/ROOT"]
COPY ./target/uoa-repository-manager-service.war /usr/local/tomcat/webapps/uoa-repository-manager-service.war
#COPY src/main/resources/dnet-override-new.properties /usr/local/tomcat/lib/dnet-override.properties
COPY validator.env /usr/local/tomcat/lib/dnet-override.properties
#COPY src/main/resources/application.properties /usr/local/tomcat/lib/application.properties
#COPY src/main/resources/email-texts.properties /usr/local/tomcat/lib/email-texts.properties
VOLUME /var/log/uoa-repository-manager-service:/var/log/uoa-repository-manager-service/
CMD ["catalina.sh", "run"]

