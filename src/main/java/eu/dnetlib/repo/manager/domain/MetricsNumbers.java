package eu.dnetlib.repo.manager.domain;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

/**
 * Created by stefania on 11/7/17.
 */
public class MetricsNumbers{

    private List<String> downloads;
    private List<String> views;

    @JsonProperty("total_downloads")
    private String totalDownloads;

    @JsonProperty("total_views")
    private String totalViews;

    @JsonProperty("total_openaire_downloads")
    private String totalOpenAIREDownloads;

    @JsonProperty("total_openaire_views")
    private String totalOpenAIREViews;

    @JsonProperty("pageviews")
    private String pageViews;

    public MetricsNumbers() {
    }

    public MetricsNumbers(List<String> downloads, List<String> views, String totalDownloads, String totalViews,
                          String totalOpenAIREDownloads, String totalOpenAIREViews, String pageViews) {
        this.downloads = downloads;
        this.views = views;
        this.totalDownloads = totalDownloads;
        this.totalViews = totalViews;
        this.totalOpenAIREDownloads = totalOpenAIREDownloads;
        this.totalOpenAIREViews = totalOpenAIREViews;
        this.pageViews = pageViews;
    }

    public List<String> getDownloads() {
        return downloads;
    }

    public void setDownloads(List<String> downloads) {
        this.downloads = downloads;
    }

    public List<String> getViews() {
        return views;
    }

    public void setViews(List<String> views) {
        this.views = views;
    }

    public String getTotalDownloads() {
        return totalDownloads;
    }

    public void setTotalDownloads(String totalDownloads) {
        this.totalDownloads = totalDownloads;
    }

    public String getTotalViews() {
        return totalViews;
    }

    public void setTotalViews(String totalViews) {
        this.totalViews = totalViews;
    }

    public String getTotalOpenAIREDownloads() {
        return totalOpenAIREDownloads;
    }

    public void setTotalOpenAIREDownloads(String totalOpenAIREDownloads) {
        this.totalOpenAIREDownloads = totalOpenAIREDownloads;
    }

    public String getTotalOpenAIREViews() {
        return totalOpenAIREViews;
    }

    public void setTotalOpenAIREViews(String totalOpenAIREViews) {
        this.totalOpenAIREViews = totalOpenAIREViews;
    }

    public String getPageViews() {
        return pageViews;
    }

    public void setPageViews(String pageViews) {
        this.pageViews = pageViews;
    }
}

