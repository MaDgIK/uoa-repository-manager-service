package eu.dnetlib.repo.manager.domain.broker;


/**
 * Created by stefanos on 10-Mar-17.
 */

public class OpenaireSubscription {

    private String subscriber;
    private NotificationFrequency frequency;
    private NotificationMode mode;
    private AdvQueryObject query;

    public OpenaireSubscription() {
    }

    public OpenaireSubscription(final String subscriber, final NotificationFrequency frequency, final
    NotificationMode mode,
                                final AdvQueryObject query) {
        this.subscriber = subscriber;
        this.frequency = frequency;
        this.mode = mode;
        this.query = query;
    }

    public String getSubscriber() {
        return this.subscriber;
    }

    public void setSubscriber(final String subscriber) {
        this.subscriber = subscriber;
    }

    public NotificationFrequency getFrequency() {
        return this.frequency;
    }

    public void setFrequency(final NotificationFrequency frequency) {
        this.frequency = frequency;
    }

    public NotificationMode getMode() {
        return this.mode;
    }

    public void setMode(final NotificationMode mode) {
        this.mode = mode;
    }

    public AdvQueryObject getQuery() {
        return this.query;
    }

    public void setQuery(final AdvQueryObject query) {
        this.query = query;
    }


}
