package eu.dnetlib.repo.manager.domain.broker;



import java.util.ArrayList;
import java.util.List;

/**
 * Created by stefanos on 17/3/2017.
 */
public class MapConditions   {

    private String field;
    private MapValueType fieldType;
    private ConditionOperator operator;
    private List<ConditionParams> listParams = new ArrayList<>();

    public MapConditions() {
    }

    public String getField() {
        return field;
    }

    public void setField(String field) {
        this.field = field;
    }

    public MapValueType getFieldType() {
        return fieldType;
    }

    public void setFieldType(MapValueType fieldType) {
        this.fieldType = fieldType;
    }

    public ConditionOperator getOperator() {
        return operator;
    }

    public void setOperator(ConditionOperator operator) {
        this.operator = operator;
    }

    public List<ConditionParams> getListParams() {
        return listParams;
    }

    public void setListParams(List<ConditionParams> listParams) {
        this.listParams = listParams;
    }
}
