package eu.dnetlib.repo.manager.domain.broker;



import java.util.ArrayList;
import java.util.List;

public class AdvQueryObject   {

    private String datasource = "";
    private String topic = "";
    private List<String> titles = new ArrayList<>();
    private List<String> subjects = new ArrayList<>();
    private List<String> authors = new ArrayList<>();
    private List<Range> dates = new ArrayList<>();
    private Range trust = new Range("0", "1");

    private long page = 0;

    public AdvQueryObject() {
    }

    public AdvQueryObject(String datasource, String topic, List<String> titles, List<String> subjects, List<String> authors,
                          List<Range> dates, Range trust, long page) {
        this.datasource = datasource;
        this.topic = topic;
        this.titles = titles;
        this.subjects = subjects;
        this.authors = authors;
        this.dates = dates;
        this.trust = trust;
        this.page = page;
    }

    public String getDatasource() {
        return datasource;
    }

    public void setDatasource(String datasource) {
        this.datasource = datasource;
    }

    public String getTopic() {
        return topic;
    }

    public void setTopic(String topic) {
        this.topic = topic;
    }

    public List<String> getTitles() {
        return titles;
    }

    public void setTitles(List<String> titles) {
        this.titles = titles;
    }

    public List<String> getSubjects() {
        return subjects;
    }

    public void setSubjects(List<String> subjects) {
        this.subjects = subjects;
    }

    public List<String> getAuthors() {
        return authors;
    }

    public void setAuthors(List<String> authors) {
        this.authors = authors;
    }

    public List<Range> getDates() {
        return dates;
    }

    public void setDates(List<Range> dates) {
        this.dates = dates;
    }

    public Range getTrust() {
        return trust;
    }

    public void setTrust(Range trust) {
        this.trust = trust;
    }

    public long getPage() {
        return page;
    }

    public void setPage(long page) {
        this.page = page;
    }
}

