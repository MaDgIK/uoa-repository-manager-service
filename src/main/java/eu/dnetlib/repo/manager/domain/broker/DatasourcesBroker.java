package eu.dnetlib.repo.manager.domain.broker;


import eu.dnetlib.repo.manager.domain.Tuple;

import java.util.List;

/**
 * Created by stefanos on 31/10/2016.
 */
public class DatasourcesBroker   {

    private List<Tuple<BrowseEntry, String>> datasourcesOfUser;
    private List<Tuple<BrowseEntry, String>> sharedDatasources;
    private List<Tuple<BrowseEntry, String>> datasourcesOfOthers;

    public List<Tuple<BrowseEntry, String>> getDatasourcesOfUser() {
        return datasourcesOfUser;
    }

    public void setDatasourcesOfUser(List<Tuple<BrowseEntry, String>> datasourcesOfUser) {
        this.datasourcesOfUser = datasourcesOfUser;
    }

    public List<Tuple<BrowseEntry, String>> getSharedDatasources() {
        return sharedDatasources;
    }

    public void setSharedDatasources(List<Tuple<BrowseEntry, String>> sharedDatasources) {
        this.sharedDatasources = sharedDatasources;
    }

    public List<Tuple<BrowseEntry, String>> getDatasourcesOfOthers() {
        return datasourcesOfOthers;
    }

    public void setDatasourcesOfOthers(List<Tuple<BrowseEntry, String>> datasourcesOfOthers) {
        this.datasourcesOfOthers = datasourcesOfOthers;
    }
}
