package eu.dnetlib.repo.manager.domain.broker;



/**
 * Created by stefanos on 10-Mar-17.
 */
public enum NotificationFrequency   {
    never, realtime, daily, weekly, monthly
}