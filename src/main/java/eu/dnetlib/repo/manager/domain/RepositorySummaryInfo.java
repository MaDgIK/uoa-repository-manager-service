package eu.dnetlib.repo.manager.domain;



import java.util.Date;

public class RepositorySummaryInfo{

    String id;
    String repositoryName;
    String logoURL;
    int recordsCollected;
    Date lastIndexedVersion;
    long enrichmentEvents;
    String totalViews;
    String totalDownloads;

    public RepositorySummaryInfo() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getRepositoryName() {
        return repositoryName;
    }

    public void setRepositoryName(String repositoryName) {
        this.repositoryName = repositoryName;
    }

    public String getLogoURL() {
        return logoURL;
    }

    public void setLogoURL(String logoURL) {
        this.logoURL = logoURL;
    }

    public int getRecordsCollected() {
        return recordsCollected;
    }

    public void setRecordsCollected(int recordsCollected) {
        this.recordsCollected = recordsCollected;
    }

    public Date getLastIndexedVersion() {
        return lastIndexedVersion;
    }

    public void setLastIndexedVersion(Date lastIndexedVersion) {
        this.lastIndexedVersion = lastIndexedVersion;
    }

    public long getEnrichmentEvents() {
        return enrichmentEvents;
    }

    public void setEnrichmentEvents(long enrichmentEvents) {
        this.enrichmentEvents = enrichmentEvents;
    }

    public String getTotalViews() {
        return totalViews;
    }

    public void setTotalViews(String totalViews) {
        this.totalViews = totalViews;
    }

    public String getTotalDownloads() {
        return totalDownloads;
    }

    public void setTotalDownloads(String totalDownloads) {
        this.totalDownloads = totalDownloads;
    }
}
