package eu.dnetlib.repo.manager.domain.dto;

import java.util.Date;

public class RepositoryTerms {

    private String id;
    private String name;
    private Boolean consentTermsOfUse;
    private Boolean fullTextDownload;
    private Date consentTermsOfUseDate;
    private Date lastConsentTermsOfUseDate;

    public RepositoryTerms() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Boolean getConsentTermsOfUse() {
        return consentTermsOfUse;
    }

    public void setConsentTermsOfUse(Boolean consentTermsOfUse) {
        this.consentTermsOfUse = consentTermsOfUse;
    }

    public Boolean getFullTextDownload() {
        return fullTextDownload;
    }

    public void setFullTextDownload(Boolean fullTextDownload) {
        this.fullTextDownload = fullTextDownload;
    }

    public Date getConsentTermsOfUseDate() {
        return consentTermsOfUseDate;
    }

    public void setConsentTermsOfUseDate(Date consentTermsOfUseDate) {
        this.consentTermsOfUseDate = consentTermsOfUseDate;
    }

    public Date getLastConsentTermsOfUseDate() {
        return lastConsentTermsOfUseDate;
    }

    public void setLastConsentTermsOfUseDate(Date lastConsentTermsOfUseDate) {
        this.lastConsentTermsOfUseDate = lastConsentTermsOfUseDate;
    }
}
