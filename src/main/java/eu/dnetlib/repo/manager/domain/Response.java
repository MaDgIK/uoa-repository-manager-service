package eu.dnetlib.repo.manager.domain;

public class Response {

    private Header header;

    public Response() {
        this.header = new Header();
    }

    public Header getHeader() {
        return header;
    }

    public Response setHeader(final Header header) {
        this.header = header;
        return this;
    }
}

