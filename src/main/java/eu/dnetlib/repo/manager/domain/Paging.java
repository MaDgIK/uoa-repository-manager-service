package eu.dnetlib.repo.manager.domain;


import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.List;

public class Paging<T> {

    private int total;

    private int from;

    private int to;

    private List<T> results;

    public Paging(int total, int from, int to, List<T> results) {
        this.total = total;
        this.from = from;
        this.to = to;
        this.results = results;
    }

    public Paging(@NotNull Paging<T> page) {
        this.total = page.getTotal();
        this.from = page.getFrom();
        this.to = page.getTo();
        this.results = page.getResults();
    }

    public <K> Paging(@NotNull Paging<K> page, List<T> results) {
        this.total = page.getTotal();
        this.from = page.getFrom();
        this.to = page.getTo();
        this.results = results;
    }

    public Paging() {
        this.total = 0;
        this.from = 0;
        this.to = 0;
        this.results = new ArrayList<>();
    }

    public static  <K> Paging<K> of(Header header, List<K> results) {
        Paging<K> paging = new Paging<>();
        paging.setFrom(header.getPage() * header.getSize());
        paging.setTo(paging.getFrom() + header.getSize() - 1);
        if (paging.getTo() > header.getTotal()) {
            paging.setTo((int) header.getTotal() - 1);
        }
        paging.setTotal((int) header.getTotal());
        paging.setResults(results);
        return paging;
    }

    public int getTotal() {
        return total;
    }

    public void setTotal(int total) {
        this.total = total;
    }

    public int getFrom() {
        return from;
    }

    public void setFrom(int from) {
        this.from = from;
    }

    public int getTo() {
        return to;
    }

    public void setTo(int to) {
        this.to = to;
    }

    public List<T> getResults() {
        return results;
    }

    public void setResults(List<T> results) {
        this.results = results;
    }
}
