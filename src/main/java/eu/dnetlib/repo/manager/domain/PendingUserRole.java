package eu.dnetlib.repo.manager.domain;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class PendingUserRole {
    @Id
    long id;
    int coPersonId;
    int couId;

    public PendingUserRole() {
        // no-arg constructor
    }

    public PendingUserRole(int coPersonId, int couId) {
        this.coPersonId = coPersonId;
        this.couId = couId;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public int getCoPersonId() {
        return coPersonId;
    }

    public void setCoPersonId(int coPersonId) {
        this.coPersonId = coPersonId;
    }

    public int getCouId() {
        return couId;
    }

    public void setCouId(int couId) {
        this.couId = couId;
    }

    @Override
    public String toString() {
        return "PendingUserRole{" +
                "coPersonId=" + coPersonId +
                ", couId=" + couId +
                '}';
    }
}
