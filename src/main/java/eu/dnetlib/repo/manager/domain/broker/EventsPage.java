package eu.dnetlib.repo.manager.domain.broker;

import eu.dnetlib.broker.objects.OaBrokerEventPayload;

import java.util.List;

/**
 * Created by stefanos on 26/10/2016.
 */
public class EventsPage{

    private String datasource;
    private String topic;
    private long currPage;
    private long totalPages;
    private long total;
    private List<OaBrokerEventPayload> values;

    public EventsPage() {

    }
    public EventsPage(final String datasource, final String topic, final long currPage, final long totalPages, final long total,
                      final List<OaBrokerEventPayload> values) {
        this.datasource = datasource;
        this.topic = topic;
        this.currPage = currPage;
        this.totalPages = totalPages;
        this.total = total;
        this.values = values;
    }

    public String getDatasource() {
        return datasource;
    }

    public void setDatasource(String datasource) {
        this.datasource = datasource;
    }

    public String getTopic() {
        return topic;
    }

    public void setTopic(String topic) {
        this.topic = topic;
    }

    public long getCurrPage() {
        return currPage;
    }

    public void setCurrPage(long currPage) {
        this.currPage = currPage;
    }

    public long getTotalPages() {
        return totalPages;
    }

    public void setTotalPages(long totalPages) {
        this.totalPages = totalPages;
    }

    public long getTotal() {
        return total;
    }

    public void setTotal(long total) {
        this.total = total;
    }

    public List<OaBrokerEventPayload> getValues() {
        return values;
    }

    public void setValues(List<OaBrokerEventPayload> values) {
        this.values = values;
    }
}
