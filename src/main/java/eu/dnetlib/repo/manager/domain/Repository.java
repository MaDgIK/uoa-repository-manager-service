package eu.dnetlib.repo.manager.domain;

import eu.dnetlib.domain.data.DataCollectionType;
import eu.dnetlib.domain.data.PiwikInfo;

import java.util.ArrayList;
import java.util.List;

/**
 * The domain object for the Repository resource data structure
 *
 */
public class Repository extends DatasourceDetails {
    private static final long serialVersionUID = -7241644234046760972L;

    private List<RepositoryInterface> interfaces = new ArrayList<>();

    private static List<DataCollectionType> dataCollectionTypes = new ArrayList<>();

    private PiwikInfo piwikInfo;

    public Repository() {
    }

    public List<RepositoryInterface> getInterfaces() {
        return interfaces;
    }

    public void setInterfaces(List<RepositoryInterface> interfaces) {
        this.interfaces = interfaces;
    }

    public Double getTimezone() {
        return timezone;
    }

    public void setTimezone(Double timezone) throws IllegalArgumentException {
        if (timezone < -12 || timezone > 12 || (timezone % 0.5) != 0) {
            String t = String.valueOf(timezone);
            throw new IllegalArgumentException(
                    "timezone must be in the range [-12.0, 12.0] and must me divided by 0.5. Value given is "
                            + t);
        }
        this.timezone = timezone;
    }

    public List<DataCollectionType> getDataCollectionTypes() {
        return dataCollectionTypes;
    }

    public void setDataCollectionTypes(
            List<DataCollectionType> dataCollectionTypes) {
        this.dataCollectionTypes = dataCollectionTypes;
    }

    public PiwikInfo getPiwikInfo() {
        return piwikInfo;
    }

    public void setPiwikInfo(PiwikInfo piwikInfo) {
        this.piwikInfo = piwikInfo;
    }

    @Override
    public int hashCode() {
        return officialname.hashCode();
    }

    @Override
    public boolean equals(Object o) {
        if (!(o instanceof Repository))
            return false;
        else
            return this.equals((Repository) o);
    }

    public boolean equals(Repository r) {
        // TODO: fill with required fields...

        if (this.getEnglishname() != null && r.getEnglishname() == null) {
            return false;
        } else if (this.getEnglishname() == null
                && r.getEnglishname() != null) {
            return false;
        } else if (this.getEnglishname() != null
                && r.getEnglishname() != null) {
            return this.getEnglishname().equals(r.getEnglishname());
        }

        return true;
    }
}
