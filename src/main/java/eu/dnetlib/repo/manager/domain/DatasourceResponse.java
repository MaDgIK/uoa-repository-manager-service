package eu.dnetlib.repo.manager.domain;

import com.google.common.collect.Lists;

import java.util.List;

public class DatasourceResponse extends Response {

    private List<DatasourceDetails> datasourceInfo = Lists.newArrayList();

    public DatasourceResponse addDatasourceInfo(final DatasourceDetails datasourceInfo) {
        getDatasourceInfo().add(datasourceInfo);
        return this;
    }

    public List<DatasourceDetails> getDatasourceInfo() {
        return datasourceInfo;
    }

    public DatasourceResponse setDatasourceInfo(final List<DatasourceDetails> datasourceInfo) {
        this.datasourceInfo = datasourceInfo;
        return this;
    }
}
