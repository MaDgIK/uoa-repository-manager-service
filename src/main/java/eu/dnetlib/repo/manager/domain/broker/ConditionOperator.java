package eu.dnetlib.repo.manager.domain.broker;



/**
 * Created by stefanos on 17/3/2017.
 */
public enum ConditionOperator   {
    EXACT, MATCH_ANY, MATCH_ALL, RANGE
}
