package eu.dnetlib.repo.manager.domain;


import eu.dnetlib.domain.data.PiwikInfo;

import java.util.Date;
import java.util.Set;

public class RepositorySnippet {

    // Do not refactor names to keep compatibility with external api.
    private String id;
    private String officialname;
    private String englishname;
    private String websiteurl;

    private String registeredby;
    private Date registrationdate;
    private String eoscDatasourceType;
    private String logoUrl;
    private String description;
    private Boolean consentTermsOfUse;
    private Date consentTermsOfUseDate;
    private Date lastConsentTermsOfUseDate;
    private Boolean fullTextDownload;
    private Set<OrganizationDetails> organizations;
    private String typology;


    private PiwikInfo piwikInfo;

    public RepositorySnippet() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getOfficialname() {
        return officialname;
    }

    public void setOfficialname(String officialname) {
        this.officialname = officialname;
    }

    public String getEnglishname() {
        return englishname;
    }

    public void setEnglishname(String englishname) {
        this.englishname = englishname;
    }

    public String getWebsiteurl() {
        return websiteurl;
    }

    public void setWebsiteurl(String websiteurl) {
        this.websiteurl = websiteurl;
    }

    public String getRegisteredby() {
        return registeredby;
    }

    public void setRegisteredby(String registeredby) {
        this.registeredby = registeredby;
    }

    public String getTypology() {
        return typology;
    }

    public void setTypology(String typology) {
        this.typology = typology;
    }

    public Set<OrganizationDetails> getOrganizations() {
        return organizations;
    }

    public void setOrganizations(Set<OrganizationDetails> organizations) {
        this.organizations = organizations;
    }
    public Date getRegistrationdate() {
        return registrationdate;
    }

    public void setRegistrationdate(Date registrationdate) {
        this.registrationdate = registrationdate;
    }

    public PiwikInfo getPiwikInfo() {
        return piwikInfo;
    }

    public void setPiwikInfo(PiwikInfo piwikInfo) {
        this.piwikInfo = piwikInfo;
    }

    public String getLogoUrl() {
        return logoUrl;
    }

    public void setLogoUrl(String logoUrl) {
        this.logoUrl = logoUrl;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Boolean getFullTextDownload() {
        return fullTextDownload;
    }

    public void setFullTextDownload(Boolean fullTextDownload) {
        this.fullTextDownload = fullTextDownload;
    }

    public Boolean getConsentTermsOfUse() {
        return consentTermsOfUse;
    }

    public void setConsentTermsOfUse(Boolean consentTermsOfUse) {
        this.consentTermsOfUse = consentTermsOfUse;
    }

    public Date getConsentTermsOfUseDate() {
        return consentTermsOfUseDate;
    }

    public void setConsentTermsOfUseDate(Date consentTermsOfUseDate) {
        this.consentTermsOfUseDate = consentTermsOfUseDate;
    }

    public Date getLastConsentTermsOfUseDate() {
        return lastConsentTermsOfUseDate;
    }

    public void setLastConsentTermsOfUseDate(Date lastConsentTermsOfUseDate) {
        this.lastConsentTermsOfUseDate = lastConsentTermsOfUseDate;
    }

    public String getEoscDatasourceType() {
        return eoscDatasourceType;
    }

    public void setEoscDatasourceType(String eoscDatasourceType) {
        this.eoscDatasourceType = eoscDatasourceType;
    }
}
