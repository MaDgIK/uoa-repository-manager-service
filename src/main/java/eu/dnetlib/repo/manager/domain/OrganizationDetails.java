package eu.dnetlib.repo.manager.domain;

public class OrganizationDetails {

    private String legalshortname;
    private String legalname;
    private String websiteurl;
    private String logourl;
    private String country;

    public OrganizationDetails() {
        // no arg constructor
    }

    public String getLegalshortname() {
        return legalshortname;
    }

    public void setLegalshortname(String legalshortname) {
        this.legalshortname = legalshortname;
    }

    public String getLegalname() {
        return legalname;
    }

    public void setLegalname(String legalname) {
        this.legalname = legalname;
    }

    public String getWebsiteurl() {
        return websiteurl;
    }

    public void setWebsiteurl(String websiteurl) {
        this.websiteurl = websiteurl;
    }

    public String getLogourl() {
        return logourl;
    }

    public void setLogourl(String logourl) {
        this.logourl = logourl;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }
}
