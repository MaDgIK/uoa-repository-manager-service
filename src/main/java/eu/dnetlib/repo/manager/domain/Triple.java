package eu.dnetlib.repo.manager.domain;



/**
 * Created by stefania on 3/8/16.
 */
public class Triple<K, V, L> extends Tuple<K, V>   {

    private L third;

    public Triple() {
    }

    public Triple(K first, V second, L third) {
        super(first, second);
        this.third = third;
    }

    public L getThird() {
        return third;
    }

    public void setThird(L third) {
        this.third = third;
    }
}
