package eu.dnetlib.repo.manager.domain;

import eu.dnetlib.repo.manager.domain.broker.BrowseEntry;
import eu.dnetlib.repo.manager.domain.broker.SimpleSubscriptionDesc;

import java.util.List;
import java.util.Map;

public class BrokerSummary {
    private Map<String, List<SimpleSubscriptionDesc>> userSubs;

    private List<BrowseEntry> topicsForDatasource;

    public BrokerSummary(){}

    public BrokerSummary(Map<String, List<SimpleSubscriptionDesc>> userSubs, List<BrowseEntry> topicsForDatasource) {
        this.userSubs = userSubs;
        this.topicsForDatasource = topicsForDatasource;
    }

    public Map<String, List<SimpleSubscriptionDesc>> getUserSubs() {
        return userSubs;
    }

    public void setUserSubs(Map<String, List<SimpleSubscriptionDesc>> userSubs) {
        this.userSubs = userSubs;
    }

    public List<BrowseEntry> getTopicsForDatasource() {
        return topicsForDatasource;
    }

    public void setTopicsForDatasource(List<BrowseEntry> topicsForDatasource) {
        this.topicsForDatasource = topicsForDatasource;
    }
}
