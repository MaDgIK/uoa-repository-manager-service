package eu.dnetlib.repo.manager.domain;

import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;

public class RepositoryInterface extends ApiDetails {

    private static final long serialVersionUID = 8013272950607614479L;

    public void updateApiParam(String param, String value) {
        for (ApiParamDetails entry : apiParams) {
            if (entry.getParam().equals(param)) {
                entry.setValue(value);
                return;
            }
        }
        ApiParamDetails newSet = new ApiParamDetails();
        newSet.setParam(param);
        newSet.setValue(value);
        this.apiParams.add(newSet);
    }

    public void setAccessSet(String accessSet) {
        if (accessSet != null) {
            updateApiParam("set", accessSet);
        }
    }

    public String getAccessSet() {
        Map<String, String> map;
        if (apiParams != null) {
            map = apiParams.stream()
                    .filter(Objects::nonNull)
                    .filter(k -> k.getParam() != null && k.getValue() != null)
                    .collect(Collectors.toMap(ApiParamDetails::getParam, ApiParamDetails::getValue));
            return map.get("set");
        }
        return null;
    }

    public void setAccessFormat(String accessFormat) {
        updateApiParam("format", accessFormat);
    }

    public String getAccessFormat() {
        Map<String, String> map;
        if (apiParams != null) {
            map = apiParams.stream()
                    .filter(Objects::nonNull)
                    .filter(k -> k.getParam() != null && k.getValue() != null)
                    .collect(Collectors.toMap(ApiParamDetails::getParam, ApiParamDetails::getValue));
            return map.get("format");
        }
        return null;
    }
}
