package eu.dnetlib.repo.manager.domain.broker;

/**
 * Created by stefanos on 10-Mar-17.
 */


import java.util.Date;
import java.util.List;


public class Subscription{

    private String subscriptionId;

    private String subscriber;

    private String topic;

    private NotificationFrequency frequency;

    private NotificationMode mode;

    private Date lastNotificationDate;

    private Date creationDate;

    private String conditions;

    private List<MapConditions> conditionsAsList;

    public Subscription() {
    }

    public String getSubscriptionId() {
        return subscriptionId;
    }

    public void setSubscriptionId(String subscriptionId) {
        this.subscriptionId = subscriptionId;
    }

    public String getSubscriber() {
        return subscriber;
    }

    public void setSubscriber(String subscriber) {
        this.subscriber = subscriber;
    }

    public String getTopic() {
        return topic;
    }

    public void setTopic(String topic) {
        this.topic = topic;
    }

    public NotificationFrequency getFrequency() {
        return frequency;
    }

    public void setFrequency(NotificationFrequency frequency) {
        this.frequency = frequency;
    }

    public NotificationMode getMode() {
        return mode;
    }

    public void setMode(NotificationMode mode) {
        this.mode = mode;
    }

    public Date getLastNotificationDate() {
        return lastNotificationDate;
    }

    public void setLastNotificationDate(Date lastNotificationDate) {
        this.lastNotificationDate = lastNotificationDate;
    }

    public String getConditions() {
        return conditions;
    }

    public void setConditions(String conditions) {
        this.conditions = conditions;
    }

    public List<MapConditions> getConditionsAsList() {
        return conditionsAsList;
    }

    public void setConditionsAsList(List<MapConditions> conditionsAsList) {
        this.conditionsAsList = conditionsAsList;
    }

    public Date getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }

}
