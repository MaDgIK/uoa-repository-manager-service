package eu.dnetlib.repo.manager.domain;



/**
 * Created by stefania on 3/8/16.
 */
public class Vocabulary   {

    private String id;
    private String name;

    public Vocabulary() {
    }

    public Vocabulary(String id, String name) {
        this.id = id;
        this.name = name;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
