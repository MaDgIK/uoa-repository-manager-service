package eu.dnetlib.repo.manager.domain;

import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class RequestFilter{

    private String registeredby = null;
    private String typology = null;
    private String eoscDatasourceType = null;
    private String country = null;
    private String id = null;
    private String officialname = null;
    private String englishname = null;
    private String collectedfrom = null;




    public RequestFilter() {

    }

    public String getTypology() {
        return typology;
    }

    public void setTypology(String typology) {
        this.typology = typology;
    }

    public String getEoscDatasourceType() {
        return eoscDatasourceType;
    }

    public void setEoscDatasourceType(String eoscDatasourceType) {
        this.eoscDatasourceType = eoscDatasourceType;
    }

    public String getRegisteredby() {
        return registeredby;
    }

    public void setRegisteredby(String registeredby) {
        this.registeredby = registeredby;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getOfficialname() {
        return officialname;
    }

    public void setOfficialname(String officialname) {
        this.officialname = officialname;
    }

    public String getCollectedfrom() {
        return collectedfrom;
    }

    public void setCollectedfrom(String collectedfrom) {
        this.collectedfrom = collectedfrom;
    }

    public String getEnglishname() {
        return englishname;
    }

    public void setEnglishname(String englishname) {
        this.englishname = englishname;
    }
}
