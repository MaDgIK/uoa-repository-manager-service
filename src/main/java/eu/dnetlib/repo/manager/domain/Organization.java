package eu.dnetlib.repo.manager.domain;



public class Organization{
    private String country;
    private String legalname;
    private String websiteurl;
    private String legalshortname;
    private String logourl;

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getLegalname() {
        return legalname;
    }

    public void setLegalname(String legalname) {
        this.legalname = legalname;
    }

    public String getWebsiteurl() {
        return websiteurl;
    }

    public void setWebsiteurl(String websiteurl) {
        this.websiteurl = websiteurl;
    }

    public String getLegalshortname() {
        return legalshortname;
    }

    public void setLegalshortname(String legalshortname) {
        this.legalshortname = legalshortname;
    }

    public String getLogourl() {
        return logourl;
    }

    public void setLogourl(String logourl) {
        this.logourl = logourl;
    }
}
