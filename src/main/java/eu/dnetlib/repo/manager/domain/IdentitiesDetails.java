package eu.dnetlib.repo.manager.domain;

public class IdentitiesDetails {
    private String pid;
    private String issuertype;

    public IdentitiesDetails() {
        // no arg constructor
    }

    public String getPid() {
        return pid;
    }

    public String getIssuertype() {
        return issuertype;
    }

    public IdentitiesDetails setPid(final String pid) {
        this.pid = pid;
        return this;
    }

    public IdentitiesDetails setIssuertype(final String issuertype) {
        this.issuertype = issuertype;
        return this;
    }
}

