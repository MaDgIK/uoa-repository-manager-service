package eu.dnetlib.repo.manager.domain;

import java.util.List;

public class ApiDetailsResponse extends Response {

    private List<ApiDetails> api;


    public List<ApiDetails> getApi() {
        return api;
    }

    public ApiDetailsResponse setApi(final List<ApiDetails> api) {
        this.api = api;
        return this;
    }
}

