package eu.dnetlib.repo.manager.domain;

import java.util.Date;
import java.util.Set;

public class ApiDetails {
    protected String id = null;
    protected String protocol = null;
    protected String datasource = null;
    protected String contentdescription = null;
    protected String eoscDatasourceType = null;
    protected String compatibility;
    protected String compatibilityOverride;
    protected Integer lastCollectionTotal;
    protected Date lastCollectionDate;
    protected Integer lastAggregationTotal;
    protected Date lastAggregationDate;
    protected Integer lastDownloadTotal;
    protected Date lastDownloadDate;
    protected String baseurl;
    protected Boolean removable = false;
    protected Set<ApiParamDetails> apiParams;
    protected String metadataIdentifierPath = "";
    @Deprecated
    protected String typology = null;


    public String getId() {
        return id;
    }

    public String getProtocol() {
        return protocol;
    }

    public String getDatasource() {
        return datasource;
    }

    public String getContentdescription() {
        return contentdescription;
    }

    public String getCompatibility() {
        return compatibility;
    }

    public Integer getLastCollectionTotal() {
        return lastCollectionTotal;
    }

    public Date getLastCollectionDate() {
        return lastCollectionDate;
    }

    public Integer getLastAggregationTotal() {
        return lastAggregationTotal;
    }

    public Date getLastAggregationDate() {
        return lastAggregationDate;
    }

    public Integer getLastDownloadTotal() {
        return lastDownloadTotal;
    }

    public Date getLastDownloadDate() {
        return lastDownloadDate;
    }

    public String getBaseurl() {
        return baseurl;
    }

    public ApiDetails setId(final String id) {
        this.id = id;
        return this;
    }

    public ApiDetails setProtocol(final String protocol) {
        this.protocol = protocol;
        return this;
    }

    public ApiDetails setDatasource(final String datasource) {
        this.datasource = datasource;
        return this;
    }

    public ApiDetails setContentdescription(final String contentdescription) {
        this.contentdescription = contentdescription;
        return this;
    }

    public ApiDetails setCompatibility(final String compatibility) {
        this.compatibility = compatibility;
        return this;
    }

    public ApiDetails setLastCollectionTotal(final Integer lastCollectionTotal) {
        this.lastCollectionTotal = lastCollectionTotal;
        return this;
    }

    public ApiDetails setLastCollectionDate(final Date lastCollectionDate) {
        this.lastCollectionDate = lastCollectionDate;
        return this;
    }

    public ApiDetails setLastAggregationTotal(final Integer lastAggregationTotal) {
        this.lastAggregationTotal = lastAggregationTotal;
        return this;
    }

    public ApiDetails setLastAggregationDate(final Date lastAggregationDate) {
        this.lastAggregationDate = lastAggregationDate;
        return this;
    }

    public ApiDetails setLastDownloadTotal(final Integer lastDownloadTotal) {
        this.lastDownloadTotal = lastDownloadTotal;
        return this;
    }

    public ApiDetails setLastDownloadDate(final Date lastDownloadDate) {
        this.lastDownloadDate = lastDownloadDate;
        return this;
    }

    public ApiDetails setBaseurl(final String baseurl) {
        this.baseurl = baseurl;
        return this;
    }

    public Set<ApiParamDetails> getApiParams() {
        return apiParams;
    }

    public void setApiParams(final Set<ApiParamDetails> apiParams) {
        this.apiParams = apiParams;
    }

    public String getCompatibilityOverride() {
        return compatibilityOverride;
    }

    public ApiDetails setCompatibilityOverride(final String compatibilityOverride) {
        this.compatibilityOverride = compatibilityOverride;
        return this;
    }

    public Boolean getRemovable() {
        return removable;
    }

    public ApiDetails setRemovable(final Boolean removable) {
        this.removable = removable;
        return this;
    }

    public String getMetadataIdentifierPath() {
        return metadataIdentifierPath;
    }

    public ApiDetails setMetadataIdentifierPath(final String metadataIdentifierPath) {
        this.metadataIdentifierPath = metadataIdentifierPath;
        return this;
    }

    public String getEoscDatasourceType() {
        return eoscDatasourceType;
    }

    public ApiDetails setEoscDatasourceType(final String eoscDatasourceType) {
        this.eoscDatasourceType = eoscDatasourceType;
        return this;
    }

    public String getTypology() {
        return typology;
    }

    public ApiDetails setTypology(final String typology) {
        this.typology = typology;
        return this;
    }
}
