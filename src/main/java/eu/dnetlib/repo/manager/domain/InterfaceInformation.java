package eu.dnetlib.repo.manager.domain;


import java.util.List;

/**
 * Created by nikonas on 7/1/16.
 */
public class InterfaceInformation{

    private boolean identified;
    private List<String> sets;
    private List<String> adminEmails;

    public InterfaceInformation() {
    }

    public boolean isIdentified() {
        return identified;
    }

    public void setIdentified(boolean identified) {
        this.identified = identified;
    }

    public List<String> getSets() {
        return sets;
    }

    public void setSets(List<String> sets) {
        this.sets = sets;
    }

    public List<String> getAdminEmails() {
        return adminEmails;
    }

    public void setAdminEmails(List<String> adminEmails) {
        this.adminEmails = adminEmails;
    }
}
