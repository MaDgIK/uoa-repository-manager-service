package eu.dnetlib.repo.manager.domain.dto;

import org.mitre.openid.connect.model.UserInfo;

public class User {

    private String sub;
    private String firstName;
    private String lastName;
    private String email;

    public User() {}

    public User(String sub, String firstName, String lastName, String email) {
        this.sub = sub;
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
    }

    public static User from(UserInfo userInfo) {
        User user = new User();
        user.setSub(user.getSub());
        user.setFirstName(userInfo.getGivenName());
        user.setLastName(userInfo.getFamilyName());
        user.setEmail(userInfo.getEmail());
        return user;
    }

    public String getSub() {
        return sub;
    }

    public void setSub(String sub) {
        this.sub = sub;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
