package eu.dnetlib.repo.manager.domain.broker;



import java.util.Date;

/**
 * Created by stefanos on 10-Mar-17.
 */
public class SimpleSubscriptionDesc   {

    private String id;
    private String datasource;
    private String topic;
    private long count;
    private Date creationDate;
    private Date lastNotificationDate;

    public SimpleSubscriptionDesc() {
    }

    public SimpleSubscriptionDesc(final String id, final String datasource, final String topic, final long count) {
        this.id = id;
        this.datasource = datasource;
        this.topic = topic;
        this.count = count;
    }

    public String getId() {
        return this.id;
    }

    public void setId(final String id) {
        this.id = id;
    }

    public String getDatasource() {
        return this.datasource;
    }

    public void setDatasource(final String datasource) {
        this.datasource = datasource;
    }

    public String getTopic() {
        return this.topic;
    }

    public void setTopic(final String topic) {
        this.topic = topic;
    }

    public long getCount() {
        return this.count;
    }

    public void setCount(final long count) {
        this.count = count;
    }

    public Date getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }

    public Date getLastNotificationDate() {
        return lastNotificationDate;
    }

    public void setLastNotificationDate(Date lastNotificationDate) {
        this.lastNotificationDate = lastNotificationDate;
    }
}