package eu.dnetlib.repo.manager.domain.broker;


/**
 * Created by stefanos on 17/3/2017.
 */
public enum MapValueType {
    STRING, INTEGER, FLOAT, DATE, BOOLEAN, LIST_STRING, LIST_INTEGER, LIST_FLOAT, LIST_DATE, LIST_BOOLEAN;
}
