package eu.dnetlib.repo.manager.domain;

public enum OrderByField {

    REPOSITORY_NAME("repositoryname"),
    REPOSITORY_ID("repositoryid"),
    SITE_ID("siteid"),
    CREATION_DATE("creationdate"),
    VALIDATION_DATE("validationdate"),
    REQUESTOR_NAME("requestorname"),
    REQUESTOR_EMAIL("requestoremail"),
    VALIDATED("validated"),
    COUNTRY("country");


    private final String text;

    OrderByField(String text) {
        this.text = text;
    }

    @Override
    public String toString() {
        return text;
    }
}
