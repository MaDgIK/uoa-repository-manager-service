package eu.dnetlib.repo.manager.domain;

import eu.dnetlib.domain.data.PiwikInfo;

public class UsageSummary {
    private MetricsInfo metricsInfo;

    private PiwikInfo piwikInfo;

    public UsageSummary() {
    }

    public UsageSummary(MetricsInfo metricsInfo, PiwikInfo piwikInfo) {
        this.metricsInfo = metricsInfo;
        this.piwikInfo = piwikInfo;
    }

    public MetricsInfo getMetricsInfo() {
        return metricsInfo;
    }

    public void setMetricsInfo(MetricsInfo metricsInfo) {
        this.metricsInfo = metricsInfo;
    }

    public PiwikInfo getPiwikInfo() {
        return piwikInfo;
    }

    public void setPiwikInfo(PiwikInfo piwikInfo) {
        this.piwikInfo = piwikInfo;
    }
}
