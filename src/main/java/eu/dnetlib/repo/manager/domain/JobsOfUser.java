package eu.dnetlib.repo.manager.domain;


import eu.dnetlib.domain.functionality.validator.StoredJob;

import java.util.List;

/**
 * Created by nikonas on 29/3/16.
 */
public class JobsOfUser   {

    private int totalJobs;
    private int totalJobsSuccessful;
    private int totalJobsFailed;
    private int totalJobsOngoing;

    private List<StoredJob> jobs;

    public int getTotalJobs() {
        return totalJobs;
    }

    public void setTotalJobs(int totalJobs) {
        this.totalJobs = totalJobs;
    }

    public int getTotalJobsSuccessful() {
        return totalJobsSuccessful;
    }

    public void setTotalJobsSuccessful(int totalJobsSuccessful) {
        this.totalJobsSuccessful = totalJobsSuccessful;
    }

    public int getTotalJobsFailed() {
        return totalJobsFailed;
    }

    public void setTotalJobsFailed(int totalJobsFailed) {
        this.totalJobsFailed = totalJobsFailed;
    }

    public int getTotalJobsOngoing() {
        return totalJobsOngoing;
    }

    public void setTotalJobsOngoing(int totalJobsOngoing) {
        this.totalJobsOngoing = totalJobsOngoing;
    }

    public List<StoredJob> getJobs() {
        return jobs;
    }

    public void setJobs(List<StoredJob> jobs) {
        this.jobs = jobs;
    }
}
