package eu.dnetlib.repo.manager.domain;

import java.io.Serializable;

public class InterfaceComplianceRequestId implements Serializable {
    String repositoryId;
    String interfaceId;

    public String getRepositoryId() {
        return repositoryId;
    }

    public void setRepositoryId(String repositoryId) {
        this.repositoryId = repositoryId;
    }

    public String getInterfaceId() {
        return interfaceId;
    }

    public void setInterfaceId(String interfaceId) {
        this.interfaceId = interfaceId;
    }

    public static InterfaceComplianceRequestId of(String repositoryId, String interfaceId) {
        InterfaceComplianceRequestId id = new InterfaceComplianceRequestId();
        id.setRepositoryId(repositoryId);
        id.setInterfaceId(interfaceId);
        return id;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof InterfaceComplianceRequestId)) return false;

        InterfaceComplianceRequestId that = (InterfaceComplianceRequestId) o;

        if (getRepositoryId() != null ? !getRepositoryId().equals(that.getRepositoryId()) : that.getRepositoryId() != null)
            return false;
        return getInterfaceId() != null ? getInterfaceId().equals(that.getInterfaceId()) : that.getInterfaceId() == null;
    }

    @Override
    public int hashCode() {
        int result = getRepositoryId() != null ? getRepositoryId().hashCode() : 0;
        result = 31 * result + (getInterfaceId() != null ? getInterfaceId().hashCode() : 0);
        return result;
    }
}
