package eu.dnetlib.repo.manager.domain.broker;

/**
 * Created by stefanos on 26/10/2016.
 */
public class BrowseEntry implements Comparable<BrowseEntry>{

    private String value;
    private Long size;

    public BrowseEntry() {
    }

    public BrowseEntry(final String value, final Long size) {
        this.value = value;
        this.size = size;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public Long getSize() {
        return size;
    }

    public void setSize(Long size) {
        this.size = size;
    }

    @Override
    public int compareTo(final BrowseEntry bv) {
        return Long.compare(getSize(), bv.getSize());
    }

}
