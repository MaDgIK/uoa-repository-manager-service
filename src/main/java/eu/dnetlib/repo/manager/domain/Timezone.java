package eu.dnetlib.repo.manager.domain;


public class Timezone{
	private String name;
	private double offset;

	public Timezone(){
	}

	public Timezone(String name, double offset) {
		super();
		this.name = name;
		this.offset = offset;
	}
}
