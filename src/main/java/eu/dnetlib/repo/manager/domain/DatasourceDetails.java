package eu.dnetlib.repo.manager.domain;

import javax.persistence.Transient;
import java.util.Date;
import java.util.Set;

public class DatasourceDetails {

    protected String id;
    @Transient
    protected String openaireId;
    protected String officialname;
    protected String englishname;
    protected String websiteurl;
    protected String logourl;
    protected String contactemail;
    protected Double latitude = 0.0;
    protected Double longitude = 0.0;
    protected Double timezone = 0.0;
    protected String namespaceprefix;
    protected String languages;
    protected Date dateofvalidation;
    protected String eoscDatasourceType;
    protected Date dateofcollection;
    protected String platform;
    protected String activationId;
    protected String description;
    protected String issn;
    protected String eissn;
    protected String lissn;
    protected String registeredby;
    protected String subjects;
    protected String aggregator = "OPENAIRE";
    protected String collectedfrom;
    protected Boolean managed;
    protected Boolean consentTermsOfUse;
    protected Boolean fullTextDownload;
    protected Date consentTermsOfUseDate;
    protected Date lastConsentTermsOfUseDate;
    protected Set<OrganizationDetails> organizations;
    protected Set<IdentitiesDetails> identities;
    protected String status;
    protected String typology;
    protected Date registrationdate;

    public DatasourceDetails() {
        // no arg constructor
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getOpenaireId() {
        return openaireId;
    }

    public void setOpenaireId(String openaireId) {
        this.openaireId = openaireId;
    }

    public String getOfficialname() {
        return officialname;
    }

    public void setOfficialname(String officialname) {
        this.officialname = officialname;
    }

    public String getEnglishname() {
        return englishname;
    }

    public void setEnglishname(String englishname) {
        this.englishname = englishname;
    }

    public String getWebsiteurl() {
        return websiteurl;
    }

    public void setWebsiteurl(String websiteurl) {
        this.websiteurl = websiteurl;
    }

    public String getLogourl() {
        return logourl;
    }

    public void setLogourl(String logourl) {
        this.logourl = logourl;
    }

    public String getContactemail() {
        return contactemail;
    }

    public void setContactemail(String contactemail) {
        this.contactemail = contactemail;
    }

    public Double getLatitude() {
        return latitude;
    }

    public void setLatitude(Double latitude) {
        this.latitude = latitude;
    }

    public Double getLongitude() {
        return longitude;
    }

    public void setLongitude(Double longitude) {
        this.longitude = longitude;
    }

    public String getNamespaceprefix() {
        return namespaceprefix;
    }

    public void setNamespaceprefix(String namespaceprefix) {
        this.namespaceprefix = namespaceprefix;
    }

    public String getLanguages() {
        return languages;
    }

    public void setLanguages(String languages) {
        this.languages = languages;
    }

    public Date getDateofvalidation() {
        return dateofvalidation;
    }

    public void setDateofvalidation(Date dateofvalidation) {
        this.dateofvalidation = dateofvalidation;
    }

    public String getEoscDatasourceType() {
        return eoscDatasourceType;
    }

    public void setEoscDatasourceType(String eoscDatasourceType) {
        this.eoscDatasourceType = eoscDatasourceType;
    }

    public Date getDateofcollection() {
        return dateofcollection;
    }

    public void setDateofcollection(Date dateofcollection) {
        this.dateofcollection = dateofcollection;
    }

    public String getPlatform() {
        return platform;
    }

    public void setPlatform(String platform) {
        this.platform = platform;
    }

    public String getActivationId() {
        return activationId;
    }

    public void setActivationId(String activationId) {
        this.activationId = activationId;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getIssn() {
        return issn;
    }

    public void setIssn(String issn) {
        this.issn = issn;
    }

    public String getEissn() {
        return eissn;
    }

    public void setEissn(String eissn) {
        this.eissn = eissn;
    }

    public String getLissn() {
        return lissn;
    }

    public void setLissn(String lissn) {
        this.lissn = lissn;
    }

    public String getRegisteredby() {
        return registeredby;
    }

    public void setRegisteredby(String registeredby) {
        this.registeredby = registeredby;
    }

    public String getSubjects() {
        return subjects;
    }

    public void setSubjects(String subjects) {
        this.subjects = subjects;
    }

    public String getAggregator() {
        return aggregator;
    }

    public void setAggregator(String aggregator) {
        this.aggregator = aggregator;
    }

    public String getCollectedfrom() {
        return collectedfrom;
    }

    public void setCollectedfrom(String collectedfrom) {
        this.collectedfrom = collectedfrom;
    }

    public Boolean getManaged() {
        return managed;
    }

    public void setManaged(Boolean managed) {
        this.managed = managed;
    }

    public Boolean getConsentTermsOfUse() {
        return consentTermsOfUse;
    }

    public void setConsentTermsOfUse(Boolean consentTermsOfUse) {
        this.consentTermsOfUse = consentTermsOfUse;
    }

    public Boolean getFullTextDownload() {
        return fullTextDownload;
    }

    public void setFullTextDownload(Boolean fullTextDownload) {
        this.fullTextDownload = fullTextDownload;
    }

    public Date getConsentTermsOfUseDate() {
        return consentTermsOfUseDate;
    }

    public void setConsentTermsOfUseDate(Date consentTermsOfUseDate) {
        this.consentTermsOfUseDate = consentTermsOfUseDate;
    }

    public Date getLastConsentTermsOfUseDate() {
        return lastConsentTermsOfUseDate;
    }

    public void setLastConsentTermsOfUseDate(Date lastConsentTermsOfUseDate) {
        this.lastConsentTermsOfUseDate = lastConsentTermsOfUseDate;
    }

    public Set<OrganizationDetails> getOrganizations() {
        return organizations;
    }

    public void setOrganizations(Set<OrganizationDetails> organizations) {
        this.organizations = organizations;
    }

    public Set<IdentitiesDetails> getIdentities() {
        return identities;
    }

    public void setIdentities(Set<IdentitiesDetails> identities) {
        this.identities = identities;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getTypology() {
        return typology;
    }

    public void setTypology(String typology) {
        this.typology = typology;
    }

    public Date getRegistrationdate() {
        return registrationdate;
    }

    public void setRegistrationdate(Date registrationdate) {
        this.registrationdate = registrationdate;
    }
}
