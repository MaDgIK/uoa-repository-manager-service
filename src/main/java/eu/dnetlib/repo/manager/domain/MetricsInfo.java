package eu.dnetlib.repo.manager.domain;



/**
 * Created by stefania on 11/6/17.
 */
public class MetricsInfo   {

    private MetricsNumbers metricsNumbers;
    private String diagramsBaseURL;

    public MetricsInfo() {
    }

    public MetricsInfo(MetricsNumbers metricsNumbers, String diagramsBaseURL) {
        this.metricsNumbers = metricsNumbers;
        this.diagramsBaseURL = diagramsBaseURL;
    }

    public MetricsNumbers getMetricsNumbers() {
        return metricsNumbers;
    }

    public void setMetricsNumbers(MetricsNumbers metricsNumbers) {
        this.metricsNumbers = metricsNumbers;
    }

    public String getDiagramsBaseURL() {
        return diagramsBaseURL;
    }

    public void setDiagramsBaseURL(String diagramsBaseURL) {
        this.diagramsBaseURL = diagramsBaseURL;
    }
}
