package eu.dnetlib.repo.manager.domain.broker;

/**
 * Created by stefanos on 17/3/2017.
 */
public class ConditionParams {

    private String value;
    private String otherValue;

    public ConditionParams() {
    }

    public ConditionParams(final String value, final String otherValue) {
        this.value = value;
        this.otherValue = otherValue;
    }

    public String getValue() {
        return this.value;
    }

    public void setValue(final String value) {
        this.value = value;
    }

    public String getOtherValue() {
        return this.otherValue;
    }

    public void setOtherValue(final String otherValue) {
        this.otherValue = otherValue;
    }
}