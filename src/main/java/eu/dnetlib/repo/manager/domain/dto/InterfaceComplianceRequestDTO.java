package eu.dnetlib.repo.manager.domain.dto;

import java.util.Date;

public class InterfaceComplianceRequestDTO {

    String repositoryId;
    String interfaceId;
    String desiredCompatibilityLevel;
    Date submissionDate;

    public InterfaceComplianceRequestDTO() {
        // no-arg constructor
    }

    public String getRepositoryId() {
        return repositoryId;
    }

    public void setRepositoryId(String repositoryId) {
        this.repositoryId = repositoryId;
    }

    public String getInterfaceId() {
        return interfaceId;
    }

    public void setInterfaceId(String interfaceId) {
        this.interfaceId = interfaceId;
    }

    public String getDesiredCompatibilityLevel() {
        return desiredCompatibilityLevel;
    }

    public void setDesiredCompatibilityLevel(String desiredCompatibilityLevel) {
        this.desiredCompatibilityLevel = desiredCompatibilityLevel;
    }

    public Date getSubmissionDate() {
        return submissionDate;
    }

    public void setSubmissionDate(Date submissionDate) {
        this.submissionDate = submissionDate;
    }
}
