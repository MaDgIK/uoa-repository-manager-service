package eu.dnetlib.repo.manager.domain;

import eu.dnetlib.openaire.exporter.model.dsm.AggregationInfo;

import java.util.List;

public class CollectionMonitorSummary {
    private List<AggregationInfo> aggregationInfo;

    private AggregationInfo lastIndexedVersion;

    public CollectionMonitorSummary(){}

    public CollectionMonitorSummary(List<AggregationInfo> aggregationInfo, AggregationInfo lastIndexedVersion) {
        this.aggregationInfo = aggregationInfo;
        this.lastIndexedVersion = lastIndexedVersion;
    }

    public List<AggregationInfo> getAggregationInfo() {
        return aggregationInfo;
    }

    public void setAggregationInfo(List<AggregationInfo> aggregationInfo) {
        this.aggregationInfo = aggregationInfo;
    }

    public AggregationInfo getLastIndexedVersion() {
        return lastIndexedVersion;
    }

    public void setLastIndexedVersion(AggregationInfo lastIndexedVersion) {
        this.lastIndexedVersion = lastIndexedVersion;
    }
}
