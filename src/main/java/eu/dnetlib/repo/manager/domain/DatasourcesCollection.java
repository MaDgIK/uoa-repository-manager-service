package eu.dnetlib.repo.manager.domain;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by nikonas on 14/1/16.
 */
public class DatasourcesCollection{

    private List<Repository> datasourcesOfUser;
    private List<Repository> sharedDatasources;
    private List<Repository> datasourcesOfOthers;

    public DatasourcesCollection() {
        this.datasourcesOfOthers = new ArrayList<Repository>();
        this.datasourcesOfUser = new ArrayList<Repository>();
        this.datasourcesOfOthers = new ArrayList<Repository>();
    }

    public List<Repository> getDatasourcesOfUser() {
        return datasourcesOfUser;
    }

    public void setDatasourcesOfUser(List<Repository> datasourcesOfUser) {
        this.datasourcesOfUser = datasourcesOfUser;
    }

    public List<Repository> getDatasourcesOfOthers() {
        return datasourcesOfOthers;
    }

    public void setDatasourcesOfOthers(List<Repository> datasourcesOfOthers) {
        this.datasourcesOfOthers = datasourcesOfOthers;
    }

    public List<Repository> getSharedDatasources() {
        return sharedDatasources;
    }

    public void setSharedDatasources(List<Repository> sharedDatasources) {
        this.sharedDatasources = sharedDatasources;
    }
}
