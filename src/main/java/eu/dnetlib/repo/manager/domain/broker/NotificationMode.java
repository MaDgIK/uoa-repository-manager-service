package eu.dnetlib.repo.manager.domain.broker;



/**
 * Created by stefanos on 10-Mar-17.
 */
public enum NotificationMode   {
    MOCK, EMAIL
}