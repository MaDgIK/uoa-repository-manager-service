package eu.dnetlib.repo.manager.domain.broker;

/**
 * Created by stefanos on 26/10/2016.
 */
public class Range{

    private String min;
    private String max;

    public Range() {
    }

    public Range(final String min, final String max) {
        super();
        this.min = min;
        this.max = max;
    }

    public String getMin() {
        return this.min;
    }

    public void setMin(final String min) {
        this.min = min;
    }

    public String getMax() {
        return this.max;
    }

    public void setMax(final String max) {
        this.max = max;
    }

}
