package eu.dnetlib.repo.manager.domain;

import eu.dnetlib.repo.manager.domain.dto.InterfaceComplianceRequestDTO;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.Transient;
import java.util.Date;

@Entity
@IdClass(InterfaceComplianceRequestId.class)
public class InterfaceComplianceRequest {

    @Id
    String repositoryId;
    @Id
    String interfaceId;
    String desiredCompatibilityLevel;
    Date submissionDate;

    public InterfaceComplianceRequest() {
        this.submissionDate = new Date();
    }

    public InterfaceComplianceRequest(String repositoryId, String interfaceId, String desiredCompatibilityLevel) {
        this.submissionDate = new Date();
        this.repositoryId = repositoryId;
        this.interfaceId = interfaceId;
        this.desiredCompatibilityLevel = desiredCompatibilityLevel;
    }

    public static InterfaceComplianceRequest from(InterfaceComplianceRequestDTO dto) {
        InterfaceComplianceRequest request = new InterfaceComplianceRequest();
        request.setRepositoryId(dto.getRepositoryId());
        request.setInterfaceId(dto.getInterfaceId());
        if (dto.getSubmissionDate() != null) {
            request.setSubmissionDate(dto.getSubmissionDate());
        } else {
            request.setSubmissionDate(new Date());
        }
        request.setDesiredCompatibilityLevel(dto.getDesiredCompatibilityLevel());
        return request;
    }

    @Transient
    public InterfaceComplianceRequestId getId() {
        InterfaceComplianceRequestId id = new InterfaceComplianceRequestId();
        id.repositoryId = this.repositoryId;
        id.interfaceId = this.interfaceId;
        return id;
    }

    public String getRepositoryId() {
        return repositoryId;
    }

    public void setRepositoryId(String repositoryId) {
        this.repositoryId = repositoryId;
    }

    public String getInterfaceId() {
        return interfaceId;
    }

    public void setInterfaceId(String interfaceId) {
        this.interfaceId = interfaceId;
    }

    public String getDesiredCompatibilityLevel() {
        return desiredCompatibilityLevel;
    }

    public void setDesiredCompatibilityLevel(String desiredCompatibilityLevel) {
        this.desiredCompatibilityLevel = desiredCompatibilityLevel;
    }

    public Date getSubmissionDate() {
        return submissionDate;
    }

    public void setSubmissionDate(Date submissionDate) {
        this.submissionDate = submissionDate;
    }

    @Override
    public String toString() {
        return "InterfaceComplianceRequest{" +
                "repositoryId='" + repositoryId + '\'' +
                ", interfaceId='" + interfaceId + '\'' +
                ", desiredCompatibilityLevel='" + desiredCompatibilityLevel + '\'' +
                ", submissionDate=" + submissionDate +
                '}';
    }
}
