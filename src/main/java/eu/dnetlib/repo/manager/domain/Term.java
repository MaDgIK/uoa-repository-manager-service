package eu.dnetlib.repo.manager.domain;



/**
 * Created by panagiotis on 15/1/2018.
 */
public class Term   {

    private String englishName;
    private String nativeName;
    private String encoding;
    private String code;

    public Term(String englishName, String nativeName, String encoding, String code) {
        this.englishName = englishName;
        this.nativeName = nativeName;
        this.encoding = encoding;
        this.code = code;
    }

    public String getEnglishName() {
        return englishName;
    }

    public void setEnglishName(String englishName) {
        this.englishName = englishName;
    }

    public String getNativeName() {
        return nativeName;
    }

    public void setNativeName(String nativeName) {
        this.nativeName = nativeName;
    }

    public String getEncoding() {
        return encoding;
    }

    public void setEncoding(String encoding) {
        this.encoding = encoding;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }
}
