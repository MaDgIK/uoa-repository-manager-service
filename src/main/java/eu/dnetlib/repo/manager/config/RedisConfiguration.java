package eu.dnetlib.repo.manager.config;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.connection.jedis.JedisConnectionFactory;
import org.springframework.session.data.redis.config.annotation.web.http.EnableRedisHttpSession;
import org.springframework.session.web.http.CookieSerializer;
import org.springframework.session.web.http.DefaultCookieSerializer;

import javax.annotation.PostConstruct;

@Configuration
@EnableRedisHttpSession
public class RedisConfiguration {

    private static final Logger logger = LoggerFactory.getLogger(RedisConfiguration.class);

    @Value("${services.provide.redis.host}")
    private String host;

    @Value("${services.provide.redis.port:6379}")
    private String port;

    @Value("${services.provide.redis.password}")
    private String password;

    @Value("${services.provide.aai.oidc.domain}")
    private String domain;

    @PostConstruct
    private void init() {
        logger.info(String.format("Redis : %s Port : %s Password : %s", host, port, password));
    }

    @Bean
    public JedisConnectionFactory connectionFactory() {
        logger.info(String.format("Redis : %s Port : %s Password : %s", host, port, password));
        JedisConnectionFactory jedisConnectionFactory = new JedisConnectionFactory();
        jedisConnectionFactory.setHostName(host);
        jedisConnectionFactory.setPort(Integer.parseInt(port));
        jedisConnectionFactory.setUsePool(true);
        if (password != null) jedisConnectionFactory.setPassword(password);
        return jedisConnectionFactory;
    }

    @Bean
    public CookieSerializer cookieSerializer() {
        DefaultCookieSerializer serializer = new DefaultCookieSerializer();
        serializer.setCookieName("openAIRESession");
        serializer.setCookiePath("/");
        serializer.setDomainName(domain);
        logger.info("Cookie Serializer : {}", serializer);
        return serializer;
    }

}
