package eu.dnetlib.repo.manager.config;

import org.mitre.oauth2.model.ClientDetailsEntity.AuthMethod;
import org.mitre.oauth2.model.RegisteredClient;
import org.mitre.openid.connect.client.OIDCAuthenticationFilter;
import org.mitre.openid.connect.client.OIDCAuthenticationProvider;
import org.mitre.openid.connect.client.service.impl.*;
import org.mitre.openid.connect.config.ServerConfiguration;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.web.authentication.preauth.AbstractPreAuthenticatedProcessingFilter;

import java.util.*;

@Configuration
@EnableWebSecurity
public class AaiSecurityConfiguration extends WebSecurityConfigurerAdapter {

    @Value("${services.provide.aai.oidc.webURL}")
    private String logoutSuccessUrl;

    @Value("${services.provide.aai.oidc.issuer}")
    private String oidcIssuer;

    @Value("${services.provide.aai.oidc.id}")
    private String oidcId;

    @Value("${services.provide.aai.oidc.secret}")
    private String oidcSecret;

    @Value("${services.provide.aai.oidc.redirectURL}")
    private String oidcDevHome;

    @Value("${services.provide.aai.oidc.webURL}")
    private String webAppFrontEnd;

    @Bean
    @Override
    public AuthenticationManager authenticationManagerBean() throws Exception {
        return authenticationManager();
    }

    @Override
    protected void configure(AuthenticationManagerBuilder auth) {
        auth.authenticationProvider(openIdConnectAuthenticationProvider());
    }

    @Override
    public void configure(WebSecurity web) {
        web.ignoring().antMatchers("/stats/**");
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http
                .csrf().disable()
                .authorizeRequests()
                    .regexMatchers("/actuator/.*").permitAll()
                    .regexMatchers("/repositories/.*/metrics/?.*").permitAll()
                    .regexMatchers("/metrics").permitAll()
                    .antMatchers("/api-docs/**","/swagger-ui/**").permitAll()
                    .anyRequest().authenticated()
                .and()
                .logout().logoutUrl("/openid_logout")
                .clearAuthentication(true)
                .invalidateHttpSession(true)
                .deleteCookies()
                .logoutSuccessUrl(logoutSuccessUrl)
                .and()
                .addFilterBefore(openIdConnectAuthenticationFilter(), AbstractPreAuthenticatedProcessingFilter.class)
        ;
    }

    @Bean
    public OIDCAuthenticationProvider openIdConnectAuthenticationProvider() {
        OIDCAuthenticationProvider oidcProvider = new OIDCAuthenticationProvider();
        oidcProvider.setAuthoritiesMapper(authoritiesMapper());
        return oidcProvider;
    }

    @Bean
    public OpenAIREAuthoritiesMapper authoritiesMapper() {
        OpenAIREAuthoritiesMapper authoritiesMapper = new OpenAIREAuthoritiesMapper();
        return authoritiesMapper;
    }

    @Bean
    public StaticServerConfigurationService staticServerConfigurationService() {
        StaticServerConfigurationService staticServerConfigurationService = new StaticServerConfigurationService();
        Map<String, ServerConfiguration> servers = new HashMap<>();
        servers.put(oidcIssuer, serverConfiguration());
        staticServerConfigurationService.setServers(servers);
        return staticServerConfigurationService;
    }

    @Bean
    public StaticClientConfigurationService staticClientConfigurationService() {
        StaticClientConfigurationService staticClientConfigurationService = new StaticClientConfigurationService();
        Map<String, RegisteredClient> clients = new HashMap<>();
        clients.put(oidcIssuer, registeredClient());
        staticClientConfigurationService.setClients(clients);
        return staticClientConfigurationService;
    }

    @Bean
    public RegisteredClient registeredClient() {
        RegisteredClient registeredClient = new RegisteredClient();
        registeredClient.setClientId(oidcId);
        registeredClient.setClientSecret(oidcSecret);
        registeredClient.setScope(new HashSet<>(Arrays.asList("openid", "eduperson_entitlement", "profile", "email")));
        registeredClient.setTokenEndpointAuthMethod(AuthMethod.SECRET_BASIC);
        registeredClient.setRedirectUris(new HashSet<>(Collections.singletonList(oidcDevHome)));
        return registeredClient;
    }

    @Bean
    public StaticAuthRequestOptionsService staticAuthRequestOptionsService() {
        return new StaticAuthRequestOptionsService();
    }

    @Bean
    public PlainAuthRequestUrlBuilder plainAuthRequestUrlBuilder() {
        return new PlainAuthRequestUrlBuilder();
    }

    @Bean
    public ServerConfiguration serverConfiguration() {
        ServerConfiguration serverConfiguration = new ServerConfiguration();
        serverConfiguration.setIssuer(oidcIssuer);
        serverConfiguration.setAuthorizationEndpointUri(oidcIssuer + "authorize");
        serverConfiguration.setTokenEndpointUri(oidcIssuer + "token");
        serverConfiguration.setUserInfoUri(oidcIssuer + "userinfo");
        serverConfiguration.setJwksUri(oidcIssuer + "jwk");
        serverConfiguration.setRevocationEndpointUri(oidcIssuer + "revoke");
        return serverConfiguration;
    }

    @Bean
    public OIDCAuthenticationFilter openIdConnectAuthenticationFilter() throws Exception {
        OIDCAuthenticationFilter oidc = new OIDCAuthenticationFilter();
        oidc.setAuthenticationManager(authenticationManagerBean());
        oidc.setIssuerService(staticSingleIssuerService());
        oidc.setServerConfigurationService(staticServerConfigurationService());
        oidc.setClientConfigurationService(staticClientConfigurationService());
        oidc.setAuthRequestOptionsService(staticAuthRequestOptionsService());
        oidc.setAuthRequestUrlBuilder(plainAuthRequestUrlBuilder());
        oidc.setAuthenticationSuccessHandler(frontEndRedirect());
        return oidc;
    }

    @Bean
    public StaticSingleIssuerService staticSingleIssuerService() {
        StaticSingleIssuerService staticSingleIssuerService = new StaticSingleIssuerService();
        staticSingleIssuerService.setIssuer(oidcIssuer);
        return staticSingleIssuerService;
    }

    @Bean(initMethod = "init")
    public FrontEndLinkURIAuthenticationSuccessHandler frontEndRedirect() {
        FrontEndLinkURIAuthenticationSuccessHandler frontEnd = new FrontEndLinkURIAuthenticationSuccessHandler();
        frontEnd.setFrontEndURI(webAppFrontEnd);
        return frontEnd;
    }

}
