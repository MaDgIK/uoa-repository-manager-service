package eu.dnetlib.repo.manager.config;

import eu.dnetlib.utils.MailLibrary;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class MailConfig {

    @Value("${services.provide.mail.host}")
    private String host;

    @Value("${services.provide.mail.port}")
    private int port;

    @Value("${services.provide.mail.authenticate}")
    private boolean authenticate;

    @Value("${services.provide.mail.username}")
    private String username;

    @Value("${services.provide.mail.password}")
    private String password;

    @Value("${services.provide.mail.from}")
    private String from;

    @Value("${services.provide.mail.replyTo}")
    private String replyTo;

    @Value("${services.provide.mail.mode}")
    private String mode;

    @Value("${services.provide.mail.debug}")
    private boolean debug;

    @Bean
    public MailLibrary createMailLibrary() {
        MailLibrary lib = new MailLibrary();

        lib.setAuthenticate(authenticate);
        lib.setDebug(debug);
        lib.setFrom(from);
        lib.setMailhost(host);
        lib.setSmtpPort(port);
        lib.setMode(mode);
        lib.setReplyTo(replyTo);
        lib.setUsername(username);
        lib.setPassword(password);
        lib.setMode(mode);

        lib.init();

        return  lib;
    }
}
