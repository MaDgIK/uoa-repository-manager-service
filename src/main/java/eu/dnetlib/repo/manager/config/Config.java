package eu.dnetlib.repo.manager.config;

import eu.dnetlib.api.enabling.ISLookUpService;
import eu.dnetlib.api.functionality.ValidatorService;
import eu.dnetlib.clients.enabling.islookup.ws.ISLookUpClient;
import eu.dnetlib.clients.functionality.validator.ws.ValidatorWebService;
import eu.dnetlib.clients.functionality.validator.ws.ValidatorWebServiceClient;
import eu.dnetlib.domain.enabling.Vocabulary;
import gr.uoa.di.driver.enabling.ISLookUp;
import gr.uoa.di.driver.enabling.islookup.ISLookUpImpl;
import gr.uoa.di.driver.enabling.vocabulary.ISVocabularyLoader;
import gr.uoa.di.driver.enabling.vocabulary.VocabularyLoader;
import gr.uoa.di.driver.util.ServiceLocator;
import gr.uoa.di.driver.util.StaticServiceLocator;
import gr.uoa.di.driver.xml.VocabularyXmlConverter;
import org.apache.cxf.jaxws.JaxWsProxyFactoryBean;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan(basePackages = {"org.eurocris.openaire.cris.validator.service"})
public class Config {

    @Value("${services.provide.iSLookUpService.url}")
    private String lookupURL;

    @Value("${services.provide.validatorService.url}")
    private String validatorUrl;

    @Bean(name = "vocabularyLoader")
    public VocabularyLoader createVocabularyLoader() throws Exception {
        ISVocabularyLoader loader = new ISVocabularyLoader();

        loader.setLookUp(createVocabularyLookUp());

        return loader;
    }

    @Bean("validatorServiceLocator")
    public ServiceLocator<ValidatorService> createValidatorServiceLocator() {
        JaxWsProxyFactoryBean factory = new JaxWsProxyFactoryBean();

        factory.setServiceClass(ValidatorWebService.class);
        factory.setAddress(validatorUrl);

        ValidatorWebService service = (ValidatorWebService) factory.create();

        ValidatorWebServiceClient client = new ValidatorWebServiceClient();
        client.setWebService(service);

        StaticServiceLocator<ValidatorService> locator = new StaticServiceLocator<>();
        locator.setService(client);

        return locator;
    }

    //    @Bean(name="vocabularyLookUp")
    private ISLookUp<Vocabulary> createVocabularyLookUp() throws Exception {
        ISLookUpImpl<Vocabulary> lookUp = new ISLookUpImpl<>();

        lookUp.setLookupLocator(createIsLookupServiceLocator());
        lookUp.setConverter(new VocabularyXmlConverter());

        return lookUp;
    }

    private ServiceLocator<ISLookUpService> createIsLookupServiceLocator() throws Exception {
        JaxWsProxyFactoryBean factory = new JaxWsProxyFactoryBean();

        factory.setServiceClass(eu.dnetlib.enabling.is.lookup.rmi.ISLookUpService.class);
        factory.setAddress(lookupURL);
        eu.dnetlib.enabling.is.lookup.rmi.ISLookUpService client = (eu.dnetlib.enabling.is.lookup.rmi.ISLookUpService) factory.create();

        ISLookUpClient service = new ISLookUpClient();
        service.setWebService(client);

        StaticServiceLocator<ISLookUpService> locator = new StaticServiceLocator<>();
        locator.setService(service);

        return locator;
    }
}
