package eu.dnetlib.repo.manager.config;

import com.google.gson.JsonArray;
import com.nimbusds.jwt.JWT;
import eu.dnetlib.repo.manager.service.security.AuthoritiesMapper;
import org.mitre.openid.connect.client.OIDCAuthoritiesMapper;
import org.mitre.openid.connect.model.UserInfo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.stereotype.Component;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

@Component
public class OpenAIREAuthoritiesMapper implements OIDCAuthoritiesMapper {

    private static final Logger logger = LoggerFactory.getLogger(OpenAIREAuthoritiesMapper.class);

    @Value("${services.provide.adminEmail}")
    String adminEmail;

    @Override
    public Collection<? extends GrantedAuthority> mapAuthorities(JWT jwtToken, UserInfo userInfo) {
        JsonArray entitlements = null;
        Set<GrantedAuthority> authorities = new HashSet<>();
        if (userInfo != null && userInfo.getSource() != null) {
            if (userInfo.getSource().getAsJsonArray("edu_person_entitlements") != null) {
                entitlements = userInfo.getSource().getAsJsonArray("edu_person_entitlements");
            } else if (userInfo.getSource().getAsJsonArray("eduperson_entitlement") != null) {
                entitlements = userInfo.getSource().getAsJsonArray("eduperson_entitlement");
            }
            logger.debug("user info: {}\nentitlements: {}", userInfo, entitlements);

            // FIXME: delete this if statement when super administrators are set
            if (userInfo.getEmail() != null && userInfo.getEmail().equals(adminEmail)) {
                authorities.add(new SimpleGrantedAuthority("SUPER_ADMINISTRATOR"));
            }

            authorities.addAll(AuthoritiesMapper.map(entitlements));
        }
        return authorities;
    }
}

