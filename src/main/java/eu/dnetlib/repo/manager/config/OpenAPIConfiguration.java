package eu.dnetlib.repo.manager.config;


import io.swagger.v3.oas.annotations.OpenAPIDefinition;
import io.swagger.v3.oas.annotations.info.Contact;
import io.swagger.v3.oas.annotations.info.Info;
import io.swagger.v3.oas.annotations.info.License;

@OpenAPIDefinition(
        info = @Info(
                title = "Repository Manager Dashboard API Documentation",
                description = "Repository Manager Dashboard API Documentation",
                version = "1.0",
                termsOfService = "urn:tos",
                license = @License(
                        name = "Apache 2.0",
                        url = "https://www.apache.org/licenses/LICENSE-2.0.html"
                ),
                contact = @Contact(name = "", url = "", email = "")
        )
)
public class OpenAPIConfiguration {
}
