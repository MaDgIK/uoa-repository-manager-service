package eu.dnetlib.repo.manager.utils;

import eu.dnetlib.repo.manager.domain.Timezone;
import org.apache.commons.codec.digest.DigestUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class Converter {

    private static final Logger logger = LoggerFactory.getLogger(Converter.class);

    private Converter() {
    }

    public static List<String> readFile(String filename) {
        String line;
        List<String> list = new ArrayList<>();
        try ( BufferedReader br = new BufferedReader(new InputStreamReader(Objects.requireNonNull(Converter.class.getResourceAsStream("/eu/**/" + filename)))) )
        {
            while ((line = br.readLine()) != null) {
                list.add(line.trim());
            }
        } catch (Exception e) {
            logger.error("Error opening file!", e);
        }
        return list;    // It may be empty.
    }

    public static List<Timezone> toTimezones(List<String> timezones) {

        List<Timezone> tmz = new ArrayList<>();
        for (String t : timezones) {
            String[] s = t.split("\t");
            tmz.add(new Timezone(s[1], Double.parseDouble(s[0])));
        }
        return tmz;
    }

    public static String getOpenaireId(String repositoryId) {
        if (repositoryId != null && repositoryId.contains("::"))
            return repositoryId.split("::")[0] + "::" + DigestUtils.md5Hex(repositoryId.split("::")[1]);
        return null;
    }

    public static Boolean convertStringToBoolean(String value) {
        return value.equals("null") ? null : Boolean.valueOf(value);
    }

    public static Double toDouble(String number) {
        if (Objects.equals(number, "null"))
            return 0.0;
        else
            return Double.valueOf(number);
    }

}

