package eu.dnetlib.repo.manager.utils;



import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Objects;

public class DateUtils {

    private static final Logger logger = LoggerFactory.getLogger(DateUtils.class);

    public static Date toDate(String date) {

        if (Objects.equals(date, "null"))
            return null;

        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
        try {
            return formatter.parse(date);
        } catch (ParseException e) {
            logger.error(e.getMessage(), e);
        }
        return null;
    }

    public static String toString(Date date) {

        if (Objects.equals(date, null))
            return null;

        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
        return formatter.format(date);
    }

    public static String getYear(String date) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(Objects.requireNonNull(toDate(date)));
        return String.valueOf(calendar.get(Calendar.YEAR));
    }

    private DateUtils() {
    }
}
