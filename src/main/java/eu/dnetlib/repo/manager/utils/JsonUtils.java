package eu.dnetlib.repo.manager.utils;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

public class JsonUtils {

    private JsonUtils() {
    }

    public JsonObject createResponse(JsonElement response) {
        JsonObject json = new JsonObject();
        json.add("response", response);
        return json;
    }

    public static JsonObject createResponse(String response) {
        JsonObject json = new JsonObject();
        json.addProperty("response", response);
        return json;
    }

    public JsonObject createResponse(Number response) {
        JsonObject json = new JsonObject();
        json.addProperty("response", response);
        return json;
    }

    public static JsonObject createResponse(Boolean response) {
        JsonObject json = new JsonObject();
        json.addProperty("response", response);
        return json;
    }

    public static JsonObject createResponse(Character response) {
        JsonObject json = new JsonObject();
        json.addProperty("response", response);
        return json;
    }
}
