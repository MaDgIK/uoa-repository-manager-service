package eu.dnetlib.repo.manager.utils;

import eu.dnetlib.domain.functionality.validator.JobResultEntry;
import eu.dnetlib.domain.functionality.validator.StoredJob;
import org.eurocris.openaire.cris.validator.model.Job;
import org.eurocris.openaire.cris.validator.model.Rule;
import org.eurocris.openaire.cris.validator.model.RuleResults;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class CrisValidatorUtils {

    private static final DateFormat DATE_FORMATTER = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    private static final DateFormat DURATION_FORMATTER = new SimpleDateFormat("HH 'hours' mm 'min' ss 'sec'");

    public static StoredJob convertJobToStoredJob(Job job) {
        StoredJob sj = new StoredJob();
        sj.setId(job.getId().hashCode());
        sj.setValidationStatus(job.getStatus());

        Date dateFinished = job.getDateFinished();
        Date dateStarted = job.getDateStarted();
        if ( dateFinished != null ) {
            sj.setEnded(DATE_FORMATTER.format(dateFinished));
            sj.setDuration(DURATION_FORMATTER.format(new Date(dateFinished.getTime() - dateStarted.getTime())));
        } else {
            sj.setEnded("-");
            sj.setDuration("-");
        }
        sj.setStarted(DATE_FORMATTER.format(dateStarted));

        sj.setUserEmail(job.getUser());
        sj.setCris(true);
        sj.setBaseUrl(job.getUrl());
        sj.setValidationType("CU");
        sj.setJobType("CRIS Validation");
        sj.setGuidelinesShortName("For CRIS Managers 1.1");
        sj.setValidationSet("-");

        // Set status
        sj.setValidationStatus(job.getStatus());
        sj.setUsageJobStatus(job.getUsageJobStatus());
        sj.setContentJobStatus(job.getContentJobStatus());

        // Set scores
//            sj.setFilteredScores(); // TODO what is this?
        sj.setContentJobScore(job.getContentScore());
        sj.setUsageJobScore(job.getUsageScore());

        sj.setResultEntries(CrisValidatorUtils.crisResultsToJobEntries(job));
        sj.setRecordsTested(job.getRecordsTested());

        return sj;
    }

    public static List<JobResultEntry> crisResultsToJobEntries(Job crisJob) {
        List<JobResultEntry> jobResultEntries = new ArrayList<>();
        for (RuleResults ruleResults : crisJob.getRuleResults()) {
            Rule rule = ruleResults.getRule();
            JobResultEntry jobResultEntry = new JobResultEntry();
            jobResultEntry.setName(rule.getName());
            jobResultEntry.setRuleId(rule.getId());
            jobResultEntry.setDescription(rule.getDescription());
            jobResultEntry.setMandatory(true);
            jobResultEntry.setWeight(Math.round(rule.getWeight()));
            jobResultEntry.setType(rule.getType());
            jobResultEntry.setHasErrors(false);
            long countRuleResults = ruleResults.getCount();
            if (countRuleResults == 0) {
                jobResultEntry.setSuccesses("-");
            } else {
                jobResultEntry.setSuccesses(countRuleResults - ruleResults.getFailed() + "/" + countRuleResults);
            }
            List<String> errorMessages = ruleResults.getErrorMessages();
            if (errorMessages != null && !errorMessages.isEmpty()) {
                jobResultEntry.setErrors(errorMessages);
                jobResultEntry.setHasErrors(true);
            }
            jobResultEntries.add(jobResultEntry);
        }
        return jobResultEntries;
    }

    private CrisValidatorUtils() {
    }
}
