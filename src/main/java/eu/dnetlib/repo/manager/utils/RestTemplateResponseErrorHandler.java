package eu.dnetlib.repo.manager.utils;

import eu.dnetlib.repo.manager.exception.EndPointException;
import org.springframework.http.HttpStatus;
import org.springframework.http.client.ClientHttpResponse;
import org.springframework.stereotype.Component;
import org.springframework.web.client.ResponseErrorHandler;

import java.io.IOException;

import static org.springframework.http.HttpStatus.Series.CLIENT_ERROR;
import static org.springframework.http.HttpStatus.Series.SERVER_ERROR;

@Component
public class RestTemplateResponseErrorHandler implements ResponseErrorHandler {

    @Override
    public boolean hasError(ClientHttpResponse httpResponse) throws IOException {
        HttpStatus.Series seriesError = httpResponse.getStatusCode().series();
        return ( (seriesError == CLIENT_ERROR) || (seriesError == SERVER_ERROR) );
    }

    @Override
    public void handleError(ClientHttpResponse httpResponse) throws IOException {

        HttpStatus statusCode = httpResponse.getStatusCode();
        if ( statusCode == HttpStatus.NOT_FOUND )
            throw new IOException();
        else if (statusCode.series() == SERVER_ERROR)
            throw new EndPointException();
    }
}
