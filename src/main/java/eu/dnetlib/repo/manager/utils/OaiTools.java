package eu.dnetlib.repo.manager.utils;

import org.dom4j.io.DOMWriter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Document;
import se.kb.oai.pmh.*;

import javax.net.ssl.*;
import javax.xml.namespace.NamespaceContext;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

public class OaiTools {

	{
		disableSslVerification();
	}

	private static final Logger logger = LoggerFactory.getLogger(OaiTools.class);

	public static List<String> getSetsOfRepo(String baseUrl) throws Exception {
		try {
			logger.debug("Getting sets of repository: {}", baseUrl);
			OaiPmhServer harvester = new OaiPmhServer(baseUrl);
			SetsList setList = harvester.listSets();
			ResumptionToken token = setList.getResumptionToken();
			List<Set> sets = new ArrayList<>(setList.asList());
			while (token != null) {
				setList = harvester.listSets(token);
				sets.addAll(setList.asList());
				token = setList.getResumptionToken();
			}

			List<String> ret = new ArrayList<String>();
			for (Set set : sets) {
				ret.add(set.getSpec().trim());
			}
			if (ret.size() > 0 )
				Collections.sort(ret);
			return ret;

		} catch (Exception e) {
			logger.error("Error getting sets of repository " + baseUrl, e);
			return new ArrayList<String>();
			//throw e;
		}
	}

	public static boolean identifyRepository(String baseUrl) throws Exception {
		logger.debug("sending identify request to repo: {}", baseUrl);

		OaiPmhServer harvester = new OaiPmhServer(baseUrl);

		if (baseUrl.trim().isEmpty()) {
			return false;
		}

		try {
			Identification identification = harvester.identify();
			DOMWriter d4Writer = new DOMWriter();
			Document d = d4Writer.write(identification.getResponse());

			return verifyIdentify(d);
		} catch (Exception e) {
			logger.debug("Error verifying identify response", e);
			throw e;
		}
	}

	private static boolean verifyIdentify(Document doc) throws XPathExpressionException {
		NamespaceContext ctx = new NamespaceContext() {
			public String getNamespaceURI(String prefix) {
				String uri;
				if (prefix.equals("oai"))
					uri = "http://www.openarchives.org/OAI/2.0/";
				else
					uri = null;
				return uri;
			}

			// Dummy implementation - not used!
			public Iterator<String> getPrefixes(String val) {
				return null;
			}

			// Dummy implemenation - not used!
			public String getPrefix(String uri) {
				return null;
			}
		};

		// Now the XPath expression

		String xpathStr = "//oai:OAI-PMH/oai:Identify";
		XPathFactory xpathFact = XPathFactory.newInstance();
		XPath xpath = xpathFact.newXPath();
		xpath.setNamespaceContext(ctx);
		String result = xpath.evaluate(xpathStr, doc);

		return (result != null && !result.equals(""));
	}

	private static void disableSslVerification() {
		try
		{
			logger.debug("disabling ssl verification");
			// Create a trust manager that does not validate certificate chains
			TrustManager[] trustAllCerts = new TrustManager[] {new X509TrustManager() {
				public X509Certificate[] getAcceptedIssuers() {
					return null;
				}
				public void checkClientTrusted(X509Certificate[] certs, String authType) {
				}
				public void checkServerTrusted(X509Certificate[] certs, String authType) {
				}
			}
			};

			// Install the all-trusting trust manager
			SSLContext sc = SSLContext.getInstance("SSL");
			sc.init(null, trustAllCerts, new java.security.SecureRandom());
			HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());

			// Create all-trusting host name verifier
			HostnameVerifier allHostsValid = new HostnameVerifier() {
				public boolean verify(String hostname, SSLSession session) {
					return true;
				}
			};

			// Install the all-trusting host verifier
			HttpsURLConnection.setDefaultHostnameVerifier(allHostsValid);
		} catch (NoSuchAlgorithmException e) {
			logger.error("disabling ssl verification", e);
		} catch (KeyManagementException e) {
			logger.error("error while disabling ssl verification", e);
		}
	}
}
