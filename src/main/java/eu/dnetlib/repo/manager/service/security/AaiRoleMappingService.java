package eu.dnetlib.repo.manager.service.security;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.stereotype.Service;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.util.Collection;
import java.util.Objects;
import java.util.stream.Collectors;

@Service("roleMappingService")
public class AaiRoleMappingService implements RoleMappingService {

    private static final Logger logger = LoggerFactory.getLogger(AaiRoleMappingService.class);

    @Value("${services.provide.aai.registry.production:true}")
    private boolean production;

    @Override
    public String getRepositoryId(String roleId) {
        if (!roleActive(roleId)) {
            return null;
        }
        return roleId.replaceFirst(".*datasource\\.", "").replace("$", ":");
    }

    @Override
    public Collection<String> getRepositoryIds(Collection<String> roleIds) {
        return roleIds
                .stream()
                .map(this::getRepositoryId)
                .filter(Objects::nonNull)
                .collect(Collectors.toList());
    }

    @Override
    public String getRole(String repoId) {
        String role = null;
        String prefix = (production ? "" : "beta.") + "datasource";
        if (repoId != null) {
            role = createRole(prefix, repoId);
        }
        return role;
    }

    @Override
    public Collection<String> getRoles(Collection<String> repoIds) {
        return repoIds
                .stream()
                .map(this::getRole)
                .filter(Objects::nonNull)
                .collect(Collectors.toList());
    }

    @Override
    public String authorityToRepositoryId(GrantedAuthority authority) {
        String repo = null;
        String auth = null;
        try {
            auth = URLDecoder.decode(authority.getAuthority(), "UTF-8").toLowerCase();
        } catch (UnsupportedEncodingException e) {
            logger.error("", e);
        }

        if (auth != null && roleActive(auth)) {
            repo = auth
                    .replaceFirst(".*datasource\\_", "")
                    .replace("$", ":")
                    .toLowerCase();
        }
        return repo;
    }

    @Override
    public GrantedAuthority repositoryIdToAuthority(String repoId) {
        String role = null;
        try {
            role = URLEncoder.encode(convertRepoIdToAuthorityId(repoId), "UTF-8");
        } catch (UnsupportedEncodingException e) {
            logger.error("", e);
        }
        return new SimpleGrantedAuthority(role);
    }

    private String createRole(String prefix, String repoId) {
        return prefix + "." + repoId.replace(":", "$");
    }

    private boolean roleActive(String roleId) {
        return (production && !roleId.toLowerCase().startsWith("beta"))
                || (!production && roleId.toLowerCase().startsWith("beta"));
    }

    private String convertRepoIdToAuthorityId(String repoId) {
        StringBuilder roleBuilder = new StringBuilder();
        String role = "";
        if (repoId != null) {
            roleBuilder.append(production ? "" : "beta.");
            roleBuilder.append("datasource.");
            roleBuilder.append(repoId.replace(":", "$"));
            role = roleBuilder.toString().replace(".", "_").toUpperCase();
        }
        return role;
    }
}
