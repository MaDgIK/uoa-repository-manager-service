package eu.dnetlib.repo.manager.service;

import eu.dnetlib.repo.manager.domain.RepositorySummaryInfo;
import org.json.JSONException;
import org.springframework.web.bind.annotation.PathVariable;

import java.util.List;

public interface DashboardService {

    List<RepositorySummaryInfo> getRepositoriesSummaryInfo(@PathVariable("userEmail") String userEmail,
                                                           @PathVariable("page") String page,
                                                           @PathVariable("size") String size) throws JSONException;
}
