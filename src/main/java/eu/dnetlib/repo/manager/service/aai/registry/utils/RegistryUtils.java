package eu.dnetlib.repo.manager.service.aai.registry.utils;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import eu.dnetlib.repo.manager.domain.dto.Role;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.util.Date;

@Component
public class RegistryUtils {

    @Value("1.0")
    private String version;

    @Value("2")
    private String coid;

    public JsonObject coPersonRoles(Integer coPersonId, Integer couId, String status) {
        JsonObject role = new JsonObject();
        JsonArray coPersonRoles = new JsonArray();
        JsonObject coPersonRole = new JsonObject();
        JsonObject person = new JsonObject();
        person.addProperty("Type", "CO");
        person.addProperty("Id", coPersonId.toString());
        coPersonRole.addProperty("Version", version);
        coPersonRole.add("Person", person);
        coPersonRole.addProperty("CouId", couId.toString());
        coPersonRole.addProperty("Affiliation", "member");
        coPersonRole.addProperty("Title", "");
        coPersonRole.addProperty("O", "Openaire");
        coPersonRole.addProperty("Status", status);
        if(status.equals("Active")) {
            coPersonRole.addProperty("ValidFrom", new Date().toString());
        } else {
            coPersonRole.addProperty("ValidThrough", new Date().toString());
        }
        coPersonRoles.add(coPersonRole);
        role.addProperty("RequestType", "CoPersonRoles");
        role.addProperty("Version", version);
        role.add("CoPersonRoles", coPersonRoles);
        return role;
    }

    public JsonObject createNewCou(Role role) {
        JsonObject cou = new JsonObject();
        JsonArray cous = new JsonArray();
        JsonObject newCou = new JsonObject();
        newCou.addProperty("Version", version);
        newCou.addProperty("CoId", coid);
        newCou.addProperty("Name", role.getName());
        newCou.addProperty("Description", role.getDescription());
        cous.add(newCou);
        cou.addProperty("RequestType", "Cous");
        cou.addProperty("Version", version);
        cou.add("Cous", cous);
        return cou;
    }

    public JsonObject coGroupMembers(Integer coGroupId, Integer coPersonId, boolean member) {
        JsonObject coGroup = new JsonObject();
        JsonArray coGroupMembers = new JsonArray();
        JsonObject coGroupMember = new JsonObject();
        JsonObject person = new JsonObject();
        person.addProperty("Type", "CO");
        person.addProperty("Id", coPersonId.toString());
        coGroupMember.addProperty("Version", version);
        coGroupMember.add("Person", person);
        coGroupMember.addProperty("CoGroupId", coGroupId.toString());
        coGroupMember.addProperty("Member", member);
        coGroupMember.addProperty("Owner", false);
        coGroupMember.addProperty("ValidFrom", "");
        coGroupMember.addProperty("ValidThrough", "");
        coGroupMembers.add(coGroupMember);
        coGroup.addProperty("RequestType", "CoGroupMembers");
        coGroup.addProperty("Version", version);
        coGroup.add("CoGroupMembers", coGroupMembers);
        return coGroup;
    }
}
