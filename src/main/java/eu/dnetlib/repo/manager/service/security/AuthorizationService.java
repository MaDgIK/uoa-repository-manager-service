package eu.dnetlib.repo.manager.service.security;

import eu.dnetlib.repo.manager.domain.dto.User;
import eu.dnetlib.repo.manager.exception.ResourceNotFoundException;

import java.util.Collection;
import java.util.List;

public interface AuthorizationService {

    /**
     * @param type
     * @param id
     * @return
     */
    String member(String type, String id); //TODO: use or delete

    /**
     * @param id Resource Id to check.
     * @return Checks if a user is a member of a resource.
     */
    boolean isMemberOf(String id);

    /**
     * @param id repository interface Id to check.
     * @return Checks if a user is a member of a repository interface.
     */
    boolean isMemberOfInterface(String id);

    /**
     * Returns a list of admins of the resource.
     *
     * @param repoId
     * @return
     */
    List<User> getAdminsOfRepo(String repoId);

    /**
     * Add a user as admin to a resource.
     *
     * @param resourceId Resource id
     * @param email      User email
     * @return
     * @throws ResourceNotFoundException
     */
    void addAdmin(String resourceId, String email) throws ResourceNotFoundException;

    /**
     * Remove user from resource admins.
     *
     * @param resourceId Resource id
     * @param email      User email
     * @return
     * @throws ResourceNotFoundException
     */
    void removeAdmin(String resourceId, String email) throws ResourceNotFoundException;

    /**
     * Creates a role based on the resourceId and assigns it to the current user.
     *
     * @param resourceId      usually the repository Id.
     * @param roleDescription usually the repository official name.
     */
    void createAndAssignRoleToAuthenticatedUser(String resourceId, String roleDescription);


    /**
     * Returns the roles of the authenticated user.
     *
     * @return
     */
    Collection<String> getUserRoles();

    /**
     * Returns the roles of the user with the given email.
     *
     * @param email
     * @return
     */
    Collection<String> getUserRolesByEmail(String email);

}
