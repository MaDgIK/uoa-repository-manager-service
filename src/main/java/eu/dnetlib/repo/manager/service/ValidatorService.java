package eu.dnetlib.repo.manager.service;

import eu.dnetlib.api.functionality.ValidatorServiceException;
import eu.dnetlib.domain.functionality.validator.JobForValidation;
import eu.dnetlib.domain.functionality.validator.RuleSet;
import eu.dnetlib.domain.functionality.validator.StoredJob;
import eu.dnetlib.repo.manager.domain.InterfaceInformation;
import eu.dnetlib.repo.manager.exception.ResourceNotFoundException;
import eu.dnetlib.repo.manager.exception.ValidationServiceException;
import org.json.JSONException;
import org.springframework.http.ResponseEntity;

import java.util.List;



public interface ValidatorService {


    JobForValidation submitJobForValidation(JobForValidation jobForValidation) throws ValidatorServiceException;

    ResponseEntity<Object> reSubmitJobForValidation(String email, String jobId) throws JSONException, ValidatorServiceException;

    List<RuleSet> getRuleSets(String mode);

    List<String> getSetsOfRepository(String url);

    boolean identifyRepo(String url);

    RuleSet getRuleSet(String acronym);

    List<StoredJob> getStoredJobsNew(String user,
                                     String jobType,
                                     String offset,
                                     String limit,
                                     String dateFrom,
                                     String dateTo,
                                     String validationStatus) throws ValidatorServiceException;

    int getStoredJobsTotalNumberNew(String user, String jobType, String validationStatus) throws ValidatorServiceException;

    InterfaceInformation getInterfaceInformation(String baseUrl) throws ValidationServiceException;

    List<StoredJob> getJobsSummary(String repoId, int limit) throws ValidatorServiceException, ResourceNotFoundException, JSONException;

    void onComplete(String repoId, String interfaceId, String jobId, String issuerEmail, boolean isUpdate, boolean isSuccess, int scoreUsage, int scoreContent) throws Exception;
}
