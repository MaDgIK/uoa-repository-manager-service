package eu.dnetlib.repo.manager.service;

import org.springframework.http.ResponseEntity;


public interface UserService {

    ResponseEntity<Object> login();
}
