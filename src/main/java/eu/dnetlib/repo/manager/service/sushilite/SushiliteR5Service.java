package eu.dnetlib.repo.manager.service.sushilite;

import org.json.JSONException;
import org.springframework.http.ResponseEntity;

import java.util.List;

public interface SushiliteR5Service {


    ResponseEntity<?> getReportResults(String Report,
                                       String release,
                                       String requestorID,
                                       String beginDate,
                                       String endDate,
                                       String repositoryIdentifier,
                                       String datasetIdentifier,
                                       String itemIdentifier,
                                       List<String> metricTypes,
                                       String dataType,
                                       String granularity,
                                       String pretty) throws JSONException;




}
