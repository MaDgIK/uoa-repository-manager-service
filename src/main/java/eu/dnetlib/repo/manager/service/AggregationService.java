package eu.dnetlib.repo.manager.service;

import eu.dnetlib.openaire.exporter.model.dsm.AggregationInfo;
import org.json.JSONException;

import java.util.List;
import java.util.Map;

public interface AggregationService {

    <T extends AggregationInfo> List<T> getRepositoryAggregations(String id) throws JSONException;

    <T extends AggregationInfo> List<T> getRepositoryAggregations(String id, int from, int size) throws JSONException;

    <T extends AggregationInfo> Map<String, List<T>> getRepositoryAggregationsByYear(String id) throws JSONException;

}
