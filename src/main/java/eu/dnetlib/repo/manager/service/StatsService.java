package eu.dnetlib.repo.manager.service;

import java.util.Map;


public interface StatsService {

    Map getStatistics() ;
}
