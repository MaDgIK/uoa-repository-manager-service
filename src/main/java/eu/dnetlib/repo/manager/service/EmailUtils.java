package eu.dnetlib.repo.manager.service;

import eu.dnetlib.domain.data.PiwikInfo;
import eu.dnetlib.domain.functionality.validator.JobForValidation;
import eu.dnetlib.repo.manager.domain.InterfaceComplianceRequest;
import eu.dnetlib.repo.manager.domain.Repository;
import eu.dnetlib.repo.manager.domain.RepositoryInterface;
import org.springframework.scheduling.annotation.Async;
import org.springframework.security.core.Authentication;

import java.util.List;

public interface EmailUtils {

    @Async
    void sendAdministratorRequestToEnableMetrics(PiwikInfo piwikInfo);

    @Async
    void sendUserRequestToEnableMetrics(PiwikInfo piwikInfo);

    @Async
    void sendAdministratorMetricsEnabled(PiwikInfo piwikInfo);

    @Async
    void sendUserMetricsEnabled(PiwikInfo piwikInfo);

    /****USER REGISTRATION REQUEST EMAILS****/
    @Async
    void sendAdminRegistrationEmail(Repository repository, Authentication authentication);

    @Async
    void sendUserRegistrationEmail(Repository repository, Authentication authentication);

    @Async
    void sendAdminRegisterInterfaceEmail(Repository repository, String comment, RepositoryInterface repositoryInterface, String desiredCompatibility, Authentication authentication);

    @Async
    void sendUserRegisterInterfaceEmail(Repository repository, String comment, RepositoryInterface repositoryInterface, String desiredCompatibility, Authentication authentication);

    /****SUCCESSFUL REGISTRATION RESULTS EMAILS****/
    @Async
    void sendUserRegistrationResultsSuccessEmail(String issuerEmail, String jobId, RepositoryInterface repositoryInterface, String desiredCompatibility, Repository repository, Authentication authentication);

    @Async
    void sendAdminRegistrationResultsSuccessEmail(String issuerEmail, String jobId, RepositoryInterface repositoryInterface, String desiredCompatibility, Repository repository, Authentication authentication);

    /****FAILURE REGISTRATION RESULTS EMAILS****/
    @Async
    void sendUserRegistrationResultsFailureEmail(String jobId, RepositoryInterface repositoryInterface, String desiredCompatibility, Repository repository, Authentication authentication);

    @Async
    void sendAdminRegistrationResultsFailureEmail(String issuerEmail, String jobId, RepositoryInterface repositoryInterface, String desiredCompatibility, Repository repository, Authentication authentication);

    /****SUCCESSFUL UPDATE RESULTS EMAILS****/
    @Async
    void sendUserUpdateResultsSuccessEmail(String issuer, String jobId, RepositoryInterface repositoryInterface, String desiredCompatibility, Repository repository, Authentication authentication);

    @Async
    void sendAdminUpdateResultsSuccessEmail(String issuerEmail, String jobId, RepositoryInterface repositoryInterface, String desiredCompatibility, Repository repository, Authentication authentication);

    /****FAILURE UPDATE RESULTS EMAILS****/
    @Async
    void sendUserUpdateResultsFailureEmail(String issuer, String jobId, RepositoryInterface repositoryInterface, String desiredCompatibility, Repository repository, Authentication authentication);

    @Async
    void sendAdminUpdateResultsFailureEmail(String issuerEmail, String jobId, RepositoryInterface repositoryInterface, String desiredCompatibility, Repository repository, Authentication authentication);

    /****FAILURE UPDATE INTERFACE COMPLIANCE****/
    @Async
    void sendUserUpdateInterfaceComplianceFailure(List<String> emails, Repository repository, RepositoryInterface repositoryInterface, InterfaceComplianceRequest request);

    @Async
    void sendAdminUpdateInterfaceComplianceFailure(Repository repository, RepositoryInterface repositoryInterface, InterfaceComplianceRequest request);

    /****VALIDATION OF CONTENT PROVIDER EMAILS****/
    @Async
    void sendUserValidationResults(String issuer, String jobId, RepositoryInterface repositoryInterface, Repository repository, Authentication authentication);

    @Async
    void sendAdminValidationResults(String issuer, String jobId, RepositoryInterface repositoryInterface, Repository repository, Authentication authentication);

    /****GENERAL FAILURE OF VALIDATOR****/
    @Async
    void sendAdminGeneralFailure(String issuer, String jobId, RepositoryInterface repositoryInterface, Repository repository, Authentication authentication);


    @Async
    void sendAdminUpdateRepositoryInfoEmail(Repository repository, Authentication authentication);

    @Async
    void sendUserUpdateRepositoryInfoEmail(Repository repository, Authentication authentication);

    @Async
    void sendAdminUpdateInterfaceEmail(Repository repository, String comment, RepositoryInterface repositoryInterface, String desiredCompatibility, Authentication authentication);

    @Async
    void sendUserUpdateInterfaceEmail(Repository repository, String comment, RepositoryInterface repositoryInterface, String desiredCompatibility, Authentication authentication);

    @Async
    void sendSubmitJobForValidationEmail(Authentication authentication, JobForValidation jobForValidation);

    @Async
    void sendUponJobCompletion(String repoId,
                               String repoInterfaceId,
                               String compatibility,
                               int scoreUsage,
                               int scoreContent,
                               boolean isSuccess,
                               boolean isUpdate,
                               String issuerEmail,
                               String jobId) throws Exception;
}
