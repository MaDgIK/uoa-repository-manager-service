package eu.dnetlib.repo.manager.service;

import eu.dnetlib.repo.manager.domain.*;
import eu.dnetlib.repo.manager.domain.dto.User;
import eu.dnetlib.repo.manager.exception.ResourceConflictException;
import eu.dnetlib.repo.manager.exception.ResourceNotFoundException;
import eu.dnetlib.repo.manager.repository.InterfaceComplianceRequestsRepository;
import eu.dnetlib.repo.manager.service.security.AuthorizationService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.stream.Collectors;

@Service
public class InterfaceComplianceService {

    private static final Logger logger = LoggerFactory.getLogger(InterfaceComplianceService.class);


    private final InterfaceComplianceRequestsRepository repository;
    private final EmailUtils emailUtils;
    private final AuthorizationService authorizationService;
    private final RepositoryService repositoryService;

    public InterfaceComplianceService(InterfaceComplianceRequestsRepository repository,
                                      EmailUtils emailUtils,
                                      AuthorizationService authorizationService,
                                      RepositoryService repositoryService) {
        this.repository = repository;
        this.emailUtils = emailUtils;
        this.authorizationService = authorizationService;
        this.repositoryService = repositoryService;
    }


    @Scheduled(cron = "0 0 0 * * *") // every day at 00:00
    public void cleanUp() {
        Set<InterfaceComplianceRequest> requests = getOutdated();

        // TODO - In case we want to send the emails, uncomment the following code.
        /*for ( InterfaceComplianceRequest request : requests )
        {
            String repoId = request.getRepositoryId();
            String interfaceId = request.getInterfaceId();
            try {
                Map<String, RepositoryInterface> repositoryInterfaceMap = repositoryService.getRepositoryInterface(repoId)
                        .stream()
                        .collect(Collectors.toMap(ApiDetails::getId, i -> i));
                RepositoryInterface iFace = repositoryInterfaceMap.get(interfaceId);
                if ( iFace == null ) {
                    logger.error("The repository-interface \"" + interfaceId + "\" does not exist! | the request had the \"repoId\": \"" + repoId + "\"");
                    continue;
                }
                Repository repo = repositoryService.getRepositoryById(repoId);
                List<User> repositoryAdmins = authorizationService.getAdminsOfRepo(repoId);
                emailUtils.sendUserUpdateInterfaceComplianceFailure(repositoryAdmins.stream().map(User::getEmail).collect(Collectors.toList()), repo, iFace, request);
                emailUtils.sendAdminUpdateInterfaceComplianceFailure(repo, iFace, request);
            } catch (ResourceNotFoundException e) {
                logger.error("Error for request with \"repoId\": \"" + repoId + "\" and \"interfaceId\": \"" + interfaceId + "\"", e);
            }   // Continue to the next request.
        }*/

        repository.deleteAll(requests);
    }

    private Set<InterfaceComplianceRequest> getOutdated() {
        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.DATE, -7);    // 7-days-old
        return this.repository.findAllBySubmissionDateBefore(calendar.getTime());
    }

    public Optional<InterfaceComplianceRequest> getById(InterfaceComplianceRequestId id) {
        return this.repository.findById(id);
    }

    public Iterable<InterfaceComplianceRequest> get() {
        return this.repository.findAll();
    }

    public InterfaceComplianceRequest create(InterfaceComplianceRequest request) {
        Optional<InterfaceComplianceRequest> existing = getById(request.getId());
        if (existing.isPresent()) {
            logger.warn("New Request: {}\nExisting request: {}", request, existing.get());
            throw new ResourceConflictException("A request for altering compliance already exists. Desired Compatibility value: " + existing.get().getDesiredCompatibilityLevel());
        }

        return this.repository.save(request);
    }

    public void delete(InterfaceComplianceRequestId id) {
        this.repository.deleteById(id);
    }
}
