package eu.dnetlib.repo.manager.service;

import eu.dnetlib.domain.data.PiwikInfo;
import eu.dnetlib.domain.functionality.validator.JobForValidation;
import eu.dnetlib.repo.manager.domain.InterfaceComplianceRequest;
import eu.dnetlib.repo.manager.domain.Repository;
import eu.dnetlib.repo.manager.domain.RepositoryInterface;
import eu.dnetlib.repo.manager.domain.dto.User;
import eu.dnetlib.repo.manager.exception.ValidationServiceException;
import eu.dnetlib.utils.MailLibrary;
import org.mitre.openid.connect.model.OIDCAuthenticationToken;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Async;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;

import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.stream.Collectors;

@Component("emailUtils")
@Async
public class EmailUtilsImpl implements EmailUtils {

    private final static Logger logger = LoggerFactory.getLogger(EmailUtilsImpl.class);

    private final MailLibrary mailLibrary;
    private final RepositoryService repositoryService;

    @Value("${services.provide.baseUrl}")
    private String baseUrl;

    @Value("${services.provide.usagestats.adminEmail}")
    private String usageStatsAdminEmail;

    @Value("${services.provide.helpdeskEmail}")
    private String helpdeskEmail;

    @Value("${services.provide.adminEmail}")
    private String provideAdminEmail;

    @Value("${services.provide.validator.results.url}")
    private String valBaseUrl;

    @Autowired
    EmailUtilsImpl(MailLibrary mailLibrary, RepositoryService repositoryService) {
        this.mailLibrary = mailLibrary;
        this.repositoryService = repositoryService;
    }

    @Override
    public void sendAdministratorRequestToEnableMetrics(PiwikInfo piwikInfo) {
        String subject = "[OpenAIRE-Usage Statistics] New request to enable usage statistics";

        String message =
                "we have received a request to enable the OpenAIRE usage statistics for the following repository \n" +
                        "\n" +
                        "Repository - " + piwikInfo.getRepositoryName() + ", " + piwikInfo.getCountry() + " (" + piwikInfo.getRepositoryId() + ")\n" +
                        "Requestor - " + piwikInfo.getRequestorName() + ", " + piwikInfo.getRequestorEmail() + "\n" +
                        "Matomo ID - " + piwikInfo.getSiteId() + "\n" +
                        "Authentication token - " + piwikInfo.getAuthenticationToken() + "\n" +
                        "\n" +
                        "For more information about this request, go here: \n" +
                        this.baseUrl + "/admin/metrics";
        message = createAdminMail(message);

        this.sendMail(List.of(this.usageStatsAdminEmail, helpdeskEmail), subject, message);
    }

    @Override
    public void sendUserRequestToEnableMetrics(PiwikInfo piwikInfo) {
        String subject = "[OpenAIRE-Usage Statistics] Your request to enable usage statistics";

        String message = "Dear " + piwikInfo.getRequestorName() + ",\n" +
                "\n" +
                "we have received your request to enable the OpenAIRE usage statistics for your repository\n" +
                "\n" +
                "Repository - " + piwikInfo.getRepositoryName() + ", " + piwikInfo.getCountry() + " (" + piwikInfo.getRepositoryId() + ")\n" +
                "Matomo ID - " + piwikInfo.getSiteId() + "\n" +
                "Authentication token - " + piwikInfo.getAuthenticationToken() + "\n" +
                "\n" +
                "In order to enable the usage statistics, you must install the OpenAIRE's tracking code in your repository software. " +
                "OpenAIRE's usage statistics service tracking code is maintained on Github as a patch for various versions of DSpace " +
                "(https://github.com/openaire/OpenAIRE-Piwik-DSpace) and as an Eprints plugin for version 3 " +
                "(https://github.com/openaire/EPrints-OAPiwik). In case the platform is different from DSpace or EPrints please contact " +
                "the OpenAIRE team in repositoryusagestats@openaire.eu in order to find a solution.\n" +
                "\n" +
                "For more information about your request and configuration details, go here: \n" +
                this.baseUrl + "/getImpact/instructions/" + piwikInfo.getRepositoryId() + "\n" +
                "\n" +
                "Once you have finished configuring your repository or if you have any questions, please notify the OpenAIRE team by sending \n" +
                "an email to repositoryusagestats@openaire.eu\n" +
                "\n" +
                "Best,\n" +
                "The OpenAIRE team";

        this.sendMail(piwikInfo.getRequestorEmail(), subject, message);
    }

    @Override
    public void sendAdministratorMetricsEnabled(PiwikInfo piwikInfo) {
        String subject = "[OpenAIRE-Usage Statistics] Usage statistics have been enabled";

        String message =
                "The installation and configuration of OpenAIRE's tracking code for the following repository " +
                        "has been completed and validated and the usage statistics have been enabled in OpenAIRE.\n" +
                        "\n" +
                        "Repository - " + piwikInfo.getRepositoryName() + ", " + piwikInfo.getCountry() + " (" + piwikInfo.getRepositoryId() + ")\n" +
                        "Requestor - " + piwikInfo.getRequestorName() + ", " + piwikInfo.getRequestorEmail() + "\n" +
                        "Piwik ID - " + piwikInfo.getSiteId() + "\n" +
                        "Authentication token - " + piwikInfo.getAuthenticationToken();
        message = createAdminMail(message);

        this.sendMail(this.usageStatsAdminEmail, subject, message);
    }

    @Override
    public void sendUserMetricsEnabled(PiwikInfo piwikInfo) {
        String subject = "[OpenAIRE-Usage Statistics] Usage statistics have been enabled";

        String message = "Dear " + piwikInfo.getRequestorName() + ",\n" +
                "\n" +
                "The installation and configuration of OpenAIRE's tracking code for your repository \"" + piwikInfo.getRepositoryName() +
                "\" has been completed and validated and the usage statistics have been enabled in OpenAIRE.\n" +
                "\n" +
                "You can preview the statistics in your repository's dashboard: \n" +
                this.baseUrl + "/getImpact/" + piwikInfo.getRepositoryId() + "\n" +
                "\n" +
                " For more information and questions, you can contact the openaire support team by sending an email to " +
                "repositoryusagestats@openaire.eu\n" +
                "\n" +
                "Best Regards,\n" +
                "The OpenAIRE team";

        this.sendMail(piwikInfo.getRequestorEmail(), subject, message);
    }

    @Override
    public void sendAdminRegistrationEmail(Repository repository, Authentication authentication) {
        String subject = "OpenAIRE content provider registration for " +
                repository.getEoscDatasourceType() + "[" + repository.getOfficialname() + "]";

        String message =
                "We received a request to register the " + repository.getEoscDatasourceType() + "[" + repository.getOfficialname() + "]" +
                        " to the OpenAIRE compliant list of content providers. " +
                        "\n\n" +
                        "User Contact: " + ((OIDCAuthenticationToken) authentication).getUserInfo().getName() +
                        " (" + ((OIDCAuthenticationToken) authentication).getUserInfo().getEmail() + ")" +
                        "\n\n" +
                        "Please do not reply to this message\n" +
                        "This message has been generated automatically.";
        message = createAdminMail(message);

        this.sendMail(this.provideAdminEmail, subject, message);
    }

    @Override
    public void sendUserRegistrationEmail(Repository repository, Authentication authentication) {
        String subject = "OpenAIRE content provider registration for " +
                repository.getEoscDatasourceType() + "[" + repository.getOfficialname() + "]";

        String message =
                "We received a request to register the " + repository.getEoscDatasourceType() + "[" + repository.getOfficialname() + "]" +
                        " to the OpenAIRE compliant list of content providers. " +
                        "\n\n" +
                        "Please do not reply to this message\n" +
                        "This message has been generated automatically.\n\n" +
                        "If you have any questions, write to 'helpdesk@openaire.eu'.";
        message = createUserMail(message, authentication);

        Collection<String> emailTo = new HashSet<>();
        if (repository.getRegisteredby() != null) {
            emailTo.add(repository.getRegisteredby());
        }
        emailTo.add(User.from(((OIDCAuthenticationToken) authentication).getUserInfo()).getEmail());
        this.sendMail(emailTo, subject, message);
    }

    @Override
    public void sendAdminRegisterInterfaceEmail(Repository repository, String comment, RepositoryInterface repositoryInterface, String desiredCompatibility, Authentication authentication) {
        String subject = "OpenAIRE new interface registration request started for " +
                repository.getEoscDatasourceType() + "[" + repository.getOfficialname() + "]";

        String message =
                "We received a request to add the following interface: \n\n" +
                        "Base URL: " + repositoryInterface.getBaseurl() + "\n" +
                        "Set: " + repositoryInterface.getAccessSet() + "\n" +
                        "Selected Guidelines: " + getSelectedGuidelines(desiredCompatibility, repositoryInterface) + "\n\n" +
                        "to " + repository.getEoscDatasourceType() + "[" + repository.getOfficialname() + "].\n";

        if (comment != null)
            message += "\nThe users comment was '" + comment + "'\n";

        message += "A validation process for this interface against the OpenAIRE guidelines compatibility " +
                "has been started. You will be informed in another message once the process is finished." +
                "\n\n" +
                "User Contact: " + ((OIDCAuthenticationToken) authentication).getUserInfo().getName() +
                " (" + ((OIDCAuthenticationToken) authentication).getUserInfo().getEmail() + ")" +
                "\n\n" +
                "Please do not reply to this message\n" +
                "This message has been generated automatically.";
        message = createAdminMail(message);

        this.sendMail(this.provideAdminEmail, subject, message);
    }

    @Override
    public void sendUserRegisterInterfaceEmail(Repository repository, String comment, RepositoryInterface repositoryInterface, String desiredCompatibility, Authentication authentication) {
        String subject = "OpenAIRE new interface registration request started for " +
                repository.getEoscDatasourceType() + "[" + repository.getOfficialname() + "]";
        String message =
                "We received a request to add the following interface: \n\n" +
                        "Base URL: " + repositoryInterface.getBaseurl() + "\n" +
                        "Set: " + repositoryInterface.getAccessSet() + "\n" +
                        "Selected Guidelines: " + getSelectedGuidelines(desiredCompatibility, repositoryInterface) + "\n\n" +
                        "to " + repository.getEoscDatasourceType() + "[" + repository.getOfficialname() + "].\n";

        if (comment != null) {
            message += "\n Your comment was '" + comment + "'\n";
        }

        message += "A validation process for this interface against the OpenAIRE guidelines compatibility " +
                "has been started. You will be informed in another message once the process is finished." +
                "\n\n" +
                "Please do not reply to this message\n" +
                "This message has been generated automatically.";
        message = createUserMail(message, authentication);

        Collection<String> emailTo = new HashSet<>();
        if (repository.getRegisteredby() != null) {
            emailTo.add(repository.getRegisteredby());
        }
        emailTo.add(User.from(((OIDCAuthenticationToken) authentication).getUserInfo()).getEmail());
        this.sendMail(emailTo, subject, message);
    }

    @Override
    public void sendUserRegistrationResultsSuccessEmail(String issuerEmail, String jobId, RepositoryInterface repositoryInterface, String desiredCompatibility, Repository repository, Authentication authentication) {
        String subject = "OpenAIRE new interface registration request - results (success) for " +
                repository.getEoscDatasourceType() + "[" + repository.getOfficialname() + "]";

        String message =
                "the compatibility test on " + "[" + repository.getOfficialname() + "]" +
                        " was successful and the datasource type \"" + repository.getEoscDatasourceType() + "\" will be prepared for aggregation in OpenAIRE." +
                        "\n\n" +
                        "Please note that it usually takes about 3-4 weeks until a data source is indexed and it's metadata visible on openaire.eu.\n\n" +
                        "Registration identifier in OpenAIRE: " + repository.getNamespaceprefix() +
                        "\nOfficial Name:" + repository.getOfficialname() +
                        "\n\nBase URL: " + repositoryInterface.getBaseurl() +
                        "\n\nSet: " + repositoryInterface.getAccessSet() +
                        "\n\nSelected Guidelines: " + getSelectedGuidelines(desiredCompatibility, repositoryInterface) +
                        "\n\nYou can review the validation results here.\n" + valBaseUrl + "" + jobId +
                        "\n\n\nPlease do not reply to this email\n" +
                        "This message has been generated manually\n\n" +
                        "If you have any questions, write to 'helpdesk@openaire.eu'.";
        message = createUserMail(message, authentication);

        Collection<String> emailTo = new HashSet<>();
        if (repository.getRegisteredby() != null) {
            emailTo.add(repository.getRegisteredby());
        }
        emailTo.add(User.from(((OIDCAuthenticationToken) authentication).getUserInfo()).getEmail());
        this.sendMail(emailTo, subject, message);
    }

    @Override
    public void sendAdminRegistrationResultsSuccessEmail(String issuerEmail, String jobId, RepositoryInterface repositoryInterface, String desiredCompatibility, Repository repository, Authentication authentication) {
        String subject = "OpenAIRE new interface registration request - results (success) for " +
                repository.getEoscDatasourceType() + "[" + repository.getOfficialname() + "]";

        String message =
                "the compatibility test on " + "[" + repository.getOfficialname() + "]" +
                        " was successful and the datasource type \"" + repository.getEoscDatasourceType() + "\" will be prepared for aggregation in OpenAIRE." +
                        "\n\n" +
                        "Please note that it usually takes about 3-4 weeks until a data source is indexed and it's metadata visible on openaire.eu.\n\n" +
                        "Registration identifier in OpenAIRE: " + repository.getNamespaceprefix() +
                        "\nOfficial Name:" + repository.getOfficialname() +
                        "\n\nBase URL: " + repositoryInterface.getBaseurl() +
                        "\n\nSet: " + repositoryInterface.getAccessSet() +
                        "\n\nDesired Selected Guidelines: " + getSelectedGuidelines(desiredCompatibility, repositoryInterface) +
                        "\n\nUser Contact: " + issuerEmail + "" +
                        "\n\nYou can review the validation results here.\n" + valBaseUrl + "" + jobId +
                        "\n\n\nPlease do not reply to this email\n" +
                        "This message has been generated manually\n\n" +
                        "If you have any questions, write to 'helpdesk@openaire.eu'.";
        message = createAdminMail(message);

        this.sendMail(this.provideAdminEmail, subject, message);
    }

    @Override
    public void sendUserRegistrationResultsFailureEmail(String jobId, RepositoryInterface repositoryInterface, String desiredCompatibility, Repository repository, Authentication authentication) {
        String subject = "OpenAIRE new interface registration request - results (failure) for " +
                repository.getEoscDatasourceType() + "[" + repository.getOfficialname() + "]";
        String message =
                "the compatibility test on " + "[" + repository.getOfficialname() + "]" +
                        " was not successful and the registration process was interrupted." +
                        "\n\n" +
                        "We will check what caused the problem and get back to you within a couple of days.\n\n" +
                        "Registration identifier in OpenAIRE: " + repository.getNamespaceprefix() +
                        "\nOfficial Name:" + repository.getOfficialname() +
                        "\n\nBase URL: " + repositoryInterface.getBaseurl() +
                        "\n\nSet: " + repositoryInterface.getAccessSet() +
                        "\n\nSelected Guidelines: " + getSelectedGuidelines(desiredCompatibility, repositoryInterface) +
                        "\n\nYou can review the validation results here.\n" + valBaseUrl + "" + jobId +
                        "\n\n\nPlease do not reply to this email\n" +
                        "This message has been generated manually\n\n" +
                        "If you have any questions, write to 'helpdesk@openaire.eu'.";
        message = createUserMail(message, authentication);

        Collection<String> emailTo = new HashSet<>();
        if (repository.getRegisteredby() != null) {
            emailTo.add(repository.getRegisteredby());
        }
        emailTo.add(User.from(((OIDCAuthenticationToken) authentication).getUserInfo()).getEmail());
        this.sendMail(emailTo, subject, message);
    }

    @Override
    public void sendAdminRegistrationResultsFailureEmail(String issuerEmail, String jobId, RepositoryInterface repositoryInterface, String desiredCompatibility, Repository repository, Authentication authentication) {
        String subject = "OpenAIRE new interface registration request - results (failure) for " +
                repository.getEoscDatasourceType() + "[" + repository.getOfficialname() + "]";

        String message =
                "the compatibility test on [" + repository.getOfficialname() + "]" +
                        " was not successful and the registration process was interrupted." +
                        "\n\n" +
                        "We will check what caused the problem and get back to you within a couple of days.\n\n" +
                        "Registration identifier in OpenAIRE: " + repository.getNamespaceprefix() +
                        "\nOfficial Name:" + repository.getOfficialname() +
                        "\n\nBase URL: " + repositoryInterface.getBaseurl() +
                        "\n\nSet: " + repositoryInterface.getAccessSet() +
                        "\n\nSelected Guidelines: " + getSelectedGuidelines(desiredCompatibility, repositoryInterface) +
                        "\n\nUser Contact: " + issuerEmail + "" +
                        "\n\nYou can review the validation results here.\n" + valBaseUrl + "" + jobId +
                        "\n\n\nPlease do not reply to this email\n" +
                        "This message has been generated manually\n\n" +
                        "If you have any questions, write to 'helpdesk@openaire.eu'.";
        message = createAdminMail(message);

        this.sendMail(this.provideAdminEmail, subject, message);
    }

    @Override
    public void sendUserUpdateResultsSuccessEmail(String issuer, String jobId, RepositoryInterface repositoryInterface, String desiredCompatibility, Repository repository, Authentication authentication) {
        String subject = "OpenAIRE interface update request - results (success) for " +
                repository.getEoscDatasourceType() + "[" + repository.getOfficialname() + "]";

        String message =
                "the compatibility test on [" + repository.getOfficialname() + "] has been successful\n\n" +
                        "We will check your transmitted information and adjust the aggregation settings accordingly. Please note that it usually takes about 3-4 weeks until the changes are visible on openaire.eu." + "\n\n" +
                        "Registration identifier in OpenAIRE: " + repository.getNamespaceprefix() +
                        "\nOfficial Name:" + repository.getOfficialname() +
                        "\n\nBase URL: " + repositoryInterface.getBaseurl() +
                        "\n\nSet: " + repositoryInterface.getAccessSet() +
                        "\n\nSelected Guidelines: " + getSelectedGuidelines(desiredCompatibility, repositoryInterface) +
                        "\n\nYou can review the validation results here.\n" + valBaseUrl + "" + jobId +
                        "\n\n\nPlease do not reply to this email\n" +
                        "This message has been generated manually\n\n" +
                        "If you have any questions, write to 'helpdesk@openaire.eu'.";
        message = createUserMail(message, authentication);

        this.sendMail(issuer, subject, message);
    }

    @Override
    public void sendAdminUpdateResultsSuccessEmail(String issuerEmail, String jobId, RepositoryInterface repositoryInterface, String desiredCompatibility, Repository repository, Authentication authentication) {
        String subject = "OpenAIRE interface update request - results (success) for " +
                repository.getEoscDatasourceType() + "[" + repository.getOfficialname() + "]";

        String message =
                "the compatibility test on [" + repository.getOfficialname() + "] has been successful\n\n" +
                        "We will check your transmitted information and adjust the aggregation settings accordingly. Please note that it usually takes about 3-4 weeks until the changes are visible on openaire.eu." + "\n\n" +
                        "Registration identifier in OpenAIRE: " + repository.getNamespaceprefix() +
                        "\nOfficial Name:" + repository.getOfficialname() +
                        "\n\nBase URL: " + repositoryInterface.getBaseurl() +
                        "\n\nSet: " + repositoryInterface.getAccessSet() +
                        "\n\nSelected Guidelines: " + getSelectedGuidelines(desiredCompatibility, repositoryInterface) +
                        "\n\nUser Contact: " + issuerEmail + "" +
                        "\n\nYou can review the validation results here.\n" + valBaseUrl + "" + jobId +
                        "\n\n\nPlease do not reply to this email\n" +
                        "This message has been generated manually\n\n" +
                        "If you have any questions, write to 'helpdesk@openaire.eu'.";
        message = createAdminMail(message);

        this.sendMail(this.provideAdminEmail, subject, message);
    }

    @Override
    public void sendUserUpdateResultsFailureEmail(String issuer, String jobId, RepositoryInterface repositoryInterface, String desiredCompatibility, Repository repository, Authentication authentication) {
        String subject = "OpenAIRE interface update request - results (failure) for " +
                repository.getEoscDatasourceType() + "[" + repository.getOfficialname() + "]";

        String message =
                "the compatibility test on [" + repository.getOfficialname() + "] was not successful." +
                        "\n\n" +
                        "We will check your transmitted information to see what caused the problem and get back to you within a couple of days.\n\n" +
                        "Registration identifier in OpenAIRE: " + repository.getNamespaceprefix() +
                        "\nOfficial Name:" + repository.getOfficialname() +
                        "\n\nBase URL: " + repositoryInterface.getBaseurl() +
                        "\n\nSet: " + repositoryInterface.getAccessSet() +
                        "\n\nSelected Guidelines: " + getSelectedGuidelines(desiredCompatibility, repositoryInterface) +
                        "\n\nYou can review the validation results here.\n" + valBaseUrl + "" + jobId +
                        "\n\n\nPlease do not reply to this email\n" +
                        "This message has been generated manually\n\n" +
                        "If you have any questions, write to 'helpdesk@openaire.eu'.";
        message = createUserMail(message, authentication);

        this.sendMail(issuer, subject, message);
    }

    @Override
    public void sendAdminUpdateResultsFailureEmail(String issuerEmail, String jobId, RepositoryInterface repositoryInterface, String desiredCompatibility, Repository repository, Authentication authentication) {
        String subject = "OpenAIRE interface update request - results (failure) for " +
                repository.getEoscDatasourceType() + "[" + repository.getOfficialname() + "]";

        String message =
                "the compatibility test on " + "[" + repository.getOfficialname() + "]" +
                        " was not successful." +
                        "\n\n" +
                        "WWe will check your transmitted information to see what caused the problem and get back to you within a couple of days.\n\n" +
                        "Registration identifier in OpenAIRE: " + repository.getNamespaceprefix() +
                        "\nOfficial Name:" + repository.getOfficialname() +
                        "\n\nBase URL: " + repositoryInterface.getBaseurl() +
                        "\n\nSet: " + repositoryInterface.getAccessSet() +
                        "\n\nSelected Guidelines: " + getSelectedGuidelines(desiredCompatibility, repositoryInterface) +
                        "\n\nUser Contact: " + issuerEmail + "" +
                        "\n\nYou can review the validation results here.\n" + valBaseUrl + "" + jobId +
                        "\n\n\nPlease do not reply to this email\n" +
                        "This message has been generated manually\n\n" +
                        "If you have any questions, write to 'helpdesk@openaire.eu'.";
        message = createAdminMail(message);

        this.sendMail(this.provideAdminEmail, subject, message);
    }

    @Override
    public void sendUserUpdateInterfaceComplianceFailure(List<String> emails, Repository repository, RepositoryInterface repositoryInterface, InterfaceComplianceRequest request) {
        String subject = "OpenAIRE interface update compliance request - results (failure) for " +
                repository.getEoscDatasourceType() + "[" + repository.getOfficialname() + "]";

        String message =
                "The request for changing the compatibility of " + "[" + repository.getOfficialname() + "]" +
                        " was not successful." +
                        "\n\n" +

                        "Registration identifier in OpenAIRE: " + repository.getNamespaceprefix() +
                        "\nOfficial Name:" + repository.getOfficialname() +
                        "\n\nBase URL: " + repositoryInterface.getBaseurl() +
                        "\n\nSet: " + repositoryInterface.getAccessSet() +
                        "\n\nSelected Guidelines: " + request.getDesiredCompatibilityLevel() +
                        "\n\n\nPlease do not reply to this email\n" +
                        "This message has been generated automatically\n\n" +
                        "If you have any questions, write to 'helpdesk@openaire.eu'.";
        message = createRepoAdminsMail(message);

        this.sendMail(emails, subject, message);
    }

    @Override
    public void sendAdminUpdateInterfaceComplianceFailure(Repository repository, RepositoryInterface repositoryInterface, InterfaceComplianceRequest request) {
        String subject = "OpenAIRE interface update compliance request - results (failure) for " +
                repository.getEoscDatasourceType() + "[" + repository.getOfficialname() + "]";

        String message =
                "The request for changing the compatibility of " + "[" + repository.getOfficialname() + "]" +
                        " was not successful." +
                        "\n\n" +

                        "Registration identifier in OpenAIRE: " + repository.getNamespaceprefix() +
                        "\nOfficial Name:" + repository.getOfficialname() +
                        "\n\nBase URL: " + repositoryInterface.getBaseurl() +
                        "\n\nSet: " + repositoryInterface.getAccessSet() +
                        "\n\nSelected Guidelines: " + request.getDesiredCompatibilityLevel() +
                        "\n\n\nPlease do not reply to this email\n" +
                        "This message has been generated automatically\n\n" +
                        "If you have any questions, write to 'helpdesk@openaire.eu'.";
        message = createAdminMail(message);

        this.sendMail(this.provideAdminEmail, subject, message);
    }

    @Override
    public void sendUserValidationResults(String issuer, String jobId, RepositoryInterface repositoryInterface, Repository repository, Authentication authentication) {
        String subject = "OpenAIRE validator - Test results ";

        String message =
                "the validation request you have submitted has finished. You can retrieve the results by following this url: " + valBaseUrl + "" + jobId + " .\n\n" +
                        "Please do not reply to this message.\n" +
                        "This message has been generated automatically.\n" +
                        "If you have any questions, write to 'helpdesk@openaire.eu'.";
        message = createUserMail(message, authentication);

        Collection<String> emailTo = new HashSet<>();
        emailTo.add(issuer);
        if (repository.getRegisteredby() != null) {
            emailTo.add(repository.getRegisteredby());
        }
        emailTo.add(User.from(((OIDCAuthenticationToken) authentication).getUserInfo()).getEmail());
        this.sendMail(emailTo, subject, message);
    }

    @Override
    public void sendAdminValidationResults(String issuer, String jobId, RepositoryInterface repositoryInterface, Repository repository, Authentication authentication) {
        String subject = "OpenAIRE validator - Test results ";

        String message =
                "a validation request has finished. You can retrieve the results by following this url: " + valBaseUrl + "" + jobId + " .\n\n" +
                        "\n\nUser Contact: " + issuer + "" +
                        "Please do not reply to this message.\n" +
                        "This message has been generated automatically.\n" +
                        "If you have any questions, write to 'helpdesk@openaire.eu'.";
        message = createAdminMail(message);

        this.sendMail(this.provideAdminEmail, subject, message);
    }

    @Override
    public void sendAdminGeneralFailure(String issuer, String jobId, RepositoryInterface repositoryInterface, Repository repository, Authentication authentication) {
        String subject = "OpenAIRE validator - job failure";

        String message =
                "the validation job that was automatically submitted for the update/registration of the interface "
                        + repositoryInterface.getId() + " (" + repositoryInterface.getBaseurl() + ", "
                        + repositoryInterface.getAccessSet() + ") of the repository " + repository.getId()
                        + " (" + repository.getOfficialname() + ") failed to complete."
                        + "This message has been generated automatically.";
        message = createAdminMail(message);

        this.sendMail(this.provideAdminEmail, subject, message);
    }

    @Override
    public void sendAdminUpdateRepositoryInfoEmail(Repository repository, Authentication authentication) {
        String subject = "OpenAIRE content provider update information for " +
                repository.getEoscDatasourceType() + "[" + repository.getOfficialname() + "]";

        String message =
                "We received a request to update the basic information for " + repository.getEoscDatasourceType()
                        + "[" + repository.getOfficialname() + "].\n\n" +
                        "Please do not reply to this message\n" +
                        "This message has been generated automatically.";
        message = createAdminMail(message);

        this.sendMail(this.provideAdminEmail, subject, message);
    }

    @Override
    public void sendUserUpdateRepositoryInfoEmail(Repository repository, Authentication authentication) {
        String subject = "OpenAIRE content provider update information for " +
                repository.getEoscDatasourceType() + "[" + repository.getOfficialname() + "]";

        String message =
                "We received a request to update the basic information for " + repository.getEoscDatasourceType() + "[" + repository.getOfficialname() + "].\n\n" +
                        "Please do not reply to this message\n" +
                        "This message has been generated automatically.\n\n" +
                        "If you have any questions, write to 'helpdesk@openaire.eu'.";
        message = createUserMail(message, authentication);

        Collection<String> emailTo = new HashSet<>();
        if (repository.getRegisteredby() != null) {
            emailTo.add(repository.getRegisteredby());
        }
        emailTo.add(User.from(((OIDCAuthenticationToken) authentication).getUserInfo()).getEmail());
        this.sendMail(emailTo, subject, message);
    }

    @Override
    public void sendAdminUpdateInterfaceEmail(Repository repository, String comment, RepositoryInterface repositoryInterface, String desiredCompatibility, Authentication authentication) {
        String subject = "OpenAIRE interface update request started for " +
                repository.getEoscDatasourceType() + "[" + repository.getOfficialname() + "]";

        String message =
                "We received a request to update the following interface: \n\n" +
                        "Base URL: " + repositoryInterface.getBaseurl() + "\n" +
                        "Set: " + repositoryInterface.getAccessSet() + "\n" +
                        "Selected Guidelines: " + getSelectedGuidelines(desiredCompatibility, repositoryInterface) + "\n\n" +
                        "for " + repository.getEoscDatasourceType() + "[" + repository.getOfficialname() + "].\n";

        if (comment != null)
            message += "\nThe users comment was '" + comment + "'\n";

        message += "A new iteration process of the validation against the OpenAIRE guidelines compatibility has been started.\n\n" +
                "User Contact: " + ((OIDCAuthenticationToken) authentication).getUserInfo().getName() +
                " (" + ((OIDCAuthenticationToken) authentication).getUserInfo().getEmail() + ")" +
                "\n\n" +
                "Please do not reply to this message\n" +
                "This message has been generated automatically.";
        message = createAdminMail(message);

        this.sendMail(this.provideAdminEmail, subject, message);
    }

    @Override
    public void sendUserUpdateInterfaceEmail(Repository repository, String comment, RepositoryInterface repositoryInterface, String desiredCompatibility, Authentication authentication) {
        String subject = "OpenAIRE interface update request started for " +
                repository.getEoscDatasourceType() + "[" + repository.getOfficialname() + "]";

        String message =
                "We received a request to update the following interface: \n\n" +
                        "Base URL: " + repositoryInterface.getBaseurl() + "\n" +
                        "Set: " + repositoryInterface.getAccessSet() + "\n" +
                        "Selected Guidelines: " + getSelectedGuidelines(desiredCompatibility, repositoryInterface) + "\n\n" +
                        "for " + repository.getEoscDatasourceType() + "[" + repository.getOfficialname() + "].\n";

        if (comment != null) {
            message += "\n Your comment was '" + comment + "'\n";
        }

        message += "A new iteration process of the validation against the OpenAIRE guidelines compatibility has been started.\n\n" +
                "Please do not reply to this message\n" +
                "This message has been generated automatically.\n\n" +
                "If you have any questions, write to 'helpdesk@openaire.eu'.";
        message = createUserMail(message, authentication);

        Collection<String> emailTo = new HashSet<>();
        if (repository.getRegisteredby() != null) {
            emailTo.add(repository.getRegisteredby());
        }
        emailTo.add(User.from(((OIDCAuthenticationToken) authentication).getUserInfo()).getEmail());
        this.sendMail(emailTo, subject, message);
    }

    @Override
    public void sendSubmitJobForValidationEmail(Authentication authentication, JobForValidation jobForValidation) {
        String subject = "OpenAIRE validator - Test submission ";

        String message =
                "The validation request you have submitted has started.\n" +
                        "Please do not reply to this message.\n" +
                        "This message has been generated automatically.\n" +
                        "If you have any questions, write to 'helpdesk@openaire.eu'.";
        message = createUserMail(message, authentication);

        this.sendMail(jobForValidation.getUserEmail(), subject, message);
    }

    @Override
    public void sendUponJobCompletion(
            String repoId,
            String repoInterfaceId,
            String compatibility,
            int scoreUsage,
            int scoreContent,
            boolean isSuccess,
            boolean isUpdate,
            String issuerEmail,
            String jobId) throws Exception {
        List<RepositoryInterface> repositoryInterfaces = repositoryService.getRepositoryInterface(repoId);
        if (repositoryInterfaces.isEmpty())
            throw new ValidationServiceException("Repository interface with id \"" + repoInterfaceId + "\" not found", ValidationServiceException.ErrorCode.GENERAL_ERROR);

        RepositoryInterface repositoryInterface = repositoryInterfaces.stream().filter(repoInterface -> repoInterface.getId().equals(repoInterfaceId)).collect(Collectors.toList()).get(0);
        Repository repository = repositoryService.getRepositoryById(repoId);

        if (!isUpdate) {
            if (isSuccess) {
                if (scoreContent >= 50 && scoreUsage >= 50) {
                    this.sendUserRegistrationResultsSuccessEmail(issuerEmail, jobId, repositoryInterface, compatibility, repository, SecurityContextHolder.getContext().getAuthentication());
                    this.sendAdminRegistrationResultsSuccessEmail(issuerEmail, jobId, repositoryInterface, compatibility, repository, SecurityContextHolder.getContext().getAuthentication());
                } else {
                    this.sendUserRegistrationResultsFailureEmail(jobId, repositoryInterface, compatibility, repository, SecurityContextHolder.getContext().getAuthentication());
                    this.sendAdminRegistrationResultsFailureEmail(issuerEmail, jobId, repositoryInterface, compatibility, repository, SecurityContextHolder.getContext().getAuthentication());
                }
            } else {
                this.sendAdminGeneralFailure(issuerEmail, jobId, repositoryInterface, repository, SecurityContextHolder.getContext().getAuthentication());
            }
        } else {
            if (isSuccess) {
                if (scoreContent >= 50 && scoreUsage >= 50) {
                    this.sendUserUpdateResultsSuccessEmail(issuerEmail, jobId, repositoryInterface, compatibility, repository, SecurityContextHolder.getContext().getAuthentication());
                    this.sendAdminUpdateResultsSuccessEmail(issuerEmail, jobId, repositoryInterface, compatibility, repository, SecurityContextHolder.getContext().getAuthentication());
                } else {
                    this.sendUserUpdateResultsFailureEmail(issuerEmail, jobId, repositoryInterface, compatibility, repository, SecurityContextHolder.getContext().getAuthentication());
                    this.sendAdminUpdateResultsFailureEmail(issuerEmail, jobId, repositoryInterface, compatibility, repository, SecurityContextHolder.getContext().getAuthentication());
                }
            } else {
                this.sendAdminGeneralFailure(issuerEmail, jobId, repositoryInterface, repository, SecurityContextHolder.getContext().getAuthentication());
            }
        }

    }

    private void sendMail(String email, String subject, String message) {
        this.sendMail(Collections.singletonList(email), subject, message);
    }

    public void sendMail(Collection<String> recipients, String subject, String message) {
        try {
            logger.debug("Sending e-mail\nRecipients: {}\nSubject: {}\nMessage: {}", recipients, subject, message);
            mailLibrary.sendEmail(recipients.toArray(new String[]{}), subject, message);
        } catch (Exception e) {
            logger.error("Error sending e-mail\nRecipients: " + recipients + "\nSubject: " + subject + "\nMessage: " + message, e);
        }
    }

    private String createAdminMail(String body) {
        body = "Dear administrator,\n\n" + body;
        body += "\n\nBest Regards,\nthe OpenAIRE technical team\n";
        return body;
    }

    private String createUserMail(String body, Authentication authentication) {
        body = "Dear " + getUserName(authentication) + ",\n\n" + body;
        body += "\n\nBest Regards,\nthe OpenAIRE technical team\n";
        return body;
    }

    private String createRepoAdminsMail(String body) {
        body = "Dear repository admins,\n\n" + body;
        body += "\n\nBest Regards,\nthe OpenAIRE technical team\n";
        return body;
    }

    private String getUserName(Authentication authentication) {
        String user = "user";
        if (authentication != null) {
            try {
                user = ((OIDCAuthenticationToken) authentication).getUserInfo().getName();
            } catch (NullPointerException ex) {
            }
        }
        return user;    // It may be just "user". TODO - Wouldn't be better if it was null?
    }

    private String getSelectedGuidelines(String desiredCompatibility, RepositoryInterface repositoryInterface) {
        return desiredCompatibility != null ? desiredCompatibility : repositoryInterface.getCompatibilityOverride();
    }
}
