package eu.dnetlib.repo.manager.service.aai.registry;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import eu.dnetlib.repo.manager.domain.dto.Role;
import eu.dnetlib.repo.manager.domain.dto.User;

import java.util.List;
import java.util.Map;

public interface AaiRegistryService {

    /**
     * 1.1 Get CoPersonId List by authenticated user's Email
     *
     * @return
     */
    List<Integer> getCoPersonIdsByEmail();

    /**
     * 1.2 Get CoPersonId List by Email
     *
     * @param email
     * @return
     */
    List<Integer> getCoPersonIdsByEmail(String email);

    /**
     * 1.3 Get a list of User Identifiers by Email
     *
     * @param email
     * @return
     */
    List<String> getUserIdentifiersByEmail(String email);

    /**
     * 1.3 Get a list of User Identifiers by Email
     *
     * @param coPersonId
     * @return
     */
    List<String> getUserIdentifiersByCoPersonId(Integer coPersonId);

    /**
     * 2. Get CoPersonId by AAI identifier
     *
     * @return
     */
    Integer getCoPersonIdByIdentifier();

    /**
     * 3.1 Get OpenAIRE cous with a specific name(or substring)
     *
     * @param name
     * @return
     */
    JsonArray getCous(String name);

    /**
     * 3.2 Get all OpenAIRE cous
     *
     * @return
     */
    JsonArray getCous();

    /**
     * 4.1 Get a couId by name
     *
     * @param name
     * @return
     */
    Integer getCouId(String name);

    /**
     * 4.2 Get a couId by type.id with/without mapping type
     *
     * @param type
     * @param id
     * @return
     */
    Integer getCouId(String type, String id, boolean communityMap);

    /**
     * 4.3 Get a couId by type.id with mapping type
     *
     * @param type
     * @param id
     * @return
     */
    Integer getCouId(String type, String id);

    /**
     * 5. Get User non admin roles
     *
     * @param coPersonId
     * @return
     */
    JsonArray getRoles(Integer coPersonId);

    /**
     * 5.2 Get User non admin active roles
     *
     * @param coPersonId
     * @return
     */
    JsonArray getRolesWithStatus(Integer coPersonId, RoleStatus status);

    /**
     * 5.3 Get User non admin active roles
     *
     * @param coPersonIds
     * @return
     */
    JsonArray getRolesWithStatus(List<Integer> coPersonIds, RoleStatus status);

    /**
     * 6. Get Role id of User base on couId.
     *
     * @param coPersonId
     * @param couId
     * @return
     */
    Integer getRoleId(Integer coPersonId, Integer couId);

    /**
     * 7. Get User Groups
     *
     * @param coPersonId
     * @return
     */
    JsonArray getUserGroups(Integer coPersonId);

    /**
     * 8. Get User Admin Group of a Cou
     *
     * @param coPersonId
     * @param couId
     * @return
     */
    JsonObject getUserAdminGroup(Integer coPersonId, Integer couId);

    /**
     * 9. Get Groups of a Cou
     *
     * @param couId
     * @return
     */
    JsonArray getCouGroups(Integer couId);

    /**
     * 10. Get Admin Group of a Cou
     *
     * @param couId
     * @return
     */
    JsonObject getCouAdminGroup(Integer couId);

    /**
     * 11. Get users of a group
     *
     * @param coGroupId
     * @return
     */
    JsonArray getGroupMembers(Integer coGroupId);


    /**
     * 12. Get Users' email of a Cou
     *
     * @param couId
     * @param admin
     * @return
     */
    JsonArray getUserEmailByCouId(Integer couId, boolean admin);

    /**
     * 12.2 Get All Users that have a specific role // TODO: Keep or delete
     *
     * @param couId
     * @return
     */
    JsonArray getUsersByCouId(Integer couId);

    /**
     * 13. Get Users' names of a Cou
     *
     * @param couId
     * @param admin
     * @return
     */
    JsonArray getUserNamesByCouId(Integer couId, boolean admin);

    /**
     * 14. Get Users' identifiers of a Cou
     *
     * @param couId
     * @param admin
     * @return
     */
    JsonArray getUserIdByCouId(Integer couId, boolean admin);

    /**
     * 15. Assign a member role to a User
     *
     * @param coPersonId
     * @param couId
     */
    void assignMemberRole(Integer coPersonId, Integer couId);

    /**
     * 16. Remove a member role from a User
     *
     * @param coPersonId
     * @param couId
     * @param id
     */
    void removeMemberRole(Integer coPersonId, Integer couId, Integer id);

    /**
     * 17. Create a new role
     *
     * @param role
     * @return
     */
    Integer createRole(Role role);

    /**
     * 18. Get User's email
     *
     * @param coPersonId
     * @return
     */
    String getUserEmail(Integer coPersonId);

    /**
     * 19. Get User's names
     *
     * @param coPersonId
     * @return
     */
    String getUserNames(Integer coPersonId);

    /**
     * 20. Get User's identifier
     *
     * @param coPersonId
     * @return
     */
    String getUserId(Integer coPersonId);

    /**
     * 21. Assign an admin role to a User
     *
     * @param coPersonId
     * @param couId
     */
    void assignAdminRole(Integer coPersonId, Integer couId);

    /**
     * 22. Remove an admin role from a User
     *
     * @param coPersonId
     * @param couId
     */
    void removeAdminRole(Integer coPersonId, Integer couId);

    /**
     * 23. Get a cou Names from couIds.
     *
     * @param couIds
     * @return
     */
    Map<Integer, String> getCouNames(List<Integer> couIds);

    // TODO: add description
    List<User> getUsers(Integer couId);

    enum RoleStatus {
        ACTIVE("Active"),
        APPROVED("Approved"),
        CONFIRMED("Confirmed"),
        DECLINED("Declined"),
        DELETED("Deleted"),
        DENIED("Denied"),
        DUPLICATE("Duplicate"),
        EXPIRED("Expired"),
        GRACE_PERIOD("GracePeriod"),
        INVITED("Invited"),
        PENDING("Pending"),
        PENDING_APPROVAL("PendingApproval"),
        PENDING_CONFIRMATION("PendingConfirmation"),
        SUSPENDED("Suspended");

        public final String status;

        RoleStatus(String status) {
            this.status = status;
        }
    }
}
