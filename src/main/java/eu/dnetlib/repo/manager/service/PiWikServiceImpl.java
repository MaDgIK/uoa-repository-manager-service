package eu.dnetlib.repo.manager.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import eu.dnetlib.domain.data.PiwikInfo;
import eu.dnetlib.repo.manager.domain.OrderByField;
import eu.dnetlib.repo.manager.domain.OrderByType;
import eu.dnetlib.repo.manager.domain.Repository;
import eu.dnetlib.repo.manager.exception.RepositoryServiceException;
import org.apache.commons.codec.digest.DigestUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import javax.sql.DataSource;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URL;
import java.net.URLEncoder;
import java.sql.Types;
import java.util.List;
import java.util.Map;

@Service("piwikService")
public class PiWikServiceImpl implements PiWikService {

    private static final Logger logger = LoggerFactory.getLogger(PiWikServiceImpl.class);


    @Autowired
    private DataSource dataSource;


    @Value("${services.provide.analyticsURL}")
    private String analyticsURL;


    @Autowired
    private RepositoryService repositoryService;

    @Autowired
    @Qualifier("emailUtils")
    private EmailUtils emailUtils;


    private static final String GET_PIWIK_SITE = "select repositoryid, siteid, authenticationtoken, creationdate, requestorname, requestoremail, validated, validationdate, comment, repositoryname, country from piwik_site where repositoryid = ? LIMIT 1;";

    private static final String INSERT_PIWIK_INFO = "insert into piwik_site (repositoryid, siteid, creationdate, requestorname, requestoremail, validated, repositoryname, country, authenticationtoken) values (?, ?, now(), ?, ?, ?, ?, ?, ?)";

    private static final String UPDATE_PIWIK_INFO = "update piwik_site set siteid = ?, creationdate = now(), requestorname = ?, requestoremail = ?, validated = ?, repositoryname = ?, country = ?, authenticationtoken = ? where repositoryid = ?";

    private static final String GET_PIWIK_SITES = "select  repositoryid, siteid, authenticationtoken, creationdate, requestorname, requestoremail, validated, validationdate, comment, repositoryname, country from piwik_site ";

    private static final String GET_PIWIK_SITES_TOTAL = "select count(*) as totals from piwik_site ";

    private static final String APPROVE_PIWIK_SITE = "update piwik_site set validated=true, validationdate=now() where repositoryid = ?;";

    private final RowMapper<PiwikInfo> piwikRowMapper = (rs, i) -> new PiwikInfo(rs.getString("repositoryid"), getOpenaireId(rs.getString("repositoryid")), rs.getString("repositoryname"), rs.getString("country"),
            rs.getString("siteid"), rs.getString("authenticationtoken"), rs.getTimestamp("creationdate"), rs.getString("requestorname"), rs.getString("requestoremail"),
            rs.getBoolean("validated"), rs.getTimestamp("validationdate"), rs.getString("comment"));


    @Override
    public PiwikInfo getPiwikSiteForRepo(String repositoryId) {
        try {
            return new JdbcTemplate(dataSource).queryForObject(GET_PIWIK_SITE, new String[]{repositoryId}, new int[]{Types.VARCHAR}, piwikRowMapper);
        } catch (EmptyResultDataAccessException e) {
            return null;
        }
    }

    @Override
    @PreAuthorize("hasAuthority('SUPER_ADMINISTRATOR') or hasAuthority('CONTENT_PROVIDER_DASHBOARD_ADMINISTRATOR') or (hasAuthority('REGISTERED_USER') and #piwikInfo.requestorEmail == authentication.userInfo.email)")
    public PiwikInfo savePiwikInfo(PiwikInfo piwikInfo) {
        JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);
        if (getPiwikSiteForRepo(piwikInfo.getRepositoryId()) == null) {
            jdbcTemplate.update(INSERT_PIWIK_INFO, new Object[]{piwikInfo.getRepositoryId(), piwikInfo.getSiteId(), piwikInfo.getRequestorName(),
                            piwikInfo.getRequestorEmail(), piwikInfo.isValidated(), piwikInfo.getRepositoryName(), piwikInfo.getCountry(), piwikInfo.getAuthenticationToken()},
                    new int[]{Types.VARCHAR, Types.VARCHAR, Types.VARCHAR, Types.VARCHAR, Types.BOOLEAN, Types.VARCHAR, Types.VARCHAR, Types.VARCHAR});
        } else {
            jdbcTemplate.update(UPDATE_PIWIK_INFO, new Object[]{piwikInfo.getSiteId(), piwikInfo.getRequestorName(), piwikInfo.getRequestorEmail(),
                            piwikInfo.isValidated(), piwikInfo.getRepositoryName(), piwikInfo.getCountry(), piwikInfo.getAuthenticationToken(), piwikInfo.getRepositoryId()},
                    new int[]{Types.VARCHAR, Types.VARCHAR, Types.VARCHAR, Types.BOOLEAN, Types.VARCHAR, Types.VARCHAR, Types.VARCHAR, Types.VARCHAR});
        }
        return piwikInfo;
    }

    @Override
    public List<PiwikInfo> getPiwikSitesForRepos(OrderByField orderByField, OrderByType orderByType, int from, int quantity, String searchField) {
        try {
            String finalizedQuery = GET_PIWIK_SITES + " where (" +
                    " repositoryid ilike ? " +
                    " or siteid ilike ?" +
                    " or requestorname ilike ?" +
                    " or requestoremail ilike ?" +
                    " or comment ilike ?" +
                    " or repositoryname ilike ?" +
                    " or country ilike ?"
                    + ") order by " + orderByField + " " + orderByType + " offset ? limit ?";

            return new JdbcTemplate(dataSource).query(finalizedQuery, preparedStatement -> {
                preparedStatement.setString(1, "%" + searchField + "%");
                preparedStatement.setString(2, "%" + searchField + "%");
                preparedStatement.setString(3, "%" + searchField + "%");
                preparedStatement.setString(4, "%" + searchField + "%");
                preparedStatement.setString(5, "%" + searchField + "%");
                preparedStatement.setString(6, "%" + searchField + "%");
                preparedStatement.setString(7, "%" + searchField + "%");
                preparedStatement.setInt(8, from);
                preparedStatement.setInt(9, quantity);
            }, piwikRowMapper);
        } catch (EmptyResultDataAccessException e) {
            return null;
        }

    }

    @Override
    public int getPiwikSitesTotals(String searchField) {
        try {
            String finalizedQuery = GET_PIWIK_SITES_TOTAL + " where (" +
                    " repositoryid ilike ? " +
                    " or siteid ilike ?" +
                    " or requestorname ilike ?" +
                    " or requestoremail ilike ?" +
                    " or comment ilike ?" +
                    " or repositoryname ilike ?" +
                    " or country ilike ?)";

            return new JdbcTemplate(dataSource).query(finalizedQuery, preparedStatement -> {
                preparedStatement.setString(1, "%" + searchField + "%");
                preparedStatement.setString(2, "%" + searchField + "%");
                preparedStatement.setString(3, "%" + searchField + "%");
                preparedStatement.setString(4, "%" + searchField + "%");
                preparedStatement.setString(5, "%" + searchField + "%");
                preparedStatement.setString(6, "%" + searchField + "%");
                preparedStatement.setString(7, "%" + searchField + "%");
            }, rowMapper -> {
                rowMapper.next();
                return rowMapper.getInt("totals");
            });
        } catch (EmptyResultDataAccessException e) {
            return 0;
        }
    }

    @Override
    @PreAuthorize("hasAuthority('SUPER_ADMINISTRATOR') or hasAuthority('CONTENT_PROVIDER_DASHBOARD_ADMINISTRATOR')")
    public ResponseEntity<Object> approvePiwikSite(String repositoryId) {
        new JdbcTemplate(dataSource).update(APPROVE_PIWIK_SITE, new Object[]{repositoryId}, new int[]{Types.VARCHAR});
        return new ResponseEntity<>("OK", HttpStatus.OK);
    }

    @Override
    public String getOpenaireId(String repositoryId) {
        if (repositoryId != null && repositoryId.contains("::"))
            return repositoryId.split("::")[0] + "::" + DigestUtils.md5Hex(repositoryId.split("::")[1]);
        return null;
    }

    @Override
    @PreAuthorize("hasAuthority('SUPER_ADMINISTRATOR') or hasAuthority('CONTENT_PROVIDER_DASHBOARD_ADMINISTRATOR')")
    public ResponseEntity<Object> markPiwikSiteAsValidated(String repositoryId) throws RepositoryServiceException {
        try {
            approvePiwikSite(repositoryId);

            PiwikInfo piwikInfo = getPiwikSiteForRepo(repositoryId);
            emailUtils.sendAdministratorMetricsEnabled(piwikInfo);
            emailUtils.sendUserMetricsEnabled(piwikInfo);

        } catch (EmptyResultDataAccessException e) {
            logger.error("Error while approving piwik site: ", e);
            throw new RepositoryServiceException("General error", RepositoryServiceException.ErrorCode.GENERAL_ERROR);
        } catch (Exception e) {
            logger.error("Error while sending email to administrator or user about the enabling of metrics", e);
            throw new RepositoryServiceException(e, RepositoryServiceException.ErrorCode.GENERAL_ERROR);
        }
        return new ResponseEntity<>("OK", HttpStatus.OK);
    }

    @Override
    @PreAuthorize("hasAuthority('SUPER_ADMINISTRATOR') or hasAuthority('CONTENT_PROVIDER_DASHBOARD_ADMINISTRATOR') or (hasAuthority('REGISTERED_USER') and #piwikInfo.requestorEmail == authentication.userInfo.email)")
    public PiwikInfo enableMetricsForRepository(String officialName,
                                                String repoWebsite,
                                                PiwikInfo piwikInfo) throws RepositoryServiceException {
        try {
            String url = analyticsURL + "siteName=" + URLEncoder.encode(officialName, "UTF-8") + "&url="
                    + URLEncoder.encode(repoWebsite, "UTF-8");
            Map map = new ObjectMapper().readValue(new URL(url), Map.class);
            String siteId = null;
            if (map.get("value") != null) {
                siteId = map.get("value").toString();
            }
            piwikInfo.setSiteId(siteId);

            savePiwikInfo(piwikInfo);

            Repository repository = repositoryService.getRepositoryById(piwikInfo.getRepositoryId());
            repository.setPiwikInfo(piwikInfo);

            repositoryService.updateRepository(repository, SecurityContextHolder.getContext().getAuthentication());

            emailUtils.sendAdministratorRequestToEnableMetrics(piwikInfo);
            emailUtils.sendUserRequestToEnableMetrics(piwikInfo);
        } catch (UnsupportedEncodingException uee) {
            logger.error("Error while creating piwikScript URL", uee);
            throw new RepositoryServiceException("login.generalError", RepositoryServiceException.ErrorCode.GENERAL_ERROR);
        } catch (IOException ioe) {
            logger.error("Error while creating piwik site", ioe);
            throw new RepositoryServiceException("login.generalError", RepositoryServiceException.ErrorCode.GENERAL_ERROR);
        } catch (Exception e) {
            logger.error("Error while sending email to administrator or user about the request to enable metrics", e);
            throw new RepositoryServiceException(e, RepositoryServiceException.ErrorCode.GENERAL_ERROR);
        }
        return piwikInfo;
    }

    @Override
    public Integer getTotal() {
        return new JdbcTemplate(dataSource).queryForObject(GET_PIWIK_SITES_TOTAL, Integer.class, new Object[]{});
    }

    @Override
    public Integer getValidated(boolean validated) {
        String finalizedQuery = GET_PIWIK_SITES_TOTAL + " where validated = ?";
        return new JdbcTemplate(dataSource).queryForObject(finalizedQuery, Integer.class, validated);
    }
}
