package eu.dnetlib.repo.manager.service;

import eu.dnetlib.api.functionality.ValidatorServiceException;
import eu.dnetlib.repo.manager.domain.*;
import eu.dnetlib.repo.manager.exception.RepositoryServiceException;
import eu.dnetlib.repo.manager.exception.ResourceNotFoundException;
import org.springframework.security.core.Authentication;

import java.util.List;
import java.util.Map;

public interface RepositoryService {


    // TODO: move this elsewhere
    Country[] getCountries();

    List<Repository> getRepositories(List<String> ids);

    List<Repository> getRepositories(List<String> ids, int page, int size);

    List<RepositorySnippet> getRepositoriesSnippets(List<String> ids) throws Exception;

    List<RepositorySnippet> getRepositoriesSnippets(List<String> ids, int page, int size) throws Exception;

    List<RepositorySnippet> getRepositoriesByCountry(String country, String mode, Boolean managed);

    // TODO: remove?
    List<Repository> getRepositoriesOfUser(String page, String size);

    // TODO: remove?
    List<Repository> getRepositoriesOfUser(String userEmail, String page, String size);

    List<RepositorySnippet> getRepositoriesSnippetsOfUser(String page, String size);

    List<RepositorySnippet> getRepositoriesSnippetsOfUser(String userEmail, String page, String size);

    RepositorySnippet getRepositorySnippetById(String id) throws ResourceNotFoundException;

    Repository getRepositoryById(String id) throws ResourceNotFoundException;

    List<Repository> getRepositoriesByName(String name,
                                           String page,
                                           String size);

    List<RepositorySnippet> searchRegisteredRepositories(String country, String typology, String englishName,
                                                         String officialName, String requestSortBy, String order,
                                                         int page, int pageSize);

    Integer getTotalRegisteredRepositories();

    List<RepositoryInterface> getRepositoryInterface(String id);

    Repository addRepository(String datatype, Repository repository);

    void deleteRepositoryInterface(String id, String registeredBy);

    RepositoryInterface addRepositoryInterface(String datatype,
                                               String repoId,
                                               String comment,
                                               RepositoryInterface repositoryInterface,
                                               String desiredCompatibilityLevel) throws Exception;

    List<String> getDnetCountries();

    List<String> getTypologies();

    List<Timezone> getTimezones();

    Repository updateRepository(Repository repository, Authentication authentication);

    List<String> getUrlsOfUserRepos(String userEmail, String page, String size);

    Map<String, String> getCompatibilityClasses(String mode);

    Map<String, String> getDatasourceClasses(String mode);

    String getCountryName(String countryCode);

    MetricsInfo getMetricsInfoForRepository(String repoId) throws RepositoryServiceException;

    Map<String, String> getListLatestUpdate(String mode);

    RepositoryInterface updateRepositoryInterface(String repoId, String comment, RepositoryInterface repositoryInterface, String desiredCompatibilityLevel) throws ResourceNotFoundException, ValidatorServiceException;

    void updateInterfaceCompliance(String repositoryId, String repositoryInterfaceId, String compliance);
}
