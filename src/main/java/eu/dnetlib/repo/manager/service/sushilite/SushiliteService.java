package eu.dnetlib.repo.manager.service.sushilite;

import eu.dnetlib.usagestats.sushilite.domain.ReportResponseWrapper;
import org.json.JSONException;


public interface SushiliteService {


    ReportResponseWrapper getReportResults(String page,
                                           String pageSize,
                                           String Report,
                                           String Release,
                                           String RequestorID,
                                           String BeginDate,
                                           String EndDate,
                                           String RepositoryIdentifier,
                                           String ItemIdentifier,
                                           String ItemDataType,
                                           String Granularity,
                                           String Pretty) throws JSONException;

}
