package eu.dnetlib.repo.manager.service;

import eu.dnetlib.repo.manager.domain.Term;
import eu.dnetlib.repo.manager.domain.broker.*;
import eu.dnetlib.repo.manager.exception.BrokerException;
import org.json.JSONException;
import org.springframework.http.ResponseEntity;

import java.io.IOException;
import java.util.List;
import java.util.Map;


public interface BrokerService {

    DatasourcesBroker getDatasourcesOfUser(String user, String includeShared, String includeByOthers) throws BrokerException, JSONException;

    List<BrowseEntry> getTopicsForDatasource(String datasourceName) throws BrokerException;

    EventsPage advancedShowEvents(String page,
                                  String size,
                                  AdvQueryObject advQueryObject) throws BrokerException, JSONException ,IOException;

    EventsPage showEvents(String datasourceName,
                          String topic,
                          String page,
                          String size) throws BrokerException, JSONException;

    Map<String, List<SimpleSubscriptionDesc>> getSimpleSubscriptionsOfUser(String userEmail) throws BrokerException;

    Map<String, List<SimpleSubscriptionDesc>> getSimpleSubscriptionsOfUserByRepoId(String userEmail, String repoId) throws BrokerException;

    Subscription subscribe(OpenaireSubscription obj) throws BrokerException;

    ResponseEntity<Object> unsubscribe(String subscriptionId) throws BrokerException;

    Subscription getSubscription(String subscriptionId) throws BrokerException;

    Map<String, Term> getDnetTopics() throws BrokerException;

    EventsPage getNotificationsBySubscriptionId(String subscriptionId, String page, String size) throws BrokerException;

    Map<String, List<Subscription>> getSubscriptionsOfUser(String userEmail) throws BrokerException;
}
