package eu.dnetlib.repo.manager.service.sushilite;

//import eu.dnetlib.usagestats.sushilite.domain.COUNTER_Item_Report;
import org.json.JSONException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import java.net.URI;
import java.util.List;

@Service("sushiliteR5Service")
public class SushiliteR5ServiceImpl implements SushiliteR5Service {

    private static final Logger logger = LoggerFactory.getLogger(SushiliteR5ServiceImpl.class);


    @Value("${services.provide.usagestats.sushiliteR5Endpoint}")
    private String usagestatsSushiliteR5Endpoint;


    public ResponseEntity<?> getReportResults(String report,
                                              String release,
                                              String requestorID,
                                              String beginDate,
                                              String endDate,
                                              String repositoryIdentifier,
                                              String datasetIdentifier,
                                              String itemIdentifier,
                                              List<String> metricTypes,
                                              String dataType,
                                              String granularity,
                                              String pretty) throws JSONException
    {
        //build the uri params
        UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(this.usagestatsSushiliteR5Endpoint + "reports/" + report.toLowerCase())
                .queryParam("Report", report)
                .queryParam("Release", release)
                .queryParam("RequestorID", requestorID)
                .queryParam("BeginDate", beginDate)
                .queryParam("EndDate", endDate)
                .queryParam("RepositoryIdentifier", repositoryIdentifier)
                .queryParam("DatasetIdentifier", datasetIdentifier)
                .queryParam("ItemIdentifier", itemIdentifier);
        if ( metricTypes != null ) {
            for ( String metricType : metricTypes )
                builder.queryParam("MetricType", metricType);
        }
        builder.queryParam("DataType", dataType)
                .queryParam("Granularity", granularity)
                .queryParam("Pretty", pretty);

        URI uri = builder.build().encode().toUri();

        RestTemplate restTemplate = new RestTemplate();
        restTemplate.getMessageConverters().add(new MappingJackson2HttpMessageConverter());

        ResponseEntity<?> resp;
        try {
            resp = restTemplate.exchange(
                    uri,
                    HttpMethod.GET,
                    null,
                    new ParameterizedTypeReference<Object>() {});   // THis "Object" must be a valid json-object.
        } catch (RestClientException rce) {
            String errorMsg = "Failed to get a response from sushiliteR5!";
            String message = rce.getMessage();
            if ( (message != null) && message.contains("504 Gateway Time-out") )    // In this case the parsing-exception is thrown before we get to check the response code..
                errorMsg += " Reason: 504 Gateway Time-out";

            logger.error(errorMsg + " URI was:\n" + uri + "\n" + message);
            return ResponseEntity.internalServerError().body(errorMsg);
        }

        HttpStatus httpStatus = resp.getStatusCode();
        if ( httpStatus != HttpStatus.OK ) {
            logger.warn("Sushi cannot give us data! Responded status: " + httpStatus);
            return ResponseEntity.status(httpStatus).build();
        }

        Object reportResult = resp.getBody();
        if ( reportResult == null ) {
            logger.error("The \"reportResponseWrapper\" for sushi was null!");
            return ResponseEntity.internalServerError().build();
        }
        logger.trace(reportResult.toString());

        return ResponseEntity.ok(reportResult);



        // TODO - Depending on the "report-type", map the "object" to the right Report-type class.
        // TODO - This will be useful, in case we add preprocessing steps, like pagination.
/*        try {
            switch ( report ) {
                case "PR":

                    break;
                case "PR_P1":

                    break;
                case "IR":
                    COUNTER_Item_Report counterItemReport = (COUNTER_Item_Report) resp.getBody();
                    if ( counterItemReport == null ) {
                        logger.error("The \"reportResponseWrapper\" for sushi was null!");
                        return ResponseEntity.internalServerError().build();
                    } else {
                        logger.debug(counterItemReport.toString());
                        return ResponseEntity.ok(counterItemReport);
                    }
                case "DSR":

                    break;
                default:
                    String errorMsg = "Invalid report type was given: " + report;
                    logger.error(errorMsg);
                    return ResponseEntity.badRequest().body(errorMsg);
            }
        } catch (ClassCastException cce) {
            logger.error("The report object could not be mapped to the repo-type-object of \"" + report + "\"!", cce);
            return ResponseEntity.internalServerError().build();
        }*/

    }

}
