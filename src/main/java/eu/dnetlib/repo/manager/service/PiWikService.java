package eu.dnetlib.repo.manager.service;


import eu.dnetlib.domain.data.PiwikInfo;
import eu.dnetlib.repo.manager.domain.OrderByField;
import eu.dnetlib.repo.manager.domain.OrderByType;
import eu.dnetlib.repo.manager.exception.RepositoryServiceException;
import org.springframework.http.ResponseEntity;

import java.util.List;


public interface PiWikService {

    PiwikInfo getPiwikSiteForRepo(String repositoryId);

    PiwikInfo savePiwikInfo(PiwikInfo piwikInfo);

    List<PiwikInfo> getPiwikSitesForRepos(OrderByField orderByField, OrderByType orderByType, int from, int quantity, String searchField);

    int getPiwikSitesTotals(String searchField);

    ResponseEntity<Object> approvePiwikSite(String repositoryId);

    String getOpenaireId(String repositoryid);

    ResponseEntity<Object> markPiwikSiteAsValidated(String repositoryId) throws RepositoryServiceException;

    PiwikInfo enableMetricsForRepository(String officialName, String repoWebsite, PiwikInfo piwikInfo) throws RepositoryServiceException;

    Integer getTotal();

    Integer getValidated(boolean validated);
}
