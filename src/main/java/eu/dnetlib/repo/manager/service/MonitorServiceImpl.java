package eu.dnetlib.repo.manager.service;

import eu.dnetlib.api.functionality.ValidatorService;
import eu.dnetlib.api.functionality.ValidatorServiceException;
import eu.dnetlib.domain.functionality.validator.StoredJob;
import eu.dnetlib.repo.manager.domain.Constants;
import eu.dnetlib.repo.manager.domain.JobsOfUser;
import eu.dnetlib.repo.manager.utils.CrisValidatorUtils;
import gr.uoa.di.driver.util.ServiceLocator;
import org.eurocris.openaire.cris.validator.model.Job;
import org.eurocris.openaire.cris.validator.service.MapJobDao;
import org.json.JSONException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service("monitorService")
public class MonitorServiceImpl implements MonitorService {

    private static final Logger logger = LoggerFactory.getLogger(MonitorServiceImpl.class);

    @Autowired
    private MapJobDao crisJobs;

    @Resource(name = "validatorServiceLocator")
    private ServiceLocator<ValidatorService> validatorServiceLocator;

    private ValidatorService getValidationService() {
        return this.validatorServiceLocator.getService();
    }

    public ServiceLocator<ValidatorService> getValidatorServiceLocator() {
        return validatorServiceLocator;
    }

    public void setValidatorServiceLocator(ServiceLocator<ValidatorService> validatorServiceLocator) {
        this.validatorServiceLocator = validatorServiceLocator;
    }


    @Override
    public JobsOfUser getJobsOfUser(String user,
                                    String jobType,
                                    String offset,
                                    String limit,
                                    String dateFrom,
                                    String dateTo,
                                    String validationStatus,
                                    String includeJobsTotal) throws ValidatorServiceException, NumberFormatException {

        /////////////////////////////////////////////////////////////////////////////////////////
        // FIXME: this is a hack for CRIS Jan Dvorak Validator, should be implemented properly //
        /////////////////////////////////////////////////////////////////////////////////////////
        int crisOffset = 0;
        int limitWithCris = 0;
        if (offset.equals("0")) {
            limitWithCris = crisJobs.getJobs(user, validationStatus).size();
        } else {
            crisOffset = crisJobs.getJobs(user, validationStatus).size();
        }

        offset = String.valueOf(Math.max(Integer.parseInt(offset) - crisOffset, 0));
        limit = String.valueOf(Math.max(Integer.parseInt(limit) - limitWithCris, 0));
        /////////////////////////////////////////////////////////////////////////////////////////
        /////////////////////////////////////////////////////////////////////////////////////////

        JobsOfUser retJobs = new JobsOfUser();
        retJobs.setJobs(getValidationService().getStoredJobsNew(user, jobType, Integer.parseInt(offset),
                Integer.parseInt(limit), dateFrom, dateTo, validationStatus));

        if (Boolean.parseBoolean(includeJobsTotal)) {
            retJobs.setTotalJobs(this.getJobsTotalNumberOfUser(user, jobType, null));
            retJobs.setTotalJobsSuccessful(this.getJobsTotalNumberOfUser(user, jobType, Constants.VALIDATION_JOB_STATUS_SUCCESSFUL));
            retJobs.setTotalJobsFailed(this.getJobsTotalNumberOfUser(user, jobType, Constants.VALIDATION_JOB_STATUS_FAILED));
            retJobs.setTotalJobsOngoing(this.getJobsTotalNumberOfUser(user, jobType, Constants.VALIDATION_JOB_STATUS_ONGOING));
        }

        //TODO fix status with new validator version
        if (retJobs.getJobs() != null) {
            for (StoredJob job : retJobs.getJobs()) {
                if (job.getContentJobStatus().equals("ongoing") || job.getUsageJobStatus().equals("ongoing")) {
                    job.setValidationStatus("ongoing");
                } else if ((job.getValidationType().equals("CU") && job.getContentJobStatus().equals("finished") && job.getUsageJobStatus().equals("finished") && job.getContentJobScore() > 50 && job.getUsageJobScore() > 50)
                        || (job.getValidationType().equals("C") && job.getContentJobStatus().equals("finished") && job.getUsageJobStatus().equals("none") && job.getContentJobScore() > 50)
                        || (job.getValidationType().equals("U") && job.getContentJobStatus().equals("none") && job.getUsageJobStatus().equals("finished") && job.getUsageJobScore() > 50)) {
                    job.setValidationStatus("successful");
                } else if ((job.getValidationType().equals("CU") && job.getContentJobStatus().equals("finished") && job.getUsageJobStatus().equals("finished") && (job.getContentJobScore() <= 50 || job.getUsageJobScore() <= 50))
                        || (job.getValidationType().equals("C") && job.getContentJobStatus().equals("finished") && job.getUsageJobStatus().equals("none") && job.getContentJobScore() <= 50)
                        || (job.getValidationType().equals("U") && job.getContentJobStatus().equals("none") && job.getUsageJobStatus().equals("finished") && job.getUsageJobScore() <= 50)) {
                    job.setValidationStatus("failed");
                }
            }
        }


        /////////////////////////////////////////////////////////////////////////////////////////
        // FIXME: this is a hack for CRIS Jan Dvorak Validator, should be implemented properly //
        /////////////////////////////////////////////////////////////////////////////////////////
        if (crisOffset == 0) {
            List<StoredJob> jobs = new ArrayList<>();

            List<Job> cj = crisJobs.getJobs(user, validationStatus);
            for (Job job : cj) {
                StoredJob sj = CrisValidatorUtils.convertJobToStoredJob(job);
                jobs.add(sj);

//                // filter out entries based on 'validationStatus'
//                if ("all".equals(validationStatus)) {
//                    jobs.add(sj);
//                } else {
//                    if (sj.getValidationStatus().equals(validationStatus)) {
//                        jobs.add(sj);
//                    }
//                }
            }

            // add to CRIS Jan jobs all other jobs
            if (retJobs.getJobs() != null) {
                jobs.addAll(retJobs.getJobs());
            }
            // set all jobs back to retJobs
            retJobs.setJobs(jobs);
        }

        // fix number of jobs
        if (Boolean.parseBoolean(includeJobsTotal)) {
            retJobs.setTotalJobs(retJobs.getTotalJobs() + crisJobs.getJobs(user).size());
            retJobs.setTotalJobsSuccessful(retJobs.getTotalJobsSuccessful() + crisJobs.getJobs(user, Job.Status.SUCCESSFUL.getKey()).size());
            retJobs.setTotalJobsFailed(retJobs.getTotalJobsFailed() + crisJobs.getJobs(user, Job.Status.FAILED.getKey()).size());
            retJobs.setTotalJobsOngoing(retJobs.getTotalJobsOngoing() + crisJobs.getJobs(user, Job.Status.ONGOING.getKey()).size());
        }
        /////////////////////////////////////////////////////////////////////////////////////////
        /////////////////////////////////////////////////////////////////////////////////////////

        return retJobs;
    }

    private int getJobsTotalNumberOfUser(String user, String jobType, String validationStatus) throws ValidatorServiceException {
        return getValidationService().getStoredJobsTotalNumberNew(user, jobType, validationStatus);
    }

    @Override
    public int getJobsOfUserPerValidationStatus(String user,
                                                String jobType,
                                                String validationStatus) throws JSONException {
        logger.debug("Getting job with validation status : " + validationStatus);

        if (jobType.equalsIgnoreCase("cris")) {
            return crisJobs.getJobs(user, validationStatus).size();
        }

        try {
            return getValidationService().getStoredJobsTotalNumberNew(user, jobType, validationStatus);
        } catch (ValidatorServiceException e) {
            logger.error(e.getMessage(), e);
        }
        return 0;
    }

    @Override
    public StoredJob getJobSummary(String jobId, String groupBy) {
        logger.debug("Getting job summary with id : " + jobId);
        StoredJob job = null;
        try {
            job = getValidationService().getStoredJob(Integer.parseInt(jobId), groupBy);
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
        }
        /////////////////////////////////////////////////////////////////////////////////////////
        // FIXME: this is a hack for CRIS Jan Dvorak Validator, should be implemented properly //
        /////////////////////////////////////////////////////////////////////////////////////////
        if (job == null) {
            // not a good way to do it but Job id field is string
            List<Job> cJobs = crisJobs.getAll().stream().filter(j -> j.getId().hashCode() == Integer.parseInt(jobId)).collect(Collectors.toList());
            if (!cJobs.isEmpty()) {
                job = CrisValidatorUtils.convertJobToStoredJob(cJobs.get(0));
            }
        }
        /////////////////////////////////////////////////////////////////////////////////////////
        /////////////////////////////////////////////////////////////////////////////////////////
        return job;
    }

}
