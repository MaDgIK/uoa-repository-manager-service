package eu.dnetlib.repo.manager.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponents;
import org.springframework.web.util.UriComponentsBuilder;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

@Service("statsService")
public class StatsServiceImpl implements StatsService {

    private static final Logger logger = LoggerFactory.getLogger(StatsServiceImpl.class);

    @Autowired
    RestTemplate restTemplate;

    @Value("${services.provide.clients.search}")
    private String baseAddress;
    @Value("${services.provide.clients.usagestats}")
    private String usagestatsBaseAddress;
    @Value("${services.provide.clients.usageEvents}")
    private String usagestatsEvents;

    @Override
    public Map getStatistics()  {
        Map<String,Object> stats = new HashMap<>();
        stats.put("aggregators", this.getAggregatorsStats());
        stats.put("dataRepositories", this.getDataRepositoriesMetadata());
        stats.put("literature", this.getLiteratureStats());
        stats.put("journal", this.getJournalsStats());
        stats.put("publications", this.getPublicationStats());
        stats.put("datasets", this.getDatasetsStats());
        stats.put("software", this.getSoftwareStats());
        stats.put("lastYearUsagestats", this.getLastYearUsageStats());
        stats.put("usagestats", this.getUsageStatsTotal());
        return stats;
    }


    private String getAggregatorsStats()
    {
        String url = baseAddress + "/resources" +
                "?query= " +
                " oaftype exact datasource and " +
                " ( datasourcetypename exact Institutional Repository Aggregator " +
                "     or datasourcetypename exact Publication Repository Aggregator )";

        UriComponents uriComponents = UriComponentsBuilder
                .fromHttpUrl(url)
                .queryParam("page", 0)
                .queryParam("size", 0)
                .queryParam("format", "json")
                .build().encode();

        try {
            ResponseEntity<Map> rs = restTemplate.exchange(uriComponents.toUri(), HttpMethod.GET, null, Map.class);
            Map metadata = (Map) ((Map<?, ?>) Objects.requireNonNull(rs.getBody())).get("meta");
            return String.valueOf(metadata.get("total"));
        } catch ( RestClientException rce ) {
            logger.error(rce.getMessage());
            return null;
        } catch ( Exception e ) {
            logger.error("", e);
            return null;
        }
    }


    private String getDataRepositoriesMetadata()
    {
        String url = baseAddress + "/resources" +
                "?query= " +
                " oaftype exact datasource and " +
                " datasourcetypename exact Data Repository ";

        return getMetadataTotals(url);
    }


    private String getLiteratureStats()
    {
        String url = baseAddress + "/resources" +
                "?query= " +
                " oaftype exact datasource and " +
                " ( datasourcetypename exact Institutional Repository  " +
                "     or datasourcetypename exact Publication Repository )";

        return getMetadataTotals(url);
    }


    private String getJournalsStats()
    {
        String url = baseAddress + "/resources" +
                "?query= " +
                " oaftype exact datasource and " +
                " datasourcetypename exact Journal";

        return getMetadataTotals(url);
    }


    private String getPublicationStats()
    {
        String url = baseAddress + "/publications/count";

        return getCommonStats(url);
    }


    private String getDatasetsStats()
    {
        String url = baseAddress + "/datasets/count";

        return getCommonStats(url);
    }


    private String getSoftwareStats()
    {
        String url = baseAddress + "/software/count";

        return getCommonStats(url);
    }


    private Map<String,Object> getLastYearUsageStats()
    {
        UriComponents uriComponents = UriComponentsBuilder
                .fromHttpUrl(usagestatsBaseAddress + "/totals")
                .build().encode();

        try {
            ResponseEntity<Map> rs = restTemplate.exchange(uriComponents.toUri(), HttpMethod.GET, null, Map.class);

            List yearly_stats = (List) ((Map<?, ?>) Objects.requireNonNull(rs.getBody())).get("yearly_stats");
            Map lastYear = (Map) yearly_stats.get(yearly_stats.size() - 2);
            Integer downloads = (Integer) lastYear.get("downloads");
            Integer views = (Integer) lastYear.get("views");
            Integer year = (Integer) lastYear.get("year");

            Map<String, Object> usagestats = new HashMap<>();
            usagestats.put("number", String.valueOf(downloads + views));
            usagestats.put("year", year);
            return usagestats;
        } catch ( RestClientException rce ) {
            logger.error(rce.getMessage());
            return null;
        } catch ( Exception e ) {
            logger.error("", e);
            return null;
        }
    }


    private Integer getUsageStatsTotal()
    {
        UriComponents uriComponents = UriComponentsBuilder
                .fromHttpUrl(usagestatsEvents)
                .build().encode();

        try {
            ResponseEntity<Map> rs = restTemplate.exchange(uriComponents.toUri(), HttpMethod.GET, null, Map.class);
            Map statsTotals = (Map) ((Map<?, ?>) Objects.requireNonNull(rs.getBody())).get("totals");
            if ( statsTotals == null ) {
                logger.error("The statsTotals was null!");
                return null;
            }

            // TODO - Will we ever need the following lines?
            // String rs = restTemplate.getForObject(uriComponents.toUri(), String.class);
            // JSONObject resultSet = new JSONObject(rs);
            // JSONObject totals = resultSet.getJSONObject("totals");

            return (Integer) statsTotals.get("events");
        } catch ( RestClientException rce ) {
            logger.error(rce.getMessage());
            return null;
        } catch ( Exception e ) {
            logger.error("", e);
            return null;
        }
    }


    private String getMetadataTotals(String url) {
        UriComponents uriComponents = UriComponentsBuilder
                .fromHttpUrl(url)
                .queryParam("page", 0)
                .queryParam("size", 0)
                .queryParam("format", "json")
                .build().encode();

        try {
            ResponseEntity<Map> rs = restTemplate.exchange(uriComponents.toUri(), HttpMethod.GET, null, Map.class);
            Map metadata = (Map) ((Map<?, ?>) Objects.requireNonNull(rs.getBody())).get("meta");
            if ( metadata == null ) {
                logger.error("The metadata was null!");
                return null;
            }
            return String.valueOf(metadata.get("total"));
        } catch ( RestClientException rce ) {
            logger.error(rce.getMessage());
            return null;
        } catch ( Exception e ) {
            logger.error("", e);
            return null;
        }
    }


    private String getCommonStats(String url) {
        UriComponents uriComponents = UriComponentsBuilder
                .fromHttpUrl(url)
                .queryParam("page", 0)
                .queryParam("size", 0)
                .queryParam("format", "json")
                .build().encode();

        try {
            ResponseEntity<Map> rs = restTemplate.exchange(uriComponents.toUri(), HttpMethod.GET, null, Map.class);
            Map stats = (Map<?, ?>) rs.getBody();
            if ( stats == null ) {
                logger.error("The stats was null!");
                return null;
            }
            return String.valueOf(stats.get("total"));
        } catch ( RestClientException rce ) {
            logger.error(rce.getMessage());
            return null;
        } catch ( Exception e ) {
            logger.error("", e);
            return null;
        }
    }

}
