package eu.dnetlib.repo.manager.service.sushilite;

import eu.dnetlib.usagestats.sushilite.domain.Customer;
import eu.dnetlib.usagestats.sushilite.domain.ReportItem;
import eu.dnetlib.usagestats.sushilite.domain.ReportResponseWrapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import java.util.ArrayList;
import java.util.List;

@Service("sushiliteService")
public class SushiliteServiceImpl implements SushiliteService {


    @Value("${services.provide.usagestats.sushiliteEndpoint}")
    private String usagestatsSushiliteEndpoint;

    private static final Logger logger = LoggerFactory.getLogger(SushiliteServiceImpl.class);


    @Override
    @PreAuthorize("hasAuthority('REGISTERED_USER')")
    public ReportResponseWrapper getReportResults(String page,
                                                  String pageSize,
                                                  String Report,
                                                  String Release,
                                                  String RequestorID,
                                                  String BeginDate,
                                                  String EndDate,
                                                  String RepositoryIdentifier,
                                                  String ItemIdentifier,
                                                  String ItemDataType,
                                                  String Granularity,
                                                  String Pretty) {

        //build the uri params
        UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(this.usagestatsSushiliteEndpoint + "GetReport/")
                                                            .queryParam("Report", Report)
                                                            .queryParam("Release", Release)
                                                            .queryParam("RequestorID", RequestorID)
                                                            .queryParam("BeginDate", BeginDate)
                                                            .queryParam("EndDate", EndDate)
                                                            .queryParam("RepositoryIdentifier", RepositoryIdentifier)
                                                            .queryParam("ItemIdentifier", ItemIdentifier)
                                                            .queryParam("ItemDataType", ItemDataType)
                                                            .queryParam("Granularity", Granularity)
                                                            .queryParam("Pretty", Pretty);

        //create new restTemplate engine
        RestTemplate restTemplate = new RestTemplate();
        restTemplate.getMessageConverters().add(new MappingJackson2HttpMessageConverter());

        ResponseEntity<ReportResponseWrapper> resp;

        try {
            //communicate with endpoint
            resp = restTemplate.exchange(
                    builder.build().encode().toUri(),
                    HttpMethod.GET,
                    null,
                    new ParameterizedTypeReference<ReportResponseWrapper>() {});
        } catch (RestClientException rce) {
            logger.error("", rce);
            return null;
        }

        // check remote api's response
        HttpStatus httpStatus = resp.getStatusCode();
        if ( httpStatus != HttpStatus.OK ) {
            logger.warn("Sushi cannot give us data! Responded status: " + httpStatus);
            return null;
        }

        ReportResponseWrapper reportResponseWrapper = resp.getBody();
        if ( reportResponseWrapper == null ) {
            logger.error("The \"reportResponseWrapper\" for sushi was null!");
            return null;
        }

        // get the items corresponding to the requested page
        List<ReportItem> requestedItemList = new ArrayList<>();

        Customer customer = reportResponseWrapper.getReportResponse().getReportWrapper().getReport().getCustomer();

        List<ReportItem> allReportItems = customer.getReportItems();
        if ( allReportItems != null ) {
            try {
                int totalItems = allReportItems.size();
                int size = Integer.parseInt(pageSize);
                int offset = (Integer.parseInt(page) * size);
                if ( offset < totalItems ) {
                    int upperIndex = (offset + size);
                    if ( upperIndex > totalItems ) {
                        upperIndex = totalItems;
                    }
                    requestedItemList = allReportItems.subList(offset, upperIndex);
                }
            } catch (NumberFormatException e) {
                logger.error("Exception on getReportResults - trying to cast strings to integers", e);
                throw e;
            }

            customer.setReportItems(requestedItemList); // Setting the reportItems to the "customer"-reference, updates the "reportResponseWrapper" object.
        }

        return reportResponseWrapper;
    }

}
