package eu.dnetlib.repo.manager.service.security;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;

import java.util.Collection;

public interface RoleMappingService {

    /**
     * @param roleId Role Id
     * @return Converts {@param roleId} to a repo Id.
     */
    String getRepositoryId(String roleId);

    /**
     * @param roleIds Collection of roles
     * @return Converts {@param roleIds} to a repo Ids.
     */
    Collection<String> getRepositoryIds(Collection<String> roleIds);

    /**
     * @param repoId Repository Id
     * @return Converts {@param repoId} to a role Id.
     */
    String getRole(String repoId);

    /**
     * @param repoIds Collection of Repository Ids
     * @return Converts {@param repoIds} to role Ids.
     */
    Collection<String> getRoles(Collection<String> repoIds);

    /**
     * @param authority {@link GrantedAuthority}
     * @return Converts {@param authority} to repository Id.
     */
    String authorityToRepositoryId(GrantedAuthority authority);

    /**
     * @param repoId Repository Id
     * @return Converts {@param repoId} to {@link SimpleGrantedAuthority} with the role url encoded ($ -> %24)
     * // TODO: remove role encoding and perform url decoding when mapping authorities. (Must be performed in all OpenAIRE projects because of Redis)
     */
    GrantedAuthority repositoryIdToAuthority(String repoId);

}
