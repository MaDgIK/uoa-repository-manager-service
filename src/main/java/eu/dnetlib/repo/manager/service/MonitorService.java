package eu.dnetlib.repo.manager.service;

import eu.dnetlib.api.functionality.ValidatorServiceException;
import eu.dnetlib.domain.functionality.validator.StoredJob;
import eu.dnetlib.repo.manager.domain.JobsOfUser;
import org.json.JSONException;


public interface MonitorService {


    JobsOfUser getJobsOfUser(String user,
                             String jobType,
                             String offset,
                             String limit,
                             String dateFrom,
                             String dateTo,
                             String validationStatus,
                             String includeJobsTotal) throws JSONException, ValidatorServiceException;

    int getJobsOfUserPerValidationStatus(String user,
                                         String jobType,
                                         String validationStatus) throws JSONException;


    StoredJob getJobSummary(String jobId,
                            String groupBy) throws JSONException;

}
