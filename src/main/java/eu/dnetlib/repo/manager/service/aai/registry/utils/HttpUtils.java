package eu.dnetlib.repo.manager.service.aai.registry.utils;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import org.apache.commons.codec.binary.Base64;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.*;
import org.springframework.stereotype.Component;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponents;
import org.springframework.web.util.UriComponentsBuilder;

import java.nio.charset.StandardCharsets;
import java.util.Collections;
import java.util.Map;

@Component
public class HttpUtils {

    private static final Logger logger = LoggerFactory.getLogger(HttpUtils.class);

    @Value("${services.provide.aai.registry.url}")
    private String registryUrl;

    @Value("${services.provide.aai.registry.username}")
    private String user;

    @Value("${services.provide.aai.registry.password}")
    private String password;

    public JsonElement post(String path, JsonObject body) throws RestClientException {
        RestTemplate restTemplate = new RestTemplate();
        HttpHeaders headers = createHeaders(user, password);
        headers.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<String> request = new HttpEntity<>(body.toString(), headers);
        ResponseEntity<String> responseEntity = restTemplate.exchange(registryUrl + path, HttpMethod.POST, request, String.class);
        return getResponseEntityAsJsonElement(responseEntity);
    }

    public JsonElement put(String path, JsonObject body) throws RestClientException {
        RestTemplate restTemplate = new RestTemplate();
        HttpHeaders headers = createHeaders(user, password);
        headers.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<String> request = new HttpEntity<>(body.toString(), headers);
        ResponseEntity<String> responseEntity = restTemplate.exchange(registryUrl + path, HttpMethod.PUT, request, String.class);
        return getResponseEntityAsJsonElement(responseEntity);
    }

    public JsonElement get(String path, Map<String, String> params) throws RestClientException {
        RestTemplate restTemplate = new RestTemplate();
        String url = createUrl(registryUrl + path, params);
        ResponseEntity<String> responseEntity = restTemplate.exchange
                (url, HttpMethod.GET, new HttpEntity<>(createHeaders(user, password)), String.class);
        return getResponseEntityAsJsonElement(responseEntity);
    }

    public JsonElement delete(String path) throws RestClientException {
        RestTemplate restTemplate = new RestTemplate();
        String url = registryUrl + path;
        ResponseEntity<String> responseEntity = restTemplate.exchange(url, HttpMethod.DELETE, new HttpEntity<>(createHeaders(user, password)), String.class);
        return getResponseEntityAsJsonElement(responseEntity);
    }

    private String createUrl(String baseAddress, Map<String, String> params) {
        LinkedMultiValueMap<String, String> multiValueMap = new LinkedMultiValueMap<>();
        params.forEach((k, v) -> multiValueMap.put(k, Collections.singletonList(v)));
        UriComponents uriComponents = UriComponentsBuilder
                .fromHttpUrl(baseAddress)
                .queryParams(multiValueMap)
                .build().encode();
        return uriComponents.toString();
    }

    private HttpHeaders createHeaders(String username, String password) {
        return new HttpHeaders() {{
            String auth = username + ":" + password;
            byte[] encodedAuth = Base64.encodeBase64(auth.getBytes(StandardCharsets.US_ASCII));
            String authHeader = "Basic " + new String(encodedAuth);
            set("Authorization", authHeader);
        }};
    }

    private JsonElement getResponseEntityAsJsonElement(ResponseEntity<String> responseEntity) {

        if (responseEntity == null)
            return null;

        String responseBody = responseEntity.getBody();
        if (responseBody != null) {
            logger.trace(responseBody);

            try {
                return new JsonParser().parse(responseBody);
            } catch (Exception e) {
                logger.warn("Could not parse response body", e);    // Will return null.
            }
        }
        return null;
    }
}
