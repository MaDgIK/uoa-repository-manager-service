package eu.dnetlib.repo.manager.service;

import eu.dnetlib.openaire.exporter.model.dsm.AggregationInfo;
import eu.dnetlib.repo.manager.domain.MetricsNumbers;
import eu.dnetlib.repo.manager.domain.RepositorySnippet;
import eu.dnetlib.repo.manager.domain.RepositorySummaryInfo;
import eu.dnetlib.repo.manager.domain.broker.BrowseEntry;
import eu.dnetlib.repo.manager.exception.BrokerException;
import eu.dnetlib.repo.manager.exception.RepositoryServiceException;
import eu.dnetlib.repo.manager.utils.DateUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service("dashboardService")
public class DashboardServiceImpl implements DashboardService {

    private static final Logger logger = LoggerFactory.getLogger(DashboardServiceImpl.class);

    @Autowired
    private RepositoryService repositoryService;

    @Autowired
    private AggregationService aggregationService;

    @Autowired
    private BrokerService brokerService;

    @Override
    public List<RepositorySummaryInfo> getRepositoriesSummaryInfo(String userEmail,
                                                                  String page,
                                                                  String size) {

        List<RepositorySummaryInfo> repositorySummaryInfoList = new ArrayList<>();

        try {
            List<RepositorySnippet> repositoriesOfUser = repositoryService.getRepositoriesSnippetsOfUser(userEmail, page, size);
            for (RepositorySnippet repository : repositoriesOfUser)
            {
                String repoId = repository.getId();
                String repoOfficialName = repository.getOfficialname();

                RepositorySummaryInfo repositorySummaryInfo = new RepositorySummaryInfo();
                repositorySummaryInfo.setId(repoId);
                repositorySummaryInfo.setRepositoryName(repoOfficialName);
                repositorySummaryInfo.setLogoURL(repository.getLogoUrl());

                //TODO getRepositoryAggregations returns only the 20 more recent items. Is it possible that we will find an indexed version there?
                boolean isIndexedVersionFound = false;
                long start = System.currentTimeMillis();
                List<AggregationInfo> aggregationInfoList = aggregationService.getRepositoryAggregations(repoId, 0, 20);
                for (AggregationInfo aggregationInfo : aggregationInfoList) {
                    if (aggregationInfo.isIndexedVersion()) {
                        repositorySummaryInfo.setRecordsCollected(aggregationInfo.getNumberOfRecords());
                        repositorySummaryInfo.setLastIndexedVersion(DateUtils.toDate(aggregationInfo.getDate()));
                        isIndexedVersionFound = true;
                        break;
                    }
                }
                long end = System.currentTimeMillis();
                if ( isIndexedVersionFound )
                    logger.debug("Got repo aggregations in " + (end - start) + "ms");
                else
                    logger.warn("Could not find repo aggregations, after " + (end - start) + "ms!");

                try {
                    MetricsNumbers metricsNumbers = repositoryService.getMetricsInfoForRepository(repoId).getMetricsNumbers();
                    repositorySummaryInfo.setTotalDownloads(metricsNumbers.getTotalDownloads());
                    repositorySummaryInfo.setTotalViews(metricsNumbers.getTotalViews());
                } catch (RepositoryServiceException e) {
                    logger.error("Exception getting metrics info for repository: {}, {} ", repoId, repoOfficialName, e);
                }

                try {
                    List<BrowseEntry> events = brokerService.getTopicsForDatasource(repoOfficialName);
                    Long totalEvents = 0L;
                    for (BrowseEntry browseEntry : events)
                        totalEvents += browseEntry.getSize();
                    repositorySummaryInfo.setEnrichmentEvents(totalEvents);
                } catch (BrokerException e) {
                    logger.error("Exception getting broker events for repository: {}, {} ", repoId, repoOfficialName, e);
                }

                repositorySummaryInfoList.add(repositorySummaryInfo);
            }
        } catch (Exception e) {
            logger.error("Something baad happened!", e);
        }

        return repositorySummaryInfoList;
    }
}
