package eu.dnetlib.repo.manager.service;

import org.mitre.openid.connect.model.OIDCAuthenticationToken;
import org.mitre.openid.connect.model.UserInfo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Service("userService")
public class UserServiceImpl implements UserService {

    private static final Logger logger = LoggerFactory.getLogger(UserServiceImpl.class);

    @Override
    public ResponseEntity<Object> login() {
        OIDCAuthenticationToken authentication = (OIDCAuthenticationToken) SecurityContextHolder.getContext().getAuthentication();
        logger.debug("User authentication : " + authentication);
        Map<String,Object> body = new HashMap<>();
        body.put("sub",authentication.getSub());

        UserInfo userInfo = authentication.getUserInfo();
        String userName = userInfo.getName();

        if ( userName == null || userName.isEmpty() )
            body.put("name", userInfo.getGivenName() + " " + userInfo.getFamilyName());
        else
            body.put("name", userName);

        body.put("email",userInfo.getEmail());
        List<String> roles = authentication.getAuthorities().stream().map(GrantedAuthority::getAuthority).collect(Collectors.toList());
        body.put("role",roles);

        return new ResponseEntity<>(body, HttpStatus.OK);
    }
}
