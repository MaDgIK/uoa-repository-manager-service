package eu.dnetlib.repo.manager.service;

import eu.dnetlib.openaire.exporter.model.dsm.AggregationHistoryResponseV2;
import eu.dnetlib.openaire.exporter.model.dsm.AggregationInfo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponents;
import org.springframework.web.util.UriComponentsBuilder;

import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static eu.dnetlib.repo.manager.utils.DateUtils.getYear;

@Service("aggregationService")
public class AggregationServiceImpl implements AggregationService {

    private static final Logger logger = LoggerFactory.getLogger(AggregationServiceImpl.class);


    @Value("${services.provide.clients.dsm}")
    private String baseAddress;

    private final RestTemplate restTemplate;


    public AggregationServiceImpl(RestTemplate restTemplate) {
        this.restTemplate = restTemplate;
    }


    @Override
    public <T extends AggregationInfo> List<T> getRepositoryAggregations(String id) {

        logger.debug("Retrieving aggregations for repository with id : " + id);
        UriComponents uriComponents = getAggregationHistory(id);

        AggregationHistoryResponseV2 rs = restTemplate.getForObject(uriComponents.toUri(), AggregationHistoryResponseV2.class);

        return rs != null ? (List<T>) rs.getAggregationInfo() : null;
    }

    @Override
    public <T extends AggregationInfo> List<T> getRepositoryAggregations(String id, int from, int size) {

        List<T> res = getRepositoryAggregations(id);

        return res.subList(from, Math.min(from + size, res.size()));
    }

    @Override
    public <T extends AggregationInfo> Map<String, List<T>> getRepositoryAggregationsByYear(String id) {
        logger.debug("Retrieving aggregations (by year) for repository with id : " + id);

        List<T> aggregationHistory = getRepositoryAggregations(id);

        return aggregationHistory.isEmpty() ? new HashMap<>() : createYearMap(aggregationHistory);
    }

    private <T extends AggregationInfo> Map<String, List<T>> createYearMap(List<T> aggregationHistory) {

        return aggregationHistory.stream()
                .sorted(Comparator.comparing(AggregationInfo::getDate).reversed())
                .collect(Collectors.groupingBy(item -> getYear(item.getDate())));
    }


    private UriComponents getAggregationHistory(String repoId) {
        return UriComponentsBuilder
                .fromHttpUrl(baseAddress + "/dsm/2.0/aggregationhistory/")
                .path(repoId)
                .build().expand(repoId).encode();
    }

}
