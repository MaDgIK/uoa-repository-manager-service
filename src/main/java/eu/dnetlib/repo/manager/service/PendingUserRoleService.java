package eu.dnetlib.repo.manager.service;

import eu.dnetlib.repo.manager.domain.PendingUserRole;
import eu.dnetlib.repo.manager.repository.PendingUserRoleRepository;
import eu.dnetlib.repo.manager.service.aai.registry.AaiRegistryService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

@Service
public class PendingUserRoleService {

    private static final Logger logger = LoggerFactory.getLogger(PendingUserRoleService.class);
    private final PendingUserRoleRepository pendingUserRoleRepository;
    private final AaiRegistryService aaiRegistryService;

    public PendingUserRoleService(PendingUserRoleRepository pendingUserRoleRepository,
                                  AaiRegistryService aaiRegistryService) {
        this.pendingUserRoleRepository = pendingUserRoleRepository;
        this.aaiRegistryService = aaiRegistryService;
    }

    public void assignRoles() {
        Iterable<PendingUserRole> roles = pendingUserRoleRepository.findAll();
        for (PendingUserRole role : roles) {
            logger.debug("Attempt to assign role: {}", role);
            try {
                aaiRegistryService.assignMemberRole(role.getCoPersonId(), role.getCouId());
                pendingUserRoleRepository.deleteById(role.getId());
            } catch (Exception e) {
                logger.warn("Could not assign role to user. Pending Role: {}\n", role, e);
            }
        }
    }

}
