package eu.dnetlib.repo.manager.components;


import eu.dnetlib.repo.manager.service.PendingUserRoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

@Component
public class ScheduledTasks {

    @Autowired
    PendingUserRoleService pendingUserRoleService;

    @Scheduled(fixedRate = 3_600_000)
    public void assignPendingRoles() {
        pendingUserRoleService.assignRoles();
    }

}
