package eu.dnetlib.repo.manager.exception;



/**
 * Created by nikonas on 7/12/15.
 */
public class UserAccessException extends Exception   {

    public enum ErrorCode   {
        USER_ALREADY_EXISTS,
        SQL_ERROR,
        INVALID_USERNAME,
        INVALID_PASSWORD,
        NOT_ACTIVATED,
        ACTIVATION_ERROR,
        LDAP_ERROR,
        USERNAME_ALREADY_EXISTS, MAIL_ALREADY_EXISTS, GENERAL_ERROR, ALREADY_ACTIVATED, INVALID_EMAIL_FORMAT, WRONG_SECURITY_CODE, INCORRECT_CAPTCHA
    }

    private ErrorCode errorCode = null;

    public UserAccessException() {
    }

    public UserAccessException(ErrorCode errorCode) {
        this.errorCode = errorCode;
    }

    public UserAccessException(String message, Throwable cause, ErrorCode errorCode) {
        super(message, cause);

        this.errorCode = errorCode;
    }

    public UserAccessException(String message, ErrorCode errorCode) {
        super(message);

        this.errorCode = errorCode;
    }

    public UserAccessException(Throwable cause, ErrorCode errorCode) {
        super(cause);

        this.errorCode = errorCode;
    }

    public ErrorCode getErrorCode() {
        return errorCode;
    }
}
