package eu.dnetlib.repo.manager.exception;

public class ServerError {

    public final String url;
    public final String error;

    public ServerError(String url, Exception ex) {
        this.url = url;
        this.error = ex.getMessage();
    }

    public String getUrl() {
        return this.url;
    }

    public String getError() {
        return this.error;
    }

}
