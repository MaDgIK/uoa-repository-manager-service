package eu.dnetlib.repo.manager.exception;


/**
 * Created by stefanos on 27-Oct-16.
 */
public class BrokerException extends Exception {

    public BrokerException(Throwable th) {
        super(th);
    }

    public BrokerException() {
    }


}
