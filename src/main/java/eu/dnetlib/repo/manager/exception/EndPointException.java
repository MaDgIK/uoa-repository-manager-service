package eu.dnetlib.repo.manager.exception;

import java.io.IOException;

//@ResponseStatus(HttpStatus.GATEWAY_TIMEOUT)
public class EndPointException extends IOException {

    public EndPointException() {
            super("Endpoint not responding!");
        }

    public EndPointException(String url) {
            super("Endpoint with url: " + url + " not responding!");
        }

}
