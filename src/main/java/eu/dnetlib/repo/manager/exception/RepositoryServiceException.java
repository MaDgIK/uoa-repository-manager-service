package eu.dnetlib.repo.manager.exception;

/**
 * Created by nikonas on 7/12/15.
 */
public class RepositoryServiceException extends Exception {

    public enum ErrorCode{
        REPOSITORY_ALREADY_EXISTS,
        SQL_ERROR,
        NOT_ACTIVATED,
        ACTIVATION_ERROR,
        LDAP_ERROR,
        MAIL_ALREADY_EXISTS,
        GENERAL_ERROR,
        ALREADY_ACTIVATED,
        INVALID_EMAIL_FORMAT,
        NO_REPOS_FOR_THIS_COUNTRY, REPOSITORY_NOT_EXISTS, INCORRECT_CAPTCHA
    }

    private ErrorCode errorCode = null;

    public RepositoryServiceException() {
    }

    public RepositoryServiceException(ErrorCode errorCode) {
        this.errorCode = errorCode;
    }

    public RepositoryServiceException(String message, Throwable cause, ErrorCode errorCode) {
        super(message, cause);

        this.errorCode = errorCode;
    }

    public RepositoryServiceException(String message, ErrorCode errorCode) {
        super(message);

        this.errorCode = errorCode;
    }

    public RepositoryServiceException(Throwable cause, ErrorCode errorCode) {
        super(cause);

        this.errorCode = errorCode;
    }

    public ErrorCode getErrorCode() {
        return errorCode;
    }
}
