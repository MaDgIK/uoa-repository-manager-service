package eu.dnetlib.repo.manager.exception;

/**
 * Created by nikonas on 7/12/15.
 */
public class ValidationServiceException extends Exception{

    public enum ErrorCode{
        NO_ADMIN_EMAILS,
        NOT_VALID_BASEURL,
        GENERAL_ERROR, NOT_VALID_SET
    }

    private ErrorCode errorCode = null;

    public ValidationServiceException() {
    }

    public ValidationServiceException(ErrorCode errorCode) {
        this.errorCode = errorCode;
    }

    public ValidationServiceException(String message, Throwable cause, ErrorCode errorCode) {
        super(message, cause);

        this.errorCode = errorCode;
    }

    public ValidationServiceException(String message, ErrorCode errorCode) {
        super(message);

        this.errorCode = errorCode;
    }

    public ValidationServiceException(Throwable cause, ErrorCode errorCode) {
        super(cause);

        this.errorCode = errorCode;
    }

    public ErrorCode getErrorCode() {
        return errorCode;
    }
}
