package eu.dnetlib.repo.manager.repository;

import eu.dnetlib.repo.manager.domain.PendingUserRole;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PendingUserRoleRepository extends CrudRepository<PendingUserRole, Long> {
}
