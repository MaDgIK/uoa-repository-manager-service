package eu.dnetlib.repo.manager.repository;

import eu.dnetlib.repo.manager.domain.InterfaceComplianceRequest;
import eu.dnetlib.repo.manager.domain.InterfaceComplianceRequestId;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.Set;

@Repository
public interface InterfaceComplianceRequestsRepository extends CrudRepository<InterfaceComplianceRequest, InterfaceComplianceRequestId> {

    Set<InterfaceComplianceRequest> findAllBySubmissionDateBefore(Date submittedBefore);
}
