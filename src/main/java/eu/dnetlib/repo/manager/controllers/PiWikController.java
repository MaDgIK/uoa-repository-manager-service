package eu.dnetlib.repo.manager.controllers;

import eu.dnetlib.domain.data.PiwikInfo;
import eu.dnetlib.repo.manager.domain.OrderByField;
import eu.dnetlib.repo.manager.domain.OrderByType;
import eu.dnetlib.repo.manager.domain.Paging;
import eu.dnetlib.repo.manager.exception.RepositoryServiceException;
import eu.dnetlib.repo.manager.service.PiWikServiceImpl;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.Parameters;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.FileSystemResource;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.file.Files;
import java.nio.file.Path;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

@RestController
@RequestMapping(value = "/piwik")
@Tag(name="piwik", description = "Piwik API")
public class PiWikController {

    private static final Logger logger = LoggerFactory.getLogger(PiWikController.class);

    @Autowired
    private PiWikServiceImpl piWikService;

    @RequestMapping(value = "/validated", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public Boolean isPiwikValidated(@RequestParam("repositoryId") String repositoryId) {
        PiwikInfo info = piWikService.getPiwikSiteForRepo(repositoryId);
        if (info != null) {
            return info.isValidated();
        }
        return false;
    }

    @RequestMapping(value = "/getPiwikSiteForRepo/{repositoryId}" , method = RequestMethod.GET,produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    @PreAuthorize("hasAnyAuthority('SUPER_ADMINISTRATOR', 'CONTENT_PROVIDER_DASHBOARD_ADMINISTRATOR') or @authorizationService.isMemberOf(#repositoryId) or (@repositoryService.getRepositoryById(#repositoryId).registeredby==null and hasAuthority('REGISTERED_USER'))")
    public PiwikInfo getPiwikSiteForRepo(@PathVariable("repositoryId") String repositoryId) {
        return piWikService.getPiwikSiteForRepo(repositoryId);
    }

    @RequestMapping(value = "/savePiwikInfo" , method = RequestMethod.POST,produces = MediaType.APPLICATION_JSON_VALUE)
    @PreAuthorize("hasAnyAuthority('SUPER_ADMINISTRATOR', 'CONTENT_PROVIDER_DASHBOARD_ADMINISTRATOR') or @authorizationService.isMemberOf(#piwikInfo.repositoryId) or (@repositoryService.getRepositoryById(#piwikInfo.repositoryId).registeredby==null and hasAuthority('REGISTERED_USER'))")
    public PiwikInfo savePiwikInfo(@RequestBody PiwikInfo piwikInfo) {
        return piWikService.savePiwikInfo(piwikInfo);
    }

    @RequestMapping(value = "/getPiwikSitesForRepos" , method = RequestMethod.GET,produces = MediaType.APPLICATION_JSON_VALUE)
    @Parameters({
            @Parameter(name = "from", description = "number"),
            @Parameter(name = "quantity", description = "number"),
            @Parameter(name = "order", description = "eu.dnetlib.repo.manager.domain.OrderByType"),
            @Parameter(name = "orderField", description = "eu.dnetlib.repo.manager.domain.OrderByField"),
            @Parameter(name = "searchField", description = "string")
    })
    public Paging<PiwikInfo> getPiwikSitesForRepos(
            @RequestParam(value = "from", required=false, defaultValue = "0") int from,
            @RequestParam(value = "quantity", required=false, defaultValue = "100") int quantity,
            @RequestParam(value = "order", required=false, defaultValue = "DSC") OrderByType orderType,
            @RequestParam(value = "orderField", required = false, defaultValue = "REPOSITORY_NAME") OrderByField orderField,
            @RequestParam(value = "searchField", required = false, defaultValue = "") String searchField

    ){
        Paging<PiwikInfo> results = new Paging<>();
        List<PiwikInfo> returning = piWikService.getPiwikSitesForRepos(orderField,orderType,from,quantity,searchField);
        results.setFrom(from);
        results.setTo(from + returning.size());
        results.setTotal(piWikService.getPiwikSitesTotals(searchField));
        results.setResults(returning);
        return results;
    }

    @Parameters({
            @Parameter(name = "from", description = "number"),
            @Parameter(name = "quantity", description = "number"),
            @Parameter(name = "order", description = "eu.dnetlib.repo.manager.domain.OrderByType"),
            @Parameter(name = "searchField", description = "eu.dnetlib.repo.manager.domain.OrderByField"),
            @Parameter(name = "orderField", description = "string")
    })
    @RequestMapping(value = "/getPiwikSitesForRepos/csv" , method = RequestMethod.GET,produces = MediaType.APPLICATION_OCTET_STREAM_VALUE)
    @ResponseBody
    public FileSystemResource getPiwikSitesForReposToCsv(
            @RequestParam(value = "from",required=false,defaultValue = "0") int from,
            @RequestParam(value = "quantity",required=false,defaultValue = "10000") int quantity,
            @RequestParam(value = "order",required=false,defaultValue = "DSC") OrderByType orderType,
            @RequestParam(value = "orderField", required = false, defaultValue = "REPOSITORY_NAME") OrderByField orderField,
            @RequestParam(value = "searchField", required = false, defaultValue = "") String searchField,
            HttpServletResponse response,
            HttpServletRequest request
    ) throws IOException {

        Path p = Files.createTempFile("exportingCsv-", new Date().toString());
        List<PiwikInfo> returning = piWikService.getPiwikSitesForRepos(orderField,orderType,0,10000,searchField);
        try (PrintWriter writer = new PrintWriter(p.toFile())) {

            StringBuilder sb = new StringBuilder();
            sb.append(" Repository ID , Repository name, Country, Site ID, Authentication token, Creation date, Requestor full name, Requestor email, Validated, Validation date, Comment \n");

            for(PiwikInfo piwikInfo : returning){
                sb.append(piwikInfo.getRepositoryId() == null ? "," : piwikInfo.getRepositoryId() + ",")
                        .append(piwikInfo.getRepositoryName() == null ? "," : piwikInfo.getRepositoryName() + ",")
                        .append(piwikInfo.getCountry() == null ? "," : piwikInfo.getCountry() + ",")
                        .append(piwikInfo.getSiteId() == null ? "," : piwikInfo.getSiteId() + ",")
                        .append(piwikInfo.getAuthenticationToken() == null ? "," : piwikInfo.getAuthenticationToken() + ",")
                        .append(piwikInfo.getCreationDate() == null ? "," : piwikInfo.getCreationDate().toString() + ",")
                        .append(piwikInfo.getRequestorName() == null ? "," : piwikInfo.getRequestorName() + ",")
                        .append(piwikInfo.getRequestorEmail() == null ? "," : piwikInfo.getRequestorEmail() + ",")
                        .append(piwikInfo.isValidated()).append(",")
                        .append(piwikInfo.getValidationDate() == null ? "," : piwikInfo.getValidationDate().toString() + ",")
                        .append(piwikInfo.getComment() == null ? "\n" : piwikInfo.getComment() + "\n");
            }
            writer.write(sb.toString());

        } catch (FileNotFoundException e) {
            logger.error(e.getMessage());
        }


        String mimeType = request.getServletContext().getMimeType(p.toFile().getAbsolutePath());
        if (mimeType == null) {
            mimeType = "application/octet-stream";
        }
        response.setContentType(mimeType);
        response.setContentLength((int) p.toFile().length());


        String headerKey = "Content-Disposition";
        SimpleDateFormat sdfDate = new SimpleDateFormat("ddMMyyyy");//dd/MM/yyyy
        Date now = new Date();
        String strDate = sdfDate.format(now);
        String headerValue = String.format("attachment; filename=\"csv-%s.csv\"",
                strDate);
        response.setHeader(headerKey, headerValue);

        return new FileSystemResource(p.toFile());

    }


    @RequestMapping(value = "/approvePiwikSite/{repositoryId}" , method = RequestMethod.GET)
    @ResponseBody
    @PreAuthorize("hasAuthority('SUPER_ADMINISTRATOR') or hasAuthority('CONTENT_PROVIDER_DASHBOARD_ADMINISTRATOR')")
    public ResponseEntity<Object> approvePiwikSite(@PathVariable("repositoryId") String repositoryId) {
        return piWikService.approvePiwikSite(repositoryId);
    }

    @RequestMapping(value = "/getOpenaireId/{repositoryId}" , method = RequestMethod.GET,produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    @PreAuthorize("hasAnyAuthority('SUPER_ADMINISTRATOR', 'CONTENT_PROVIDER_DASHBOARD_ADMINISTRATOR') or @authorizationService.isMemberOf(#repositoryId) or (@repositoryService.getRepositoryById(#repositoryId).registeredby==null and hasAuthority('REGISTERED_USER'))")
    public String getOpenaireId(@PathVariable("repositoryId") String repositoryId){
        return piWikService.getOpenaireId(repositoryId);
    }

    @RequestMapping(value = "/markPiwikSiteAsValidated/{repositoryId}" , method = RequestMethod.POST,
            consumes = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    @PreAuthorize("hasAuthority('SUPER_ADMINISTRATOR') or hasAuthority('CONTENT_PROVIDER_DASHBOARD_ADMINISTRATOR')")
    public ResponseEntity<Object> markPiwikSiteAsValidated(@PathVariable("repositoryId") String repositoryId) throws RepositoryServiceException {
        return piWikService.markPiwikSiteAsValidated(repositoryId);
    }

    @RequestMapping(value = "/enableMetricsForRepository", method = RequestMethod.POST,
            consumes = MediaType.APPLICATION_JSON_VALUE)
    @PreAuthorize("hasAuthority('SUPER_ADMINISTRATOR') or hasAuthority('CONTENT_PROVIDER_DASHBOARD_ADMINISTRATOR') or (hasAuthority('REGISTERED_USER') and #piwikInfo.requestorEmail == authentication.userInfo.email)")
    public PiwikInfo enableMetricsForRepository(@RequestParam("officialName") String officialName,
                                                @RequestParam("repoWebsite") String repoWebsite,
                                                @RequestBody PiwikInfo piwikInfo) throws RepositoryServiceException {
        return piWikService.enableMetricsForRepository(officialName, repoWebsite, piwikInfo);
    }


}
