package eu.dnetlib.repo.manager.controllers;

import eu.dnetlib.repo.manager.service.PiWikService;
import eu.dnetlib.repo.manager.service.RepositoryService;
import io.micrometer.core.instrument.binder.jvm.DiskSpaceMetrics;
import io.micrometer.core.instrument.binder.jvm.JvmGcMetrics;
import io.micrometer.core.instrument.binder.jvm.JvmMemoryMetrics;
import io.micrometer.core.instrument.binder.jvm.JvmThreadMetrics;
import io.micrometer.core.instrument.binder.system.ProcessorMetrics;
import io.micrometer.core.instrument.binder.system.UptimeMetrics;
import io.micrometer.prometheus.PrometheusConfig;
import io.micrometer.prometheus.PrometheusMeterRegistry;
import io.prometheus.client.exporter.common.TextFormat;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.io.File;


@RestController
@RequestMapping(value = "/actuator/prometheus", produces = "application/openmetrics-text; version=1.0.0; charset=utf-8")
public class PrometheusController {
    private static final Logger logger = LoggerFactory.getLogger(PrometheusController.class);

    private final PiWikService piWikService;
    private final RepositoryService repositoryService;

    @Autowired
    public PrometheusController(PiWikService piWikService, RepositoryService repositoryService) {
        this.piWikService = piWikService;
        this.repositoryService = repositoryService;
    }

    @RequestMapping(method = RequestMethod.GET)
    public String getPiwikMetrics() {
        PrometheusMeterRegistry registry = new PrometheusMeterRegistry(PrometheusConfig.DEFAULT);
        registry.gauge("provide_repositories_registered", repositoryService.getTotalRegisteredRepositories());
        registry.gauge("provide_usagecounts_repositories_registered", piWikService.getTotal());
        registry.gauge("provide_usagecounts_repositories_validated", piWikService.getValidated(true));

        return registry.scrape(TextFormat.CONTENT_TYPE_OPENMETRICS_100);
    }

    @RequestMapping(method = RequestMethod.GET, path = "metrics", produces = MediaType.TEXT_PLAIN_VALUE)
    public String getMetrics() {
        PrometheusMeterRegistry registry = new PrometheusMeterRegistry(PrometheusConfig.DEFAULT);
        new JvmThreadMetrics().bindTo(registry);
        try (JvmGcMetrics jvmGcMetrics = new JvmGcMetrics() ) {
            jvmGcMetrics.bindTo(registry);
        } catch (Exception e) {
            logger.error("", e);
        }
        new JvmMemoryMetrics().bindTo(registry);
        new DiskSpaceMetrics(new File("/")).bindTo(registry);
        new ProcessorMetrics().bindTo(registry); // metrics related to the CPU stats
        new UptimeMetrics().bindTo(registry);

        return registry.scrape(TextFormat.CONTENT_TYPE_OPENMETRICS_100);
    }

}
