package eu.dnetlib.repo.manager.controllers;

import eu.dnetlib.repo.manager.domain.InterfaceComplianceRequest;
import eu.dnetlib.repo.manager.domain.InterfaceComplianceRequestId;
import eu.dnetlib.repo.manager.domain.dto.InterfaceComplianceRequestDTO;
import eu.dnetlib.repo.manager.service.InterfaceComplianceService;
import org.springframework.web.bind.annotation.*;

import java.util.Date;

@RestController
@RequestMapping("compliance")
public class InterfaceComplianceRequestController {

    private final InterfaceComplianceService service;

    public InterfaceComplianceRequestController(InterfaceComplianceService service) {
        this.service = service;
    }

    @GetMapping("{repositoryId}/{interfaceId}")
    public InterfaceComplianceRequest get(@PathVariable("repositoryId") String repositoryId, @PathVariable("interfaceId") String interfaceId) {
        InterfaceComplianceRequestId id = new InterfaceComplianceRequestId();
        id.setRepositoryId(repositoryId);
        id.setInterfaceId(interfaceId);
        return service.getById(id).orElse(null);
    }

    @GetMapping()
    public Iterable<InterfaceComplianceRequest> get() {
        return service.get();
    }

    @PostMapping()
    public InterfaceComplianceRequest add(@RequestBody InterfaceComplianceRequestDTO requestDTO) {
        requestDTO.setSubmissionDate(new Date());
        return service.create(InterfaceComplianceRequest.from(requestDTO));
    }

    @DeleteMapping("{repositoryId}/{interfaceId}")
    public void delete(@PathVariable("repositoryId") String repositoryId, @PathVariable("interfaceId") String interfaceId) {
        InterfaceComplianceRequestId id = new InterfaceComplianceRequestId();
        id.setRepositoryId(repositoryId);
        id.setInterfaceId(interfaceId);
        this.service.delete(id);
    }
}
