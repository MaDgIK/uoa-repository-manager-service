//package eu.dnetlib.repo.manager.controllers;
//
//import eu.dnetlib.repo.manager.domain.dto.Role;
//import eu.dnetlib.repo.manager.service.aai.registry.AaiRegistryService;
//import eu.dnetlib.repo.manager.service.security.AuthoritiesUpdater;
//import eu.dnetlib.repo.manager.service.security.AuthorizationService;
//import eu.dnetlib.repo.manager.service.security.RoleMappingService;
//import eu.dnetlib.repo.manager.utils.JsonUtils;
//import io.swagger.annotations.ApiOperation;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.http.HttpStatus;
//import org.springframework.http.ResponseEntity;
//import org.springframework.security.access.prepost.PreAuthorize;
//import org.springframework.web.bind.annotation.*;
//
//import javax.ws.rs.core.MediaType;
//import javax.ws.rs.core.Response;
//import java.util.Collection;
//
////@RestController
////@RequestMapping(value = "/role-management")
////@Api(description = "Role Management", value = "role-management")
//public class UserRoleController {
//
//    private final AaiRegistryService aaiRegistryService;
//    private final AuthoritiesUpdater authoritiesUpdater;
//    private final RoleMappingService roleMappingService;
//    private final AuthorizationService authorizationService;
//
//    @Autowired
//    UserRoleController(AaiRegistryService aaiRegistryService,
//                       AuthoritiesUpdater authoritiesUpdater,
//                       RoleMappingService roleMappingService,
//                       AuthorizationService authorizationService) {
//        this.aaiRegistryService = aaiRegistryService;
//        this.authoritiesUpdater = authoritiesUpdater;
//        this.roleMappingService = roleMappingService;
//        this.authorizationService = authorizationService;
//    }
//
//    /**
//     * Get the role with the given id.
//     **/
//    @RequestMapping(method = RequestMethod.GET, path = "/role/{id}")
////    @PreAuthorize("hasAnyAuthority('REGISTERED_USER', 'SUPER_ADMINISTRATOR', 'CONTENT_PROVIDER_DASHBOARD_ADMINISTRATOR')")
//    public Response getRole(@RequestParam(value = "type", defaultValue = "datasource") String type, @PathVariable("id") String id) {
//        int roleId = aaiRegistryService.getCouId(type, id);
//        return Response.status(HttpStatus.OK.value()).entity(JsonUtils.createResponse("Role id is: " + roleId).toString()).type(MediaType.APPLICATION_JSON).build();
//    }
//
//    /**
//     * Create a new role with the given name and description.
//     **/
//    @RequestMapping(method = RequestMethod.POST, path = "/role")
//    @PreAuthorize("hasAnyAuthority('SUPER_ADMINISTRATOR')")
//    public Response createRole(@RequestBody Role role) {
//        aaiRegistryService.createRole(role);
//        return Response.status(HttpStatus.OK.value()).entity(JsonUtils.createResponse("Role has been created").toString()).type(MediaType.APPLICATION_JSON).build();
//    }
//
//    /**
//     * Subscribe to a type(Community, etc.) with id(ee, egi, etc.)
//     */
//    @ApiOperation(value = "subscribe")
//    @RequestMapping(method = RequestMethod.POST, path = "/subscribe/{type}/{id}")
//    @PreAuthorize("hasAnyAuthority('SUPER_ADMINISTRATOR', 'CONTENT_PROVIDER_DASHBOARD_ADMINISTRATOR')")
//    public Response subscribe(@PathVariable("type") String type, @PathVariable("id") String id) {
//        Integer coPersonId = aaiRegistryService.getCoPersonIdByIdentifier();
//        if (coPersonId == null) {
//            coPersonId = aaiRegistryService.getCoPersonIdsByEmail();
//        }
//        Integer couId = aaiRegistryService.getCouId(type, id);
//        if (couId != null) {
//            aaiRegistryService.assignMemberRole(coPersonId, couId);
//
//            // Add role to current authorities
//            authoritiesUpdater.addRole(roleMappingService.convertRepoIdToAuthority(id));
//
//            return Response.status(HttpStatus.OK.value()).entity(JsonUtils.createResponse("Role has been assigned").toString()).type(MediaType.APPLICATION_JSON).build();
//        } else {
//            return Response.status(HttpStatus.NOT_FOUND.value()).entity(JsonUtils.createResponse("Role has not been found").toString()).type(MediaType.APPLICATION_JSON).build();
//        }
//    }
//    /////////////////////////////////////////////////////////////////////////////////////////////
//    /////////////////////////////////////////////////////////////////////////////////////////////
//
//    @RequestMapping(method = RequestMethod.GET, path = "/users/couid/{id}")
//    @PreAuthorize("hasAnyAuthority('SUPER_ADMINISTRATOR', 'CONTENT_PROVIDER_DASHBOARD_ADMINISTRATOR')")
//    public ResponseEntity<String> getUsersByCouId(@PathVariable("id") Integer id) {
////        calls.getUserByCoId()
//        return ResponseEntity.ok(aaiRegistryService.getUsersByCouId(id).toString());
//    }
//
//
//    @RequestMapping(method = RequestMethod.GET, path = "/users/{email}/roles")
//    @PreAuthorize("hasAnyAuthority('SUPER_ADMINISTRATOR', 'CONTENT_PROVIDER_DASHBOARD_ADMINISTRATOR') or hasAuthority('REGISTERED_USER') and authentication.userInfo.email==#email")
//    public ResponseEntity<Collection<String>> getRolesByEmail(@PathVariable("email") String email) {
//        return ResponseEntity.ok(authorizationService.getUserRolesByEmail(email));
//    }
//
//
//    @RequestMapping(method = RequestMethod.GET, path = "/user/roles/my")
//    @PreAuthorize("hasAuthority('REGISTERED_USER')")
//    public ResponseEntity<Collection<String>> getRoleNames() {
//        return ResponseEntity.ok(authorizationService.getUserRoles());
//    }
//
//}
