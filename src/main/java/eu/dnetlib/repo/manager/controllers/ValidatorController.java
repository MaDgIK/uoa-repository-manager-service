package eu.dnetlib.repo.manager.controllers;

import eu.dnetlib.api.functionality.ValidatorServiceException;
import eu.dnetlib.domain.functionality.validator.JobForValidation;
import eu.dnetlib.domain.functionality.validator.RuleSet;
import eu.dnetlib.domain.functionality.validator.StoredJob;
import eu.dnetlib.repo.manager.domain.InterfaceInformation;
import eu.dnetlib.repo.manager.exception.ResourceNotFoundException;
import eu.dnetlib.repo.manager.exception.ValidationServiceException;
import eu.dnetlib.repo.manager.service.EmailUtils;
import eu.dnetlib.repo.manager.service.ValidatorServiceImpl;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.json.JSONException;
import org.mitre.openid.connect.model.OIDCAuthenticationToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;

import java.util.List;


@RestController
@RequestMapping(value = "/validator")
@Tag(name="validator", description = "Validator API")
public class ValidatorController {

    @Autowired
    private ValidatorServiceImpl validatorService;

    @Autowired
    private EmailUtils emailUtils;

    @RequestMapping(value = "/submitJobForValidation",method = RequestMethod.POST,
            consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    @PreAuthorize("hasAuthority('REGISTERED_USER') and #jobForValidation.userEmail == authentication.userInfo.email")
    public JobForValidation submitJobForValidation(@RequestBody JobForValidation jobForValidation) throws ValidatorServiceException {
        return validatorService.submitJobForValidation(jobForValidation);
    }

    @RequestMapping(value = "/reSubmitJobForValidation/{jobId}",method = RequestMethod.POST,
            consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    @PreAuthorize("hasAuthority('REGISTERED_USER')")
    public ResponseEntity<Object> reSubmitJobForValidation(@PathVariable("jobId") String jobId) throws JSONException, ValidatorServiceException {
        return validatorService.reSubmitJobForValidation(((OIDCAuthenticationToken) SecurityContextHolder.getContext().getAuthentication()).getUserInfo().getEmail(), jobId);
    }

    @RequestMapping(value = "/getRuleSets/{mode}" , method = RequestMethod.GET,produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public List<RuleSet> getRuleSets(@PathVariable("mode") String mode) {
        return validatorService.getRuleSets(mode);
    }

    @RequestMapping(value = "/getSetsOfRepository" , method = RequestMethod.GET,produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public List<String> getSetsOfRepository(@RequestParam(value = "url", required = true) String url) {
        return validatorService.getSetsOfRepository(url);
    }

    @RequestMapping(value = "/identifyRepository" , method = RequestMethod.GET,produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public boolean identifyRepo(@RequestParam(value = "url", required = true) String url) {
        return validatorService.identifyRepo(url);
    }

    @RequestMapping(value = "/getRuleSet/{acronym}" , method = RequestMethod.GET,produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public RuleSet getRuleSet(@PathVariable("acronym") String acronym) {
        return validatorService.getRuleSet(acronym);
    }

    @RequestMapping(value = "/getStoredJobsNew" , method = RequestMethod.GET,produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    @PreAuthorize("hasAuthority('REGISTERED_USER')")
    public List<StoredJob> getStoredJobsNew(@RequestParam(value = "jobType", required = false)
                                            @Parameter(description = "Equals to filter job type on validation history page") String jobType,
                                            @RequestParam("offset") @Parameter(description = "Page number", required = true) String offset,
                                            @RequestParam(value = "limit", required = false,defaultValue = "10") @Parameter(description = "Null value") String limit,
                                            @RequestParam(value = "dateFrom", required = false) @Parameter(description = "Null value") String dateFrom,
                                            @RequestParam(value = "dateTo", required = false) @Parameter(description = "Null value") String dateTo,
                                            @RequestParam("validationStatus") @Parameter(description = "Equals to filter validation jobs", required = true) String validationStatus
                                            ) throws ValidatorServiceException {
        return validatorService.getStoredJobsNew(((OIDCAuthenticationToken) SecurityContextHolder.getContext().getAuthentication()).getUserInfo().getEmail(), jobType, offset, limit, dateFrom, dateTo, validationStatus);
    }

    @RequestMapping(value = "/getStoredJobsTotalNumberNew" , method = RequestMethod.GET,produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public int getStoredJobsTotalNumberNew(String user, String jobType, String validationStatus) throws  ValidatorServiceException {
        return validatorService.getStoredJobsTotalNumberNew(user, jobType, validationStatus);
    }

    @RequestMapping(value = "/getInterfaceInformation" , method = RequestMethod.GET,produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public InterfaceInformation getInterfaceInformation(@RequestParam(value = "baseUrl") String baseUrl) throws ValidationServiceException {
        return validatorService.getInterfaceInformation(baseUrl);
    }

    @RequestMapping(value = "/validationSummary/{repoId}" , method = RequestMethod.GET,produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public List<StoredJob> getInterfaceInformation(@PathVariable(value = "repoId") String repoId, @RequestParam(name = "size", defaultValue = "20") int size ) throws ValidatorServiceException, ResourceNotFoundException, JSONException {
        return validatorService.getJobsSummary(repoId,size);
    }


    @RequestMapping(value = "/complete" , method = RequestMethod.POST,  produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public void validationCompleted(
        @RequestParam(value = "repoId") String repoId,
        @RequestParam(value = "interfaceId") String interfaceId,
        @RequestParam(value = "jobId") String jobId,
        @RequestParam(value = "issuerEmail") String issuerEmail,
        @RequestParam(value = "isUpdate") boolean isUpdate,
        @RequestParam(value = "isSuccess") boolean isSuccess,
        @RequestParam(value = "scoreUsage") int scoreUsage,
        @RequestParam(value = "scoreContent") int scoreContent) throws Exception {

        validatorService.onComplete(repoId, interfaceId, jobId, issuerEmail, isUpdate, isSuccess, scoreUsage, scoreContent);
    }


}
