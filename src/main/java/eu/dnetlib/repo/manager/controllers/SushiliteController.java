package eu.dnetlib.repo.manager.controllers;

import eu.dnetlib.repo.manager.service.sushilite.SushiliteServiceImpl;
import eu.dnetlib.usagestats.sushilite.domain.ReportResponseWrapper;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(value = "/sushilite")
@Tag(name="sushilite", description = "Sushi-Lite API")
public class SushiliteController {

    @Autowired
    private SushiliteServiceImpl sushiliteService;

    @RequestMapping(value = "/getReportResults/{page}/{pageSize}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    @PreAuthorize("hasAuthority('REGISTERED_USER')")
    public ReportResponseWrapper getReportResults(@PathVariable("page") String page,
                                                  @PathVariable("pageSize") String pageSize,
                                                  @RequestParam(value = "Report") String Report,
                                                  @RequestParam(value = "Release",defaultValue="4") String Release,
                                                  @RequestParam(value = "RequestorID",required=false,defaultValue="anonymous") String RequestorID,
                                                  @RequestParam(value = "BeginDate",required=false,defaultValue="") String BeginDate,
                                                  @RequestParam(value = "EndDate",required=false,defaultValue="") String EndDate,
                                                  @RequestParam(value = "RepositoryIdentifier") String RepositoryIdentifier,
                                                  @RequestParam(value = "ItemIdentifier",required=false,defaultValue="") String ItemIdentifier,
                                                  @RequestParam(value = "ItemDataType",required=false,defaultValue="") String ItemDataType,
                                                  @RequestParam(value = "Granularity") String Granularity,
                                                  @RequestParam(value = "Pretty",required=false,defaultValue="") String Pretty) {

           return sushiliteService.getReportResults(page, pageSize, Report, Release, RequestorID, BeginDate, EndDate, RepositoryIdentifier, ItemIdentifier, ItemDataType, Granularity, Pretty);
    }

}
