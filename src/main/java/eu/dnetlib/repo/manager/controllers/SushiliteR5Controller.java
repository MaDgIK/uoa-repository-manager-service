package eu.dnetlib.repo.manager.controllers;

import eu.dnetlib.repo.manager.service.sushilite.SushiliteR5ServiceImpl;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.json.JSONException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;


@RestController
@RequestMapping(value = "/sushiliteR5")
@Tag(name="sushiliteR5", description = "Sushi-Lite R5 API")
public class SushiliteR5Controller {


    private static final Logger logger = LoggerFactory.getLogger(SushiliteR5Controller.class);


    @Autowired
    private SushiliteR5ServiceImpl sushiliteR5Service;

    @RequestMapping(value = "/getReportResults", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    @PreAuthorize("hasAuthority('REGISTERED_USER')")
    public ResponseEntity<?> getReportResults(@RequestParam(value = "Report") String report,
                                           @RequestParam(value = "Release", required=false, defaultValue="5") String release,
                                           @RequestParam(value = "RequestorID",required=false, defaultValue="anonymous") String requestorID,
                                           @RequestParam(value = "BeginDate",required=false, defaultValue="") String beginDate,
                                           @RequestParam(value = "EndDate",required=false, defaultValue="") String endDate,
                                           @RequestParam(value = "RepositoryIdentifier", required=false, defaultValue="") String repositoryIdentifier,
                                           @RequestParam(value = "DatasetIdentifier", required=false, defaultValue="") String datasetIdentifier,
                                           @RequestParam(value = "ItemIdentifier",required=false, defaultValue="") String itemIdentifier,
                                           @RequestParam(value = "MetricType",required=false) List<String> metricTypes,
                                           @RequestParam(value = "DataType",required=false, defaultValue="") String dataType,
                                           @RequestParam(value = "Granularity", required = false, defaultValue ="Monthly") String granularity,
                                           @RequestParam(value = "Pretty",required=false, defaultValue="") String pretty) {
        try {
            return sushiliteR5Service.getReportResults(report, release, requestorID, beginDate, endDate, repositoryIdentifier, datasetIdentifier, itemIdentifier, metricTypes, dataType, granularity, pretty);
        } catch (JSONException je) {
            logger.error("", je);
            return ResponseEntity.internalServerError().build();
        }
    }

}
