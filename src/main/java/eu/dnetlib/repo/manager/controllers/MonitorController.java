package eu.dnetlib.repo.manager.controllers;

import eu.dnetlib.api.functionality.ValidatorServiceException;
import eu.dnetlib.domain.functionality.validator.StoredJob;
import eu.dnetlib.repo.manager.domain.JobsOfUser;
import eu.dnetlib.repo.manager.service.MonitorServiceImpl;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.json.JSONException;
import org.mitre.openid.connect.model.OIDCAuthenticationToken;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(value = "/monitor")
@Tag(name="monitor", description="Monitor API")
public class MonitorController {

    private static final Logger logger = LoggerFactory.getLogger(MonitorController.class);

    @Autowired
    private MonitorServiceImpl monitorService;

    @RequestMapping(value = "/getJobsOfUser" , method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    @PreAuthorize("hasAuthority('REGISTERED_USER')")
    public JobsOfUser getJobsOfUser(@RequestParam(value = "jobType", required = false)
                                    @Parameter(description = "Equals to filter job type on validation history page") String jobType,
                                    @RequestParam("offset") @Parameter(name = "Page number", required = true) String offset,
                                    @RequestParam(value = "limit", required = false,defaultValue = "10") @Parameter(description = "Null value") String limit,
                                    @RequestParam(value = "dateFrom", required = false) @Parameter(description = "Null value") String dateFrom,
                                    @RequestParam(value = "dateTo", required = false) @Parameter(description = "Null value") String dateTo,
                                    @RequestParam("validationStatus") @Parameter(description = "Equals to filter validation jobs", required = false) String validationStatus,
                                    @RequestParam("includeJobsTotal") @Parameter(description = "Always true", required = true) String includeJobsTotal) throws JSONException, ValidatorServiceException {
        return monitorService.getJobsOfUser(((OIDCAuthenticationToken) SecurityContextHolder.getContext().getAuthentication()).getUserInfo().getEmail(), jobType, offset, limit, dateFrom, dateTo, validationStatus, includeJobsTotal);
    }

    @RequestMapping(value = "/getJobsOfUserPerValidationStatus" , method = RequestMethod.GET,produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    @PreAuthorize("hasAuthority('REGISTERED_USER')")
    public int getJobsOfUserPerValidationStatus(@RequestBody String jobType,
                                                @RequestBody String validationStatus) throws JSONException {
        return monitorService.getJobsOfUserPerValidationStatus(((OIDCAuthenticationToken) SecurityContextHolder.getContext().getAuthentication()).getUserInfo().getEmail(), jobType, validationStatus);
    }

    @RequestMapping(value = "/getJobSummary" , method = RequestMethod.GET,produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public StoredJob getJobSummary(@RequestParam String jobId,
                                   @RequestParam String groupBy) throws JSONException {
        return monitorService.getJobSummary(jobId, groupBy);
    }

}
