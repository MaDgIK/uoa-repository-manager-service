package eu.dnetlib.repo.manager.controllers;

import eu.dnetlib.openaire.exporter.model.dsm.AggregationInfo;
import eu.dnetlib.repo.manager.domain.BrokerSummary;
import eu.dnetlib.repo.manager.domain.CollectionMonitorSummary;
import eu.dnetlib.repo.manager.domain.RepositorySummaryInfo;
import eu.dnetlib.repo.manager.domain.UsageSummary;
import eu.dnetlib.repo.manager.exception.BrokerException;
import eu.dnetlib.repo.manager.exception.RepositoryServiceException;
import eu.dnetlib.repo.manager.service.*;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.json.JSONException;
import org.mitre.openid.connect.model.OIDCAuthenticationToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(value = "/dashboard")
@Tag(name="dashboard", description = "Dashboard API")
public class DashboardController {

    @Autowired
    private DashboardService dashboardService;

    @Autowired
    private RepositoryService repositoryService;

    @Autowired
    private AggregationService aggregationService;

    @Autowired
    private BrokerService brokerService;

    @Autowired
    private PiWikService piWikService;

    @RequestMapping(value = "/getRepositoriesSummary/{page}/{size}", method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    @PreAuthorize("hasAuthority('REGISTERED_USER')")
    public List<RepositorySummaryInfo> getRepositoriesSummaryInfo(
            @PathVariable("page") String page,
            @PathVariable("size") String size) throws JSONException {
        return dashboardService.getRepositoriesSummaryInfo(((OIDCAuthenticationToken) SecurityContextHolder.getContext().getAuthentication()).getUserInfo().getEmail(), page, size);
    }

    @RequestMapping(value = "/collectionMonitorSummary/{repoId}", method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    @PreAuthorize("hasAuthority('REGISTERED_USER')")
    public CollectionMonitorSummary getCollectionMonitorSummary(
            @PathVariable("repoId") String repoId,
            @RequestParam(name = "size", required = false, defaultValue = "20") int summarySize) throws JSONException {

        List<AggregationInfo> aggregationInfoList = aggregationService.getRepositoryAggregations(repoId);

        CollectionMonitorSummary collectionMonitorSummary = new CollectionMonitorSummary();
        // Set the "aggregationInfo" for the first <number of> requested aggregations, in order to create a "summary".
        collectionMonitorSummary.setAggregationInfo(aggregationInfoList.subList(0, Math.min(summarySize, aggregationInfoList.size())));

        // Search for the last indexed version and set the "collectionMonitorSummary".
        for ( AggregationInfo aggregationInfo : aggregationInfoList ) {
            if ( aggregationInfo.isIndexedVersion() ) {
                collectionMonitorSummary.setLastIndexedVersion(aggregationInfo);
                break;
            }
        }

        return collectionMonitorSummary;
    }

    @RequestMapping(value = "/usageSummary/{repoId}", method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    @PreAuthorize("hasAuthority('REGISTERED_USER')")
    public UsageSummary getUsageSummary(
            @PathVariable("repoId") String repoId
    ) throws RepositoryServiceException {
        return new UsageSummary(repositoryService.getMetricsInfoForRepository(repoId), piWikService.getPiwikSiteForRepo(repoId));
    }

    @RequestMapping(value = "/brokerSummary/{ds_name}", method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    @PreAuthorize("hasAuthority('REGISTERED_USER')")
    public BrokerSummary getBrokerSummary(
            @PathVariable("ds_name") String datasourceName) throws BrokerException {
        return new BrokerSummary(brokerService.getSimpleSubscriptionsOfUser(((OIDCAuthenticationToken) SecurityContextHolder.getContext().getAuthentication()).getUserInfo().getEmail()), brokerService.getTopicsForDatasource(datasourceName));
    }

}
