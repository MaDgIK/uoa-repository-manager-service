package eu.dnetlib.repo.manager.controllers;

import eu.dnetlib.repo.manager.service.UserServiceImpl;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "/user")
@Tag(name="user", description = "User API")
public class UserController {

    @Autowired
    private UserServiceImpl userService;

    @RequestMapping(value = "/login" , method = RequestMethod.GET)
    @PreAuthorize("hasAuthority('REGISTERED_USER')")
    public ResponseEntity<Object> login() {
        return userService.login();
    }
}
