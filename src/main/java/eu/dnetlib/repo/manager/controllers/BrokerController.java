package eu.dnetlib.repo.manager.controllers;

import eu.dnetlib.repo.manager.domain.Term;
import eu.dnetlib.repo.manager.domain.broker.*;
import eu.dnetlib.repo.manager.exception.BrokerException;
import eu.dnetlib.repo.manager.service.BrokerServiceImpl;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.json.JSONException;
import org.mitre.openid.connect.model.OIDCAuthenticationToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping(value = "/broker")
@Tag(name="broker", description="Broker API")
public class BrokerController{

    @Autowired
    private BrokerServiceImpl brokerService;

    @RequestMapping(value = "/getDatasourcesOfUser" , method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    @PreAuthorize("hasAuthority('REGISTERED_USER')")
    public DatasourcesBroker getDatasourcesOfUser(
                                           @RequestParam("includeShared")
                                           @Parameter(description = "Include shared datasources (default = false)", required = true) String includeShared,
                                           @RequestParam("includeByOthers")
                                           @Parameter(description = "Include datasources of other (default = false)", required = true) String includeByOthers) throws JSONException {
        return brokerService.getDatasourcesOfUser(((OIDCAuthenticationToken) SecurityContextHolder.getContext().getAuthentication()).getUserInfo().getEmail(), includeShared, includeByOthers);
    }

    @RequestMapping(value = "/getTopicsForDatasource/{datasourceName:.+}" ,
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public List<BrowseEntry> getTopicsForDatasource(@PathVariable("datasourceName")  String datasourceName) throws BrokerException{
        return brokerService.getTopicsForDatasource(datasourceName);
    }

    @RequestMapping(value = "/advancedShowEvents/{page}/{size}" ,
            method = RequestMethod.POST,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    @PreAuthorize("hasAuthority('REGISTERED_USER')")
    public EventsPage advancedShowEvents(@PathVariable("page") String page,
                                  @PathVariable("size") String size,
                                  @RequestBody AdvQueryObject advQueryObject) throws BrokerException, JSONException ,IOException{
        return brokerService.advancedShowEvents(page, size, advQueryObject);
    }

    @RequestMapping(value = "/showEvents/{datasourceName:.+}/{topic}/{page}" ,
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    @PreAuthorize("hasAuthority('REGISTERED_USER')")
    public EventsPage showEvents(@RequestParam("datasourceName") String datasourceName,
                          @RequestParam("topic") String topic,
                          @RequestParam("page") String page,
                          @RequestParam("size") String size) throws BrokerException, JSONException{
        return brokerService.showEvents(datasourceName, topic, page, size);
    }

    @RequestMapping(value = "/getSimpleSubscriptionsOfUser" ,
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    @PreAuthorize("hasAuthority('REGISTERED_USER')")
    public Map<String, List<SimpleSubscriptionDesc>> getSimpleSubscriptionsOfUser() throws BrokerException{
        return brokerService.getSimpleSubscriptionsOfUser(((OIDCAuthenticationToken) SecurityContextHolder.getContext().getAuthentication()).getUserInfo().getEmail());
    }

    @RequestMapping(value = "/subscribe" , method = RequestMethod.POST,
            consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    @PreAuthorize("hasAuthority('REGISTERED_USER') ")
    public Subscription subscribe(@RequestBody OpenaireSubscription obj) throws BrokerException{
        return brokerService.subscribe(obj);
    }

    @RequestMapping(value = "/unsubscribe/{subscriptionId}" , method = RequestMethod.POST,
            consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    @PreAuthorize("hasAuthority('REGISTERED_USER')")
    public ResponseEntity<Object> unsubscribe(@PathVariable("subscriptionId") String subscriptionId) throws BrokerException{
        return brokerService.unsubscribe(subscriptionId);
    }

    @RequestMapping(value = "/getSubscription/{subscriptionId}" , method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    @PreAuthorize("hasAuthority('REGISTERED_USER')")
    public Subscription getSubscription(@PathVariable("subscriptionId") String subscriptionId) throws BrokerException{
        return brokerService.getSubscription(subscriptionId);
    }


    @RequestMapping(value = "/getDnetTopics" , method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public Map<String, Term> getDnetTopics() throws BrokerException{
        return brokerService.getDnetTopics();
    }

    @RequestMapping(value = "/getNotificationsBySubscriptionId/{subscriptionId}/{page}/{size}" , method = RequestMethod.GET
            ,produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    @PreAuthorize("hasAuthority('REGISTERED_USER')")
    public EventsPage getNotificationsBySubscriptionId(@PathVariable("subscriptionId") String subscriptionId,
                                                @PathVariable("page") String page,
                                                @PathVariable("size") String size) throws BrokerException{
        return brokerService.getNotificationsBySubscriptionId(subscriptionId, page, size);
    }

}
