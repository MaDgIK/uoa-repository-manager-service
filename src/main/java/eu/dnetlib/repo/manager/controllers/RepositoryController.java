package eu.dnetlib.repo.manager.controllers;

import eu.dnetlib.domain.data.PiwikInfo;
import eu.dnetlib.openaire.exporter.model.dsm.AggregationInfo;
import eu.dnetlib.repo.manager.domain.*;
import eu.dnetlib.repo.manager.domain.dto.RepositoryTerms;
import eu.dnetlib.repo.manager.domain.dto.User;
import eu.dnetlib.repo.manager.exception.RepositoryServiceException;
import eu.dnetlib.repo.manager.exception.ResourceNotFoundException;
import eu.dnetlib.repo.manager.service.AggregationService;
import eu.dnetlib.repo.manager.service.PiWikService;
import eu.dnetlib.repo.manager.service.RepositoryService;
import eu.dnetlib.repo.manager.service.security.AuthorizationService;
import eu.dnetlib.repo.manager.utils.JsonUtils;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.json.JSONException;
import org.mitre.openid.connect.model.OIDCAuthenticationToken;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;

import javax.ws.rs.core.Response;
import java.util.Date;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping(value = "/repositories")
@Tag(name="repositories", description="Repository API")
public class RepositoryController {

    private static final Logger logger = LoggerFactory.getLogger(RepositoryController.class);

    private final RepositoryService repositoryService;

    private final AggregationService aggregationService;

    private final AuthorizationService authorizationService;

    private final PiWikService piWikService;

    @Autowired
    RepositoryController(RepositoryService repositoryService,
                         AggregationService aggregationService,
                         AuthorizationService authorizationService,
                         PiWikService piWikService) {
        this.repositoryService = repositoryService;
        this.aggregationService = aggregationService;
        this.authorizationService = authorizationService;
        this.piWikService = piWikService;
    }

    @RequestMapping(value = "/countries", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public Country[] getCountries() {
        return repositoryService.getCountries();
    }

    @RequestMapping(value = "/getRepositoriesByCountry/{country}/{mode}", method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public List<RepositorySnippet> getRepositoriesByCountry(@PathVariable("country") String country,
                                                            @PathVariable("mode") String mode,
                                                            @RequestParam(value = "managed", required = false) Boolean managed) {
        return repositoryService.getRepositoriesByCountry(country, mode, managed);
    }

    @RequestMapping(value = "/snippets/user", method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    @PreAuthorize("hasAuthority('REGISTERED_USER')")
    public List<RepositorySnippet> getRepositoriesSnippetsOfUser(@RequestParam(name = "page", defaultValue = "0") int page,
                                                                 @RequestParam(name = "size", defaultValue = "1000") int pageSize) throws Exception {
        return repositoryService.getRepositoriesSnippetsOfUser(String.valueOf(page), String.valueOf(pageSize));
    }

    @RequestMapping(value = "/terms", method = RequestMethod.POST,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    @PreAuthorize("hasAuthority('REGISTERED_USER')")
    public void updateRepositoriesTerms(@RequestBody List<RepositoryTerms> repositoriesTerms) throws Exception {
        Date date = new Date();
        if (repositoriesTerms != null) {
            for (RepositoryTerms terms : repositoriesTerms) {
                Repository repository = repositoryService.getRepositoryById(terms.getId());
                repository.setConsentTermsOfUse(terms.getConsentTermsOfUse());
                repository.setFullTextDownload(terms.getFullTextDownload());
                repository.setLastConsentTermsOfUseDate(date);
                repositoryService.updateRepository(repository, SecurityContextHolder.getContext().getAuthentication());
            }
        }
    }

    @RequestMapping(value = "/searchRegisteredRepositories/{page}/{size}", method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    @PreAuthorize("hasAuthority('SUPER_ADMINISTRATOR') or hasAuthority('CONTENT_PROVIDER_DASHBOARD_ADMINISTRATOR')")
    public List<RepositorySnippet> searchRegisteredRepositories(@RequestParam(name = "country", required = false) String country,
                                                                @RequestParam(name = "typology", required = false) String typology,
                                                                @RequestParam(name = "englishName", required = false) String englishName,
                                                                @RequestParam(name = "officialName", required = false) String officialName,
                                                                @RequestParam("requestSortBy") String requestSortBy,
                                                                @RequestParam("order") String order,
                                                                @PathVariable("page") int page,
                                                                @PathVariable("size") int pageSize) {

        return repositoryService.searchRegisteredRepositories(country, typology, englishName, officialName, requestSortBy, order, page, pageSize);
    }

    @RequestMapping(value = "/getRepositoryById/{id}", method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    @PostAuthorize("hasAnyAuthority('SUPER_ADMINISTRATOR', 'CONTENT_PROVIDER_DASHBOARD_ADMINISTRATOR') or @authorizationService.isMemberOf(#id) or (returnObject.registeredby==null and hasAuthority('REGISTERED_USER'))")
    public Repository getRepositoryById(@PathVariable("id") String id) throws ResourceNotFoundException {
        Repository repo = repositoryService.getRepositoryById(id);

        if (repo != null)
            logger.info("Returning repository {} registered by {}", repo.getId(), repo.getRegisteredby());
        else
            logger.info("Requested repository {} not found", id);
        return repo;
    }

    @RequestMapping(value = "/getRepositoryAggregations/{id}", method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public List<AggregationInfo> getRepositoryAggregations(@PathVariable("id") String id) throws JSONException {
        return aggregationService.getRepositoryAggregations(id, 0, 20);
    }

    @RequestMapping(value = "/getRepositoryAggregationsByYear/{id}", method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public Map<String, List<AggregationInfo>> getRepositoryAggregationsByYear(@PathVariable("id") String id) throws JSONException {
        return aggregationService.getRepositoryAggregationsByYear(id);
    }

    @RequestMapping(value = "/getRepositoriesByName/{name:.+}/{page}/{size}/", method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public List<Repository> getRepositoriesByName(@PathVariable("name") String name,
                                                  @PathVariable("page") String page,
                                                  @PathVariable("size") String size) {
        return repositoryService.getRepositoriesByName(name, page, size);
    }

    @RequestMapping(value = "/getRepositoryInterface/{id}", method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    @PostAuthorize("hasAuthority('SUPER_ADMINISTRATOR') or hasAuthority('CONTENT_PROVIDER_DASHBOARD_ADMINISTRATOR') or @authorizationService.isMemberOf(#id) or (@repositoryService.getRepositoryById(#id).registeredby==null and hasAuthority('REGISTERED_USER'))")
    public List<RepositoryInterface> getRepositoryInterface(@PathVariable("id") String id) {
        return repositoryService.getRepositoryInterface(id);
    }

    @RequestMapping(value = "/addRepository", method = RequestMethod.POST,
            consumes = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
//    @PreAuthorize("hasAuthority('SUPER_ADMINISTRATOR') or hasAuthority('CONTENT_PROVIDER_DASHBOARD_ADMINISTRATOR') or (hasAuthority(@authorizationService.convertRepoIdToRoleId(#repository.id)) or hasAuthority(@authorizationService.convertRepoIdToRoleId(returnObject.id)))")
    @PreAuthorize("hasAuthority('SUPER_ADMINISTRATOR') or hasAuthority('CONTENT_PROVIDER_DASHBOARD_ADMINISTRATOR') or hasAuthority('REGISTERED_USER')")
    public Repository addRepository(@RequestParam("datatype") String datatype,
                                    @RequestBody Repository repository) {

        return repositoryService.addRepository(datatype, repository);
    }

    @Deprecated
    @RequestMapping(value = "/getDnetCountries", method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public List<String> getDnetCountries() {
        return repositoryService.getDnetCountries();
    }

    @Deprecated
    @RequestMapping(value = "/getTypologies", method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public List<String> getTypologies() {
        return repositoryService.getTypologies();
    }

    @Deprecated
    @RequestMapping(value = "/getTimezones", method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public List<Timezone> getTimezones() {
        return repositoryService.getTimezones();
    }

    @RequestMapping(value = "/updateRepository", method = RequestMethod.POST,
            consumes = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    @PreAuthorize("hasAuthority('SUPER_ADMINISTRATOR') or hasAuthority('CONTENT_PROVIDER_DASHBOARD_ADMINISTRATOR') or @authorizationService.isMemberOf(#repository.id)")
    public Repository updateRepository(@RequestBody Repository repository, Authentication authentication) {
        return repositoryService.updateRepository(repository, authentication);
    }

    @RequestMapping(value = "/deleteInterface/", method = RequestMethod.DELETE)
    @PreAuthorize("hasAuthority('SUPER_ADMINISTRATOR') or hasAuthority('CONTENT_PROVIDER_DASHBOARD_ADMINISTRATOR') or @authorizationService.isMemberOfInterface(#id)")
    public void deleteRepositoryInterface(@RequestParam("id") String id,
                                          @RequestParam("registeredBy") String registeredBy) {
//        repositoryService.deleteRepositoryInterface(id, registeredBy);
        logger.warn("User attempted delete on Interface with ID: {}", id);
    }

    @RequestMapping(value = "/addInterface", method = RequestMethod.POST,
            consumes = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    @PreAuthorize("hasAuthority('SUPER_ADMINISTRATOR') or hasAuthority('CONTENT_PROVIDER_DASHBOARD_ADMINISTRATOR') or @authorizationService.isMemberOf(#id)")
    public RepositoryInterface addRepositoryInterface(@RequestParam("datatype") String datatype,
                                                      @RequestParam("repoId") String id,
                                                      @RequestParam(required = false, name = "desiredCompatibilityLevel") String desiredCompatibilityLevel,
                                                      @RequestParam(value = "comment", required = false) String comment,
                                                      @RequestBody RepositoryInterface repositoryInterface) throws Exception {
        return repositoryService.addRepositoryInterface(datatype, id, comment, repositoryInterface, desiredCompatibilityLevel);
    }

    @RequestMapping(value = "/updateRepositoryInterface", method = RequestMethod.POST,
            consumes = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    @PreAuthorize("hasAuthority('SUPER_ADMINISTRATOR') or hasAuthority('CONTENT_PROVIDER_DASHBOARD_ADMINISTRATOR') or @authorizationService.isMemberOf(#id)")
    public RepositoryInterface updateRepositoryInterface(@RequestParam("repoId") String id,
                                                         @RequestParam(required = false, name = "desiredCompatibilityLevel") String desiredCompatibilityLevel,
                                                         @RequestParam(value = "comment", required = false) String comment,
                                                         @RequestBody RepositoryInterface repositoryInterface) throws Exception {
        return repositoryService.updateRepositoryInterface(id, comment, repositoryInterface, desiredCompatibilityLevel);
    }

    @RequestMapping(value = "/getUrlsOfUserRepos/{page}/{size}/", method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    @PreAuthorize("hasAuthority('REGISTERED_USER')")
    public List<String> getUrlsOfUserRepos(@PathVariable("page") String page, @PathVariable("size") String size) {
        return repositoryService.getUrlsOfUserRepos(((OIDCAuthenticationToken) SecurityContextHolder.getContext().getAuthentication()).getUserInfo().getEmail(), page, size);
    }

    @RequestMapping(value = "/getCompatibilityClasses/{mode}", method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public Map<String, String> getCompatibilityClasses(@PathVariable("mode") String mode) {

        return repositoryService.getCompatibilityClasses(mode);
    }

    @RequestMapping(value = "/getDatasourceClasses/{mode}", method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public Map<String, String> getDatasourceClasses(@PathVariable("mode") String mode) {
        return repositoryService.getDatasourceClasses(mode);
    }

    @RequestMapping(value = "/getMetricsInfoForRepository/{id}", method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public MetricsInfo getMetricsInfoForRepository(@PathVariable("id") String id) throws RepositoryServiceException {
        return repositoryService.getMetricsInfoForRepository(id);
    }

    @RequestMapping(value = "/getListLatestUpdate/{mode}", method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public Map<String, String> getListLatestUpdate(@PathVariable("mode") String mode) {
        return repositoryService.getListLatestUpdate(mode);
    }

    ///////////////////////////////////////////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////////////////////////////////////

    /**
     * Get all the admins of the repository
     */
    @RequestMapping(method = RequestMethod.GET, path = "{id}/admins")
    @PreAuthorize("hasAnyAuthority('SUPER_ADMINISTRATOR', 'CONTENT_PROVIDER_DASHBOARD_ADMINISTRATOR') or @authorizationService.isMemberOf(#id)")
    public ResponseEntity<List<User>> getAdminsOfARepo(@PathVariable("id") String id) {
        return new ResponseEntity<>(authorizationService.getAdminsOfRepo(id), HttpStatus.OK);
    }

    /**
     * Subscribe to repo by email
     */
    @RequestMapping(method = RequestMethod.POST, path = "{id}/admins")
    @PreAuthorize("hasAnyAuthority('SUPER_ADMINISTRATOR', 'CONTENT_PROVIDER_DASHBOARD_ADMINISTRATOR') or @authorizationService.isMemberOf(#id)")
    public Response subscribeByEmail(@PathVariable("id") String id, @RequestBody String email) throws ResourceNotFoundException {
        authorizationService.addAdmin(id, email);
        return Response.status(HttpStatus.OK.value()).entity(JsonUtils.createResponse("Role has been assigned").toString()).type(javax.ws.rs.core.MediaType.APPLICATION_JSON).build();
    }

    /**
     * Unsubscribe from repo by email
     */
    @RequestMapping(method = RequestMethod.DELETE, path = "{id}/admins/{email:.+}")
    @PreAuthorize("hasAnyAuthority('SUPER_ADMINISTRATOR', 'CONTENT_PROVIDER_DASHBOARD_ADMINISTRATOR') or @authorizationService.isMemberOf(#id)")
    public ResponseEntity<Void> unsubscribeByEmail(@PathVariable("id") String id, @PathVariable("email") String email) throws ResourceNotFoundException {
        authorizationService.removeAdmin(id, email);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

    ///////////////////////////////////////////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////////////////////////////////////

    /**
     * Returns whether the Piwik Site of a repository is enabled and validated.
     */
    @RequestMapping(value = "{repositoryId}/metrics/valid", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public boolean getMetricsEnabledAndValidated(@PathVariable("repositoryId") String repositoryId) {
        PiwikInfo info = piWikService.getPiwikSiteForRepo(repositoryId);
        return info != null && info.isValidated();
    }

    /**
     * Returns repository Metrics.
     */
    @RequestMapping(value = "{repositoryId}/metrics", method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    public MetricsInfo getMetricsInfo(@PathVariable("repositoryId") String id) throws RepositoryServiceException {
        return repositoryService.getMetricsInfoForRepository(id);
    }

}
