package eu.dnetlib.repo.manager.controllers;

import eu.dnetlib.repo.manager.service.StatsServiceImpl;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

@RestController
@RequestMapping(value = "/stats")
@Tag(name="statistics", description = "Stats API")
public class StatsController {

    @Autowired
    private StatsServiceImpl statsService;

    @RequestMapping(value = "/getStatistics" , method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public Map getStatistics(){
       return statsService.getStatistics();
    }
}
