package eu.dnetlib.repo.manager.integration.metrics;

import eu.dnetlib.repo.manager.controllers.PrometheusController;
import eu.dnetlib.repo.manager.service.PiWikService;
import eu.dnetlib.repo.manager.service.RepositoryService;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.when;

@Component
public class PrometheusTest {

    private static final int TOTAL = 10;
    private static final int VALIDATED = 8;

    @Autowired
    @Mock
    PiWikService piWikService;

    @Autowired
    @Mock
    RepositoryService repositoryService;

    @Autowired
    @InjectMocks
    private PrometheusController prometheusController;

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);

        when(piWikService.getTotal()).thenReturn(TOTAL);
        when(piWikService.getValidated(true)).thenReturn(VALIDATED);
        when(piWikService.getValidated(false)).thenReturn(TOTAL - VALIDATED);
        when(repositoryService.getTotalRegisteredRepositories()).thenReturn(TOTAL);
    }

    @Test
    public void testMetrics() {
        String report = prometheusController.getMetrics();
        assertTrue(report.startsWith("#"));
        System.out.println(report);
    }

    @Test
    public void testPiwikMetrics() {
        assertEquals((long) piWikService.getValidated(false), (TOTAL - VALIDATED));
        String report = prometheusController.getPiwikMetrics();
        assertTrue(report.contains("provide_repositories_registered_total " + TOTAL));
        assertTrue(report.contains("provide_usagecounts_repositories_registered_total " + TOTAL));
        assertTrue(report.contains("provide_usagecounts_repositories_validated_total " + VALIDATED));
        System.out.println(report);
    }
}
