package eu.dnetlib.repo.manager.integration.service;

import eu.dnetlib.repo.manager.domain.InterfaceComplianceRequest;
import eu.dnetlib.repo.manager.domain.InterfaceComplianceRequestId;
import eu.dnetlib.repo.manager.repository.InterfaceComplianceRequestsRepository;
import org.junit.Assert;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Date;

@RunWith(SpringRunner.class)
@DataJpaTest
//@AutoConfigureTestDatabase
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
//@Transactional(propagation = Propagation.NOT_SUPPORTED)
class InterfaceComplianceRequestTests {

    @Autowired
    private InterfaceComplianceRequestsRepository repository;

    @Test
    void getRequestById() {
        InterfaceComplianceRequest request = createRequest("3");
        repository.save(request);
        long total = repository.count();
        Assert.assertEquals(1, total);
        InterfaceComplianceRequest r = repository.findById(createRequestId()).orElse(null);
        Assert.assertNotNull(r);
    }

    @AfterEach
    public void deleteRequestAfterTests() {
        repository.deleteById(createRequestId());
    }

    private InterfaceComplianceRequestId createRequestId() {
        InterfaceComplianceRequestId requestId = new InterfaceComplianceRequestId();
        requestId.setRepositoryId("repository");
        requestId.setInterfaceId("interface");
        return requestId;
    }

    private InterfaceComplianceRequest createRequest(String compatibilityLevel) {
        InterfaceComplianceRequest request = new InterfaceComplianceRequest();
        InterfaceComplianceRequestId requestId = createRequestId();
        request.setRepositoryId(requestId.getRepositoryId());
        request.setInterfaceId(requestId.getInterfaceId());
        request.setDesiredCompatibilityLevel(compatibilityLevel);
        request.setSubmissionDate(new Date());
        return request;
    }
}
