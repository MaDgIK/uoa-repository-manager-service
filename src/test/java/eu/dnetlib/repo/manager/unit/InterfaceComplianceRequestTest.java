package eu.dnetlib.repo.manager.unit;

import eu.dnetlib.repo.manager.domain.InterfaceComplianceRequest;
import eu.dnetlib.repo.manager.domain.dto.InterfaceComplianceRequestDTO;
import org.junit.Assert;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;


@SpringBootTest(classes = InterfaceComplianceRequest.class)
@RunWith(SpringRunner.class)
class InterfaceComplianceRequestTest {

    @Test
    void interfaceComplianceRequest_contains_date_test() {
        InterfaceComplianceRequest request = new InterfaceComplianceRequest();
        Assert.assertNotNull(request.getSubmissionDate());

        InterfaceComplianceRequestDTO dto = new InterfaceComplianceRequestDTO();
        Assert.assertNull(dto.getSubmissionDate());

        request = InterfaceComplianceRequest.from(dto);
        Assert.assertNotNull(request.getSubmissionDate());
    }
}
