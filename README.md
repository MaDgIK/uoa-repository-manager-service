# OpenAIRE Provide - backend service

## Introduction
OpenAIRE PROVIDE is the content gateway service of OpenAIRE, where data providers are invited to connect scholarly content with OpenAIRE.<br>
OpenAIRE PROVIDE allows repositories, data archives, journals, aggregators, CRIS systems, to enter the OpenAIRE and European Open Science (EOSC) ecosystem and be accessible by millions of researchers, research institutes and networks, research funders, policy makers and citizens.<br>
OpenAIRE PROVIDE lowers any technological barriers, by supporting a series of integrations, therefore, enabling its users to visually access OpenAIRE's services that are responsible for the data harvesting process.
<br><br>
There are four distinctive steps from the initial express of interest for OpenAIRE PROVIDE to the actual content availability on OpenAIRE and EOSC. 
<br>
The steps indicate the important subservices of OpenAIRE that perform the following functionalities:
- Validation of data sources with the OpenAIRE guidelines (via the OpenAIRE Validator)
- Registration of data sources to OpenAIRE and global interlinked networks provides links to content for text and data mining, view history of validations, status of harvesting Enrichment of metadata information that describes the data sources to be available through OpenAIRE.
- Subscribe and view/receive notifications to enrich the metadata or the content of the data source (via the OpenAIRE Broker)
- View usage statistics of Open research impact by subscribing to the OpenAIRE UsageCounts service; view aggregated, cleaned usage stats for repository access and broaden your mechanisms for impact assessment.

<br>

## Architecture
[...]

<br>

## Building
The backend is a [Maven](https://maven.apache.org/index.html) project. It has been tested using Java 8.
<br>

#### Manual Build Instructions

###### Requirements:
- Java 8
- Apache Maven 3+

###### Build Instructions:
1. Clone the repository and move inside the directory
   <br> `git clone https://code-repo.d4science.org/MaDgIK/uoa-repository-manager-service.git && cd uoa-repository-manager-service`
2. Provide all not-set or redacted configurations, inside the **src/main/resources/application.yml** file.
3. Build Maven project
   <br> `mvn clean package`
   <br> Produces the file "./target/**uoa-repository-manager-service.jar**" which can be run with: - Run the app with: `java -jar ./target/uoa-repository-manager-service.jar`

#### Build using Docker
The repository contains a **Dockerfile** which can be used to build an image containing the compiled project.

###### Requirements:
- Docker

###### Build Instructions:
1. Clone the repository and move inside the directory
   <br> `git clone https://code-repo.d4science.org/MaDgIK/uoa-repository-manager-service.git && cd uoa-repository-manager-service`
2. Provide all not-set or redacted configurations, inside the **src/main/resources/application.yml** file.
3. Build Docker image
   <br> `docker build . -t <docker-image-name>`

<br>

## Configuration
The configuration can be set inside the **src/main/resources/application.yml** file.

### Server-related configuration.
```
server:
   port: 8480
   servlet:
      context-path: /uoa-repository-manager-service
```

### Swagger UI - related configuration
```
springdoc:
   swagger-ui:
      disable-swagger-default-url: true
      path: /swagger-ui.html
      displayRequestDuration: true
   api-docs:
      path: /api-docs
```

### Datasource -related configuration
```
spring:
   jpa:
      hibernate:
         ddl-auto: update
    datasource:
        url: ${services.provide.db.url}
        username: ${services.provide.db.username}
        password: ${services.provide.db.password}
        driverClassName: ${services.provide.db.driverClassName}
```

### Configurations about the provide service.
```
services:
   openaireServicesBaseUrl: https://beta.services.openaire.eu
   provide:
      dev-machine: 88.197.53.71   # VM-71
```

### Configuration about Authentication and authorization infrastructure (AAI).
```
      aai:
         baseURL: https://aai.openaire.eu
         oidc:
            domain: .openaire.eu # use empty value for local, otherwise: ".openaire.eu"
            id: XX
            issuer: ${services.provide.aai.baseURL}/oidc/
            redirectURL: http://localhost:${server.port}${server.servlet.context-path}/openid_connect_login
            secret: XX
            webURL: http://localhost:4200/join
         registry:
            coid: XX
            username: XX
            password: XX
            production: false
            url: ${services.provide.aai.baseURL}/registry/
```

### Broker's configuration
```
      broker:
         api: api/
         openaire: openaireBroker
         port: 443
         url: https://beta.broker.openaire.eu
```

### Client's configuration
```
      clients:
         dsm: ${services.provide.baseUrl}
         search: ${services.openaireServicesBaseUrl}/search/v2/api
         usageEvents: http://beta.lbs.openaire.eu:8080/ajax/summary
         usagestats: ${services.openaireServicesBaseUrl}/usagestats
```

### Database configuration for Provide.
```
      db:
         driverClassName: org.postgresql.Driver
         password: dnetPwd
         url: jdbc:postgresql://${services.provide.dev-machine}:5432/repomanager
         username: dnet
```

### Configuration for the IS LookUp service.
```
      iSLookUpService:
         url: https://dev-openaire.d4science.org:443/is/services/isLookUp
```

### Mail-server configuration.
```
      mail:
         authenticate: true
         debug: false
         from: XX
         host: smtp.gmail.com
         mode: ssl
         password: XX
         port: 465
         replyTo: XX
         username: XX
```

### Redis's configuration
```
      redis:
         host: vereniki.athenarc.gr
         password: XX
         port: 6379
```

### Usage statistics
```
      usageStatisticsDiagramsBaseURL: https://beta.openaire.eu/stats3/
      usageStatisticsNumbersBaseURL: ${services.openaireServicesBaseUrl}/usagestats/datasources/
      usagestats:
         adminEmail: XX
         sushiliteEndpoint: ${services.openaireServicesBaseUrl}/usagestats/sushilite/
         sushiliteR5Endpoint: ${services.openaireServicesBaseUrl}/usagestats_r5/sushilite/r5/
```

### Validator - related configuration.
```
      validator:
         results:
            url: https://beta.provide.openaire.eu/compatibility/browseHistory/
      validatorService:
         url: http://${services.provide.dev-machine}:8080/uoa-validator-service/services/validatorWebService
```

### Miscellaneous
```
      topic_types:
         url: ${services.openaireServicesBaseUrl}/provision/mvc/vocabularies/dnet:topic_types.json
      adminEmail: XX
      analyticsURL: https://analytics.openaire.eu/addsite.php?
      baseUrl: ${services.openaireServicesBaseUrl}/openaire
```

<br>

## Deployment


### Prerequisites
* install [PostgreSQL 9.5+](https://www.postgresql.org/)
*  Run the app with: `java -jar ./target/uoa-repository-manager-service.jar --spring.config.location=application.yml`

<br>


## misc

### Notes for Swagger-UI:
- Access it through this url: http://localhost:8480/uoa-repository-manager-service/swagger-ui/index.html
- In order to request data from most endpoints, you have to be a "REGISTERED_USER", otherwise you will get a 403 error code.
- In order to be a registered user, you have to run the [UI-service](https://code-repo.d4science.org/MaDgIK/uoa-repository-manager-ui) , in the same machine, at the same time and login through a browser, using the following url: http://localhost:8480/uoa-repository-manager-service/openid_connect_login